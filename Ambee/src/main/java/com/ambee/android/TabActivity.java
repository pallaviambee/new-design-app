package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.TabAdapter;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;

public class TabActivity extends AppCompatActivity {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Context mContext;
    private ImageView mBackBtn;
    // mId = station or device id to get data
    // mParameter for placing selected para at first position
    private String mId = "", mParameter;
    private List<ModelItem> mDataList = new ArrayList<>();

    // below navigation
    private LinearLayout mHomeLayout, mMapLayout, mRankLayout, mProfileLayout;
    private ImageView mHomeImg, mMapImg, mRankImg, mProfileImg;
    private TextView mHomeTxt, mMapText, mRankText, mProfileText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        mContext = getApplicationContext();
        initUi();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mId = bundle.getString("ID", "");
            mParameter = bundle.getString("Para", "");
        }
        // convert parameter to server parameter to get data
        switch (mParameter) {
            case "PM 2.5":
                mParameter = "PM25";
                break;
            case "PM 10":
                mParameter = "PM10";
                break;
            case "PM 1":
                mParameter = "PM1";
                break;
            case "O3":
                mParameter = "OZONE";
                break;
            default:
                break;
        }
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            // get details
            getPlaceDetails();
        }

        // For below navigation
        setBelowNavigation();
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setBelowNavigation() {
        mHomeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        mHomeImg.setColorFilter(mContext.getResources().getColor(R.color.white));
        mHomeTxt.setVisibility(View.VISIBLE);
        mHomeTxt.setTextColor(mContext.getResources().getColor(R.color.white));
        mHomeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // finish();
            }
        });
        mHomeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  finish();
            }
        });
        mHomeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  finish();
            }
        });
        mMapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        mProfileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(TabActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initUi() {

        mBackBtn = (ImageView) findViewById(R.id.back);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        mHomeLayout = (LinearLayout) findViewById(R.id.home);
        mHomeImg = (ImageView) findViewById(R.id.homeImg);
        mHomeTxt = (TextView) findViewById(R.id.homeTxt);

        mMapLayout = (LinearLayout) findViewById(R.id.map);
        mMapImg = (ImageView) findViewById(R.id.mapImg);
        mMapText = (TextView) findViewById(R.id.mapTxt);

        mRankLayout = (LinearLayout) findViewById(R.id.rank);
        mRankImg = (ImageView) findViewById(R.id.rankImg);
        mRankText = (TextView) findViewById(R.id.rankTxt);

        mProfileLayout = (LinearLayout) findViewById(R.id.profile);
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        mProfileText = (TextView) findViewById(R.id.profileTxt);

    }

    // check and get token after every 5 min
    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());

                            // get details
                            getPlaceDetails();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getPlaceDetails() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<LatestData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationLatestData("Bearer " +
                            AccountUtil.getToken(mContext), mId);

            call.enqueue(new Callback<LatestData>() {
                @Override
                public void onResponse(Call<LatestData> call, Response<LatestData> response) {
                    LatestData latestData = response.body();
                    if (latestData != null) {
                        LinkedHashMap<String, String> hashMap = latestData.getData();
                        if (hashMap != null && hashMap.size() > 0) {
                            setTabTitle(hashMap, latestData.getDataType());
                        }
                    }
                }

                @Override
                public void onFailure(Call<LatestData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setTabTitle(LinkedHashMap<String, String> hashMap, String type) {
        mDataList.clear();
        String para = "";
        String value = "";

        // selected parameter on first tab
        if (hashMap.containsKey(mParameter)) {
            para = mParameter;
            value = getValue(hashMap.get(mParameter));
            mDataList.add(new ModelItem(para, value));
        }

        switch (type) {
            case "device":
                if (hashMap.containsKey("PM25")) {
                    para = "PM25";
                    value = getValue(hashMap.get("PM25"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("PM10")) {
                    para = "PM10";
                    value = getValue(hashMap.get("PM10"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("PM1")) {
                    para = "PM1";
                    value = getValue(hashMap.get("PM1"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("CO2")) {
                    para = "CO2";
                    value = getValue(hashMap.get("CO2"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("TVOC")) {
                    para = "TVOC";
                    value = getValue(hashMap.get("TVOC"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("HCHO")) {
                    para = "HCHO";
                    value = getValue(hashMap.get("HCHO"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("AQI")) {
                    para = "AQI";
                    value = getValue(hashMap.get("AQI"));
                    mDataList.add(new ModelItem(para, value));
                }
                break;
            case "station":
                if (hashMap.containsKey("PM25")) {
                    para = "PM25";
                    value = getValue(hashMap.get("PM25"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("PM10")) {
                    para = "PM10";
                    value = getValue(hashMap.get("PM10"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("OZONE")) {
                    para = "OZONE";
                    value = getValue(hashMap.get("OZONE"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("CO")) {
                    para = "CO";
                    value = getValue(hashMap.get("CO"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("NO2")) {
                    para = "NO2";
                    value = getValue(hashMap.get("NO2"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("SO2")) {
                    para = "SO2";
                    value = getValue(hashMap.get("SO2"));
                    if (!mDataList.get(0).getParameter().contains(para))
                        mDataList.add(new ModelItem(para, value));
                }
                if (hashMap.containsKey("AQI")) {
                    para = "AQI";
                    value = getValue(hashMap.get("AQI"));
                    mDataList.add(new ModelItem(para, value));
                }
                break;
        }
        if (mDataList != null && mDataList.size() > 0) {
            setAdapter();
        }
    }

    private void setAdapter() {
        adapter = new TabAdapter(getSupportFragmentManager(), mContext);
        int hightLight = 0;
        for (int i = 0; i < mDataList.size(); i++) {
            String para = mDataList.get(i).getParameter();
            String value = mDataList.get(i).getValue();
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            adapter.addFragment(new DetailsFragment(TabActivity.this), para + "_" + value, mId);
            switch (para) {
                case "PM25":
                    para = "PM 2.5";
                    break;
                case "PM10":
                    para = "PM 10";
                    break;
                case "PM1":
                    para = "PM 1";
                    break;
                case "OZONE":
                    para = "O3";
                    break;
                default:
                    para = para;
                    break;
            }
          /*  if (para != null && para.equals(mParameter)) {
                hightLight = i;
            }*/
        }
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        highLightCurrentTab(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                highLightCurrentTab(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }

        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }
}
