package com.ambee.android;

import android.os.Bundle;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import com.ambee.android.Adapter.RewardsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RewardsActivity extends BaseActivity {
    private ImageView mBack;
    private GridView mRewardGrid;
    private List<String> mRewardsList = new ArrayList<>();

    private String[] mRewards = new String[]{"Early adopter", "Green conscious", "Smart Fitness", "Low carbon consumption",
            "Eco warrior", "Breathtaking", "Good samaritan", "Golden eco warrior"};
    private RewardsAdapter mRewardsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);
        mBack = (ImageView) findViewById(R.id.back);
        mRewardGrid = (GridView) findViewById(R.id.gridView);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        addRewardsList();
        setAdapter();

    }

    private void addRewardsList() {
        mRewardsList.addAll(Arrays.asList(mRewards));
    }

    private void setAdapter() {
        mRewardsAdapter = new RewardsAdapter(RewardsActivity.this, getApplicationContext(), mRewardsList);
        mRewardGrid.setAdapter(mRewardsAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
