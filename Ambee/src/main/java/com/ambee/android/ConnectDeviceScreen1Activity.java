package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.droidsonroids.gif.GifImageView;

public class ConnectDeviceScreen1Activity extends BaseActivity {
    private Context mContext;
    private GifImageView mGifConnection;
    private TextView mHeading;
    private LinearLayout mBackLay, mNextLay;
    private ImageView mBackImg, mNextImg;
    private TextView mBackTxt, mNextTxt;
    private ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_device_screen1);
        initUi();
        mContext = getApplicationContext();

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBackLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        HomeActivity.class);
                startActivity(intent);
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        HomeActivity.class);
                startActivity(intent);
            }
        });
        mBackTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        HomeActivity.class);
                startActivity(intent);
            }
        });
        mNextLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        ConnectDeviceScreen2Activity.class);
                startActivity(intent);
            }
        });
        mNextImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        ConnectDeviceScreen2Activity.class);
                startActivity(intent);
            }
        });
        mNextTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                        ConnectDeviceScreen2Activity.class);
                startActivity(intent);
            }
        });
    }

    private void initUi() {
        mBack = (ImageView) findViewById(R.id.back);
        mHeading = (TextView) findViewById(R.id.heading);
        mBackLay = (LinearLayout) findViewById(R.id.backLay);
        mBackImg = (ImageView) findViewById(R.id.backArr);
        mBackTxt = (TextView) findViewById(R.id.backText);
        mNextLay = (LinearLayout) findViewById(R.id.nextLay);
        mNextImg = (ImageView) findViewById(R.id.nextArr);
        mNextTxt = (TextView) findViewById(R.id.nextText);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(ConnectDeviceScreen1Activity.this,
                HomeActivity.class);
        startActivity(intent);
    }
}
