package com.ambee.android;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.AddDeviceBody;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.esptouch.EsptouchTask;
import com.ambee.android.esptouch.IEsptouchListener;
import com.ambee.android.esptouch.IEsptouchResult;
import com.ambee.android.esptouch.IEsptouchTask;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PairDeviceActivity extends BaseActivity {
    private Context mContext;
    private EditText mDeviceName, mWifiName, mWifiPassword;
    private TextView mConnectText;
    private AlertDialog mAlertDialog;
    private ImageView mBack;

    private String mDeviceId = "";
    private String mDeviceNameStr = "";
    private String mAddress = "";
    private String mPlaceName = "";
    private String mCity = "";
    private String mState = "";
    private String mCountry = "";
    private String mPostalCode = "";
    private String mCountryCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pair_device);
        initUi();
        mContext = getApplicationContext();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mDeviceId = bundle.getString("DeviceId");
        }
        String ssid = getSsid();
        if (ssid != null) {

        } else {
            showPopup("No Network !!\nPlease connect your mobile to WIFI first", false);
        }
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mConnectText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ssid = getSsid();
                mDeviceNameStr = mDeviceName.getText().toString();
                if (mDeviceNameStr.isEmpty()) {
                    Toast.makeText(mContext, "Please enter your device name",
                            Toast.LENGTH_LONG).show();
                    return;
                }
                if (ssid != null) {
                    showPopup("The device is waiting to configure the wifi phase",
                            true);
                } else {
                    showPopup("No Network !!\nPlease connect your mobile to wifi first", false);
                }
            }
        });
    }

    private void showPopup(String str, final boolean connect) {
        if (mAlertDialog != null)
            mAlertDialog.dismiss();
        LayoutInflater li = LayoutInflater.from(PairDeviceActivity.this);
        View promptsView = li.inflate(R.layout.prompts, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                PairDeviceActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        heading.setText(str);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        cancel.setVisibility(View.INVISIBLE);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        if (connect)
            ok.setText("Confirm");
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDialog != null)
                    mAlertDialog.dismiss();
                if (connect) {
                    String apSsid = mWifiName.getText().toString();
                    String apPassword = mWifiPassword.getText().toString();
                    String apBssid = getWifiConnectedBssid();
                    Boolean isSsidHidden = false;
                    String isSsidHiddenStr = "NO";
                    if (isSsidHidden) {
                        isSsidHiddenStr = "YES";
                    }
                    new EsptouchAsyncTask3().execute(apSsid, apBssid, apPassword,
                            isSsidHiddenStr);
                }

            }
        });
        mAlertDialog = alertDialogBuilder.show();
        mAlertDialog.setCanceledOnTouchOutside(false);
    }

    private String getWifiConnectedBssid() {
        WifiInfo mWifiInfo = getConnectionInfo();
        String bssid = null;
        if (mWifiInfo != null && isWifiConnected()) {
            bssid = mWifiInfo.getBSSID();
        }
        return bssid;
    }

    private String getSsid() {
        WifiInfo mWifiInfo = getConnectionInfo();
        String ssid = null;
        if (mWifiInfo != null && isWifiConnected()) {
            int len = mWifiInfo.getSSID().length();
            if (mWifiInfo.getSSID().startsWith("\"")
                    && mWifiInfo.getSSID().endsWith("\"")) {
                ssid = mWifiInfo.getSSID().substring(1, len - 1);
            } else {
                ssid = mWifiInfo.getSSID();
            }
        }
        return ssid;
    }

    private boolean isWifiConnected() {
        NetworkInfo mWiFiNetworkInfo = getWifiNetworkInfo();
        boolean isWifiConnected = false;
        if (mWiFiNetworkInfo != null) {
            isWifiConnected = mWiFiNetworkInfo.isConnected();
        }
        return isWifiConnected;
    }

    public String getWifiConnectedSsidAscii(String ssid) {
        final long timeout = 100;
        final long interval = 20;
        String ssidAscii = ssid;

        WifiManager wifiManager = (WifiManager) mContext
                .getSystemService(Context.WIFI_SERVICE);
        wifiManager.startScan();

        boolean isBreak = false;
        long start = System.currentTimeMillis();
        do {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException ignore) {
                isBreak = true;
                break;
            }
            List<ScanResult> scanResults = wifiManager.getScanResults();
            for (ScanResult scanResult : scanResults) {
                if (scanResult.SSID != null && scanResult.SSID.equals(ssid)) {
                    isBreak = true;
                    try {
                        Field wifiSsidfield = ScanResult.class
                                .getDeclaredField("wifiSsid");
                        wifiSsidfield.setAccessible(true);
                        Class<?> wifiSsidClass = wifiSsidfield.getType();
                        Object wifiSsid = wifiSsidfield.get(scanResult);
                        Method method = wifiSsidClass
                                .getDeclaredMethod("getOctets");
                        byte[] bytes = (byte[]) method.invoke(wifiSsid);
                        ssidAscii = new String(bytes, "ISO-8859-1");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        } while (System.currentTimeMillis() - start < timeout && !isBreak);

        return ssidAscii;
    }

    private NetworkInfo getWifiNetworkInfo() {
        ConnectivityManager mConnectivityManager = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = mConnectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo != null) {
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return activeNetworkInfo;
            }
        }
        return null;
    }

    private WifiInfo getConnectionInfo() {
        WifiManager mWifiManager = (WifiManager) mContext
                .getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = mWifiManager.getConnectionInfo();
        return wifiInfo;
    }

    private void initUi() {
        mBack = (ImageView) findViewById(R.id.back);
        mDeviceName = (EditText) findViewById(R.id.device_name);
        mWifiName = (EditText) findViewById(R.id.wifi_name);
        mWifiPassword = (EditText) findViewById(R.id.password);
        mConnectText = (TextView) findViewById(R.id.connect_device);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // display the connected ap's ssid
        String ssid = getSsid();
        if (ssid != null) {
            mWifiName.setText(ssid);
        } else {
            mWifiName.setText("");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(PairDeviceActivity.this,
                HomeActivity.class);
        startActivity(intent);
    }

    private void onEsptoucResultAddedPerform(final IEsptouchResult result) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showConnectedPopup();
            }

        });
    }

    private void showConnectedPopup() {
        if (mAlertDialog != null)
            mAlertDialog.dismiss();
        LayoutInflater li = LayoutInflater.from(PairDeviceActivity.this);
        View promptsView = li.inflate(R.layout.promts_connected, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                PairDeviceActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView done = (TextView) promptsView.findViewById(R.id.done);

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDialog != null)
                    mAlertDialog.dismiss();
                finish();
                Intent intent = new Intent(PairDeviceActivity.this,
                        HomeActivity.class);
                startActivity(intent);
            }
        });
        mAlertDialog = alertDialogBuilder.show();
        mAlertDialog.setCanceledOnTouchOutside(false);
    }

    private IEsptouchListener myListener = new IEsptouchListener() {

        @Override
        public void onEsptouchResultAdded(final IEsptouchResult result) {
            onEsptoucResultAddedPerform(result);
        }
    };

    // check and get token after every 5 min
    public void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext),
                            AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            // send device info to map user with device
                            sendDeviceData();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void sendDeviceData() {
        getAddress();
        AddDeviceBody addDeviceBody = new AddDeviceBody(mDeviceId, mDeviceNameStr,
                Double.parseDouble(HomeActivity.mCurrentLatitude),
                Double.parseDouble(HomeActivity.mCurrentLongitude), mAddress, mPlaceName, mCity,
                mState, mCountry, mPostalCode, mCountryCode);
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<ResponseBody> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .registerDevice("Bearer " + AccountUtil.getToken(mContext),
                            addDeviceBody);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        Toast.makeText(mContext, "Device register successfully !! ", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, "Failed !! Please try again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getAddress() {
        Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(HomeActivity.mCurrentLatitude),
                    Double.parseDouble(HomeActivity.mCurrentLongitude), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            mAddress = addresses.get(0).getAddressLine(0);
            mPlaceName = addresses.get(0).getFeatureName() + "," +
                    addresses.get(0).getSubLocality();
            mCity = addresses.get(0).getLocality();
            mState = addresses.get(0).getAdminArea();
            mCountry = addresses.get(0).getCountryName();
            mPostalCode = addresses.get(0).getPostalCode();
            mCountryCode = addresses.get(0).getCountryCode();
        } else {
            // do your stuff
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private class EsptouchAsyncTask3 extends AsyncTask<String, Void, List<IEsptouchResult>> {
        private ProgressDialog mProgressDialog;

        private IEsptouchTask mEsptouchTask;
        // without the lock, if the user tap confirm and cancel quickly enough,
        // the bug will arise. the reason is follows:
        // 0. task is starting created, but not finished
        // 1. the task is cancel for the task hasn't been created, it do nothing
        // 2. task is created
        // 3. Oops, the task should be cancelled, but it is running
        private final Object mLock = new Object();

        @Override
        protected void onPreExecute() {
            mProgressDialog = new ProgressDialog(PairDeviceActivity.this);
            mProgressDialog
                    .setMessage("Connecting to WIFI...");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    synchronized (mLock) {
                        if (mEsptouchTask != null) {
                            mEsptouchTask.interrupt();
                        }
                    }
                }
            });
            mProgressDialog.show();
        }

        @Override
        protected List<IEsptouchResult> doInBackground(String... params) {
            int taskResultCount = -1;
            synchronized (mLock) {
                // !!!NOTICE
                String apSsid = getWifiConnectedSsidAscii(params[0]);
                String apBssid = params[1];
                String apPassword = params[2];
                String isSsidHiddenStr = params[3];
                boolean isSsidHidden = false;
                if (isSsidHiddenStr.equals("YES")) {
                    isSsidHidden = true;
                }
                taskResultCount = 1;
                mEsptouchTask = new EsptouchTask(apSsid, apBssid, apPassword,
                        isSsidHidden, PairDeviceActivity.this);
                mEsptouchTask.setEsptouchListener(myListener);
            }
            List<IEsptouchResult> resultList = mEsptouchTask.executeForResults(taskResultCount);
            return resultList;
        }

        @Override
        protected void onPostExecute(List<IEsptouchResult> result) {
            if (mProgressDialog != null)
                mProgressDialog.dismiss();

            IEsptouchResult firstResult = result.get(0);
            // check whether the task is cancelled and no results received
            if (!firstResult.isCancelled()) {
                int count = 0;
                // max results to be displayed, if it is more than maxDisplayCount,
                // just show the count of redundant ones
                final int maxDisplayCount = 5;
                // the task received some results including cancelled while
                // executing before receiving enough results
                if (firstResult.isSuc()) {
                    //check time for token 5 min expire
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken();
                    } else {
                        sendDeviceData();
                    }
                    StringBuilder sb = new StringBuilder();
                    for (IEsptouchResult resultInList : result) {
                        sb.append("Esptouch success, bssid = "
                                + resultInList.getBssid()
                                + ",InetAddress = "
                                + resultInList.getInetAddress()
                                .getHostAddress() + "\n");
                        count++;
                        if (count >= maxDisplayCount) {
                            break;
                        }
                    }
                    if (count < result.size()) {
                        sb.append("\nthere's " + (result.size() - count)
                                + " more result(s) without showing\n");
                    }
                } else {
                    showPopup("Not Connected to WIFI!! Failed", false);
                }
            }
        }
    }
}
