package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.HomeAdapter;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;

public class FavouritesActivity extends BaseActivity {
    private Context mContext;
    private ImageView mBackImg, mAddPlace, mAddDeviceImg;
    private TextView mAddDeviceTxt;
    private LinearLayout mAddLay;
    private ListView mMyDevicesList, mSavedPlacesList;
    // Add list for saved stations
    private List<ModelItem> mDataList = new ArrayList<>();
    private HomeAdapter mHomeAdapter;
    private TextView mNoSavedPlaces, mNoAddedDevices;
    // Add list for added device
    private List<ModelItem> mAddedDeviceDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourites);
        mContext = getApplicationContext();
        initUi();
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            // for saved place
            getSavedPlaces();
            // get added device
            getAddedDevice();
        }
        mAddPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FavouritesActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mAddLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(FavouritesActivity.this, ConnectDeviceScreen1Activity.class);
                startActivity(intent);
            }
        });
        mAddDeviceTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(FavouritesActivity.this, ConnectDeviceScreen1Activity.class);
                startActivity(intent);
            }
        });
    }

    // check and get token after every 5 min
    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());

                            // get saved places
                            getSavedPlaces();
                            // get added device
                            getAddedDevice();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getAddedDevice() {
        mAddedDeviceDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getDevices("Bearer " + AccountUtil.getToken(mContext));
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mAddedDeviceDataList.clear();
                    UserStations userDevice = response.body();
                    if (userDevice != null) {
                        List<LinkedHashMap<String, String>> stations = userDevice.getStations();
                        if (stations != null && stations.size() > 0) {
                            for (int i = 0; i < stations.size(); i++) {
                                String para = "";
                                String value = "";
                                String placeName = "";
                                String city = "";
                                String country = "";
                                String devId = "";
                                LinkedHashMap<String, String> hashMap = stations.get(i);
                                if (hashMap.containsKey("_id")) {
                                    devId = hashMap.get("_id");
                                }
                                if (hashMap.containsKey("name")) {
                                    placeName = hashMap.get("name");
                                }
                                if (placeName.isEmpty()) {
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }
                                if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("PM1")) {
                                    para = "PM 1";
                                    value = getValue(hashMap.get("PM1"));
                                } else if (hashMap.containsKey("CO2")) {
                                    para = "CO2";
                                    value = getValue(hashMap.get("CO2"));
                                } else if (hashMap.containsKey("TVOC")) {
                                    para = "TVOC";
                                    value = getValue(hashMap.get("TVOC"));
                                } else if (hashMap.containsKey("HCHO")) {
                                    para = "HCHO";
                                    value = getValue(hashMap.get("HCHO"));
                                } else if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                }
                                mAddedDeviceDataList.add(new ModelItem(para, value, placeName, city,
                                        devId, country, "Device"));
                            }
                        }
                    }

                    if (mAddedDeviceDataList != null && mAddedDeviceDataList.size() > 0) {
                        mMyDevicesList.setFocusable(false);
                        mNoAddedDevices.setVisibility(View.GONE);
                        mHomeAdapter = new HomeAdapter(FavouritesActivity.this, mContext, mAddedDeviceDataList);
                        mMyDevicesList.setAdapter(mHomeAdapter);
                    } else {
                        mNoAddedDevices.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getSavedPlaces() {
        mDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStations("Bearer " + AccountUtil.getToken(mContext));
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userSavedPlace = response.body();
                    if (userSavedPlace != null) {
                        List<LinkedHashMap<String, String>> stations = userSavedPlace.getStations();
                        if (stations != null && stations.size() > 0) {
                            for (int i = 0; i < stations.size(); i++) {
                                String para = "";
                                String value = "";
                                String placeName = "";
                                String city = "";
                                String country = "";
                                String cityId = "";
                                LinkedHashMap<String, String> hashMap = stations.get(i);
                                if (hashMap.containsKey("placeId")) {
                                    cityId = hashMap.get("placeId");
                                }
                                if (hashMap.containsKey("name")) {
                                    placeName = hashMap.get("name");
                                }
                                if (placeName.isEmpty()) {
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }
                                if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("OZONE")) {
                                    para = "O3";
                                    value = getValue(hashMap.get("OZONE"));
                                } else if (hashMap.containsKey("CO")) {
                                    para = "CO";
                                    value = getValue(hashMap.get("CO"));
                                } else if (hashMap.containsKey("NO2")) {
                                    para = "NO2";
                                    value = getValue(hashMap.get("NO2"));
                                } else if (hashMap.containsKey("SO2")) {
                                    para = "SO2";
                                    value = getValue(hashMap.get("SO2"));
                                } else if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                }
                                mDataList.add(new ModelItem(para, value, placeName, city, cityId, country, "Place"));
                            }
                        }
                    }
                    setAdapter();
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setAdapter() {
        if (mDataList != null && mDataList.size() > 0) {
            mNoSavedPlaces.setVisibility(View.GONE);
            mSavedPlacesList.setFocusable(false);
            mHomeAdapter = new HomeAdapter(FavouritesActivity.this, mContext, mDataList);
            mSavedPlacesList.setAdapter(mHomeAdapter);
        } else {
            mNoSavedPlaces.setVisibility(View.VISIBLE);
        }
    }

    private void initUi() {
        mBackImg = (ImageView) findViewById(R.id.back);
        mAddPlace = (ImageView) findViewById(R.id.addPlace);
        mMyDevicesList = (ListView) findViewById(R.id.myDevicesList);
        mAddLay = (LinearLayout) findViewById(R.id.addLay);
        mAddDeviceImg = (ImageView) findViewById(R.id.addImg);
        mAddDeviceTxt = (TextView) findViewById(R.id.addDevice);
        mSavedPlacesList = (ListView) findViewById(R.id.savedPlacesList);
        mNoAddedDevices = (TextView) findViewById(R.id.noDevices);
        mNoSavedPlaces = (TextView) findViewById(R.id.noSavedPlaces);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            // for saved place
            getSavedPlaces();
            // get added device
            getAddedDevice();
        }
    }
}
