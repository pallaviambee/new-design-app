package com.ambee.android.Util;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {

    public static String getCurrentTimeDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        return formatter.format(date);
    }

    public static boolean checkTime(Context context) {
        try {
            String time = AccountUtil.getTime(context);
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date dateObj1 = sdf.parse(time);
            String currentTime = DateTimeUtil.getCurrentTimeDate();
            Date dateObj2 = sdf.parse(currentTime);

            long diff = dateObj2.getTime() - dateObj1.getTime();
            double diffInHours = diff / ((double) 1000 * 60 * 60);
            double hr = (int) diffInHours;
            double Min = (diffInHours - (int) diffInHours) * 60;
            double sec = (diffInHours - (int) diffInHours) * 60 * 60;
            if (Min > 5)
                return true;
            else return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }
}
