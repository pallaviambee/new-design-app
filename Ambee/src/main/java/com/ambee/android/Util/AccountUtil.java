package com.ambee.android.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class AccountUtil {
    private static String TOKEN = "token";
    private static String GUEST = "guest";
    private static String USER_IMG_URL = "user_img_url";
    private static String USER_NAME = "user_name";
    private static String USER_EMAIL = "user_email";
    private static String USER_ID = "user_id";
    private static String TIME = "time";
    private static String GUEST_TOKEN = "a2155cca775750fc0f2039841412774e";


    static SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setToken(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(TOKEN, token);
        editor.commit();
    }

    public static String getToken(Context context) {
        return getSharedPreferences(context).getString(TOKEN, "");
    }

    public static void setGuest(Context context, boolean token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(GUEST, token);
        editor.commit();
    }


    public static boolean isGuest(Context context) {
        return getSharedPreferences(context).getBoolean(GUEST, false);
    }

    public static String getGuestToken(Context context) {
        return GUEST_TOKEN;
    }

    public static void setUserPic(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_IMG_URL, token);
        editor.commit();
    }

    public static String getUserPic(Context context) {
        return getSharedPreferences(context).getString(USER_IMG_URL, "");
    }

    public static void setName(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_NAME, token);
        editor.commit();
    }

    public static String getName(Context context) {
        return getSharedPreferences(context).getString(USER_NAME, "");
    }

    public static void setEmail(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_EMAIL, token);
        editor.commit();
    }

    public static String getEmail(Context context) {
        return getSharedPreferences(context).getString(USER_EMAIL, "");
    }

    public static void setUserId(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(USER_ID, token);
        editor.commit();
    }

    public static String getUserId(Context context) {
        return getSharedPreferences(context).getString(USER_ID, "");
    }

    public static void setTime(Context context, String token) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(TIME, token);
        editor.commit();
    }

    public static String getTime(Context context) {
        return getSharedPreferences(context).getString(TIME, "");
    }

}
