package com.ambee.android.Util;

import android.content.Context;

public class ParameterUtil {

    public static String getParameterColour(Context context, String parameter, double value) {
        String colour = "";
        switch (parameter) {
            case "PM 2.5":
                if (value >= 0 && value <= 60) {
                    colour = "green";
                } else if (value > 60 && value <= 90) {
                    colour = "orange";
                } else if (value > 90 && value <= 200) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "PM 10":
                if (value >= 0 && value <= 60) {
                    colour = "green";
                } else if (value > 60 && value <= 90) {
                    colour = "orange";
                } else if (value > 90 && value <= 200) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "PM 1":
                if (value >= 0 && value <= 60) {
                    colour = "green";
                } else if (value > 60 && value <= 90) {
                    colour = "orange";
                } else if (value > 90 && value <= 200) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "O3":
                if (value >= 0 && value <= 0.05094) {
                    colour = "green";
                } else if (value > 0.05094 && value <= 0.08558) {
                    colour = "orange";
                } else if (value > 0.08558 && value <= 0.38) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "CO":
                if (value >= 0 && value <= 1.75) {
                    colour = "green";
                } else if (value > 1.75 && value <= 8.73) {
                    colour = "orange";
                } else if (value > 8.73 && value <= 29.68) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "NO2":
                if (value >= 0 && value <= 0.04252) {
                    colour = "green";
                } else if (value > 0.04252 && value <= 0.9566) {
                    colour = "orange";
                } else if (value > 0.9566 && value <= 0.21) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "SO2":
                if (value >= 0 && value <= 0.03053) {
                    colour = "green";
                } else if (value > 0.03053 && value <= 0.15) {
                    colour = "orange";
                } else if (value > 0.15 && value <= 0.61) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "CO2":
                if (value >= 0 && value <= 1000) {
                    colour = "green";
                } else if (value > 1000 && value <= 1500) {
                    colour = "yellow";
                } else if (value > 1500 && value <= 2000) {
                    colour = "orange";
                } else if (value > 2000 && value <= 2500) {
                    colour = "brown";
                } else if (value > 2500) {
                    colour = "purple";
                }
                break;
            case "AQI":
                if (value >= 0 && value <= 60) {
                    colour = "green";
                } else if (value > 60 && value <= 90) {
                    colour = "orange";
                } else if (value > 90 && value <= 200) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "TVOC":
                if (value >= 0 && value <= 0.03053) {
                    colour = "green";
                } else if (value > 0.03053 && value <= 0.15) {
                    colour = "orange";
                } else if (value > 0.15 && value <= 0.61) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            case "HCHO":
                if (value >= 0 && value <= 0.03053) {
                    colour = "green";
                } else if (value > 0.03053 && value <= 0.15) {
                    colour = "orange";
                } else if (value > 0.15 && value <= 0.61) {
                    colour = "red";
                } else {
                    colour = "black";
                }
                break;
            default:
                break;
        }
        return colour;
    }

    public static double getParameterValue(Context context, String parameter, String value) {
        double val = 0;
        double paraVal = 0;
        if (value != null && !value.equals("null") && !value.isEmpty())
            paraVal = Double.parseDouble(value);
        /* formula to cal.  ppm
           ug/m3 = 0.001 mg/m3
           ppm = ((x mg/m3)*24.45)/molecularWeight*/
        switch (parameter) {
            case "PM 1":
                val = paraVal;
                break;
            case "PM 2.5":
                val = paraVal;
                break;
            case "PM 10":
                val = paraVal;
                break;
            case "O3":
                double molecularWeight = 48;
                val = (0.001 * paraVal * 24.45) / molecularWeight;
                val = Math.round(val * 1000.0) / 1000.0;
                break;
            case "CO":
                molecularWeight = 28.01;
                val = (0.001 * paraVal * 24.45) / molecularWeight;
                val = Math.round(val * 1000.0) / 1000.0;
                break;
            case "NO2":
                molecularWeight = 46.0055;
                val = (0.001 * paraVal * 24.45) / molecularWeight;
                val = Math.round(val * 1000.0) / 1000.0;
                break;
            case "SO2":
                molecularWeight = 64.066;
                val = (0.001 * paraVal * 24.45) / molecularWeight;
                val = Math.round(val * 1000.0) / 1000.0;
                break;
            case "AQI":
                val = paraVal;
                break;
            case "TVOC":
                val = paraVal;
                break;
            case "HCHO":
                val = paraVal;
                break;
            case "CO2":
                val = paraVal;
                break;
            default:
                break;
        }
        return val;
    }

    public static String getParameterUnit(Context context, String parameter) {
        String unit = "";
        switch (parameter) {
            case "PM 2.5":
                unit = "ug/m3";
                break;
            case "PM 10":
                unit = "ug/m3";
                break;
            case "PM 1":
                unit = "ug/m3";
                break;
            case "O3":
                unit = "ppm";
                break;
            case "CO":
                unit = "ppm";
                break;
            case "NO2":
                unit = "ppm";
                break;
            case "SO2":
                unit = "ppm";
                break;
            case "AQI":
                unit = "AQI";
                break;
            case "TVOC":
                unit = "mg/m3";
                break;
            case "HCHO":
                unit = "mg/m3";
                break;
            case "CO2":
                unit = "ppm";
                break;
            default:
                break;
        }
        return unit;
    }

    public static String getValue(String value) {
        String val = value;
        if (value != null && value.contains(".")) {
            String[] valArr = value.split("\\.");
            value = valArr[0];
        }
      /*  if (value.contains(".") && val.split("\\.")[1].length() > 3) {
            String[] valArr = value.split("\\.");
            value = valArr[0] + "." + valArr[1].substring(0, 3);
        }*/
        return value;
    }

}
