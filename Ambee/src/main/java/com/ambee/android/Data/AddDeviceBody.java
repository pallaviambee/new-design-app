package com.ambee.android.Data;

public class AddDeviceBody {
    private String devId;
    private String name;
    private double lat;
    private double lng;
    private String address;
    private String placeName;
    private String city;
    private String state;
    private String country;
    private String postalCode;
    private String countryCode;

    public AddDeviceBody(String devId, String name, double lat, double lng, String address,
                         String placeName, String city, String state, String country,
                         String postalCode, String countryCode) {
        this.devId = devId;
        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.address = address;
        this.placeName = placeName;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.countryCode = countryCode;
    }
}
