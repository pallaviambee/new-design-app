package com.ambee.android.Data;

import com.google.gson.annotations.SerializedName;

public class UserToken {
    @SerializedName("_id")
    String id;
    @SerializedName("userId")
    String userId;
    @SerializedName("tokens")
    String token;

    public UserToken(String id, String userId, String token) {
        this.id = id;
        this.userId = userId;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }

}
