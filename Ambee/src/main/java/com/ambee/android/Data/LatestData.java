package com.ambee.android.Data;

import java.util.LinkedHashMap;

public class LatestData {
    private String message;
    private String dataType;
    private LinkedHashMap<String, String> data;

    public LatestData(String message,String dataType, LinkedHashMap<String, String> data) {
        this.message = message;
        this.dataType=dataType;
        this.data = data;
    }

    public String getDataType() {
        return dataType;
    }

    public String getMessage() {
        return message;
    }

    public LinkedHashMap<String, String> getData() {
        return data;
    }
}
