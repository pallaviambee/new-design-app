package com.ambee.android.Data;

public class DeviceIdFromQR {
    private String device_id;

    public DeviceIdFromQR(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_id() {
        return device_id;
    }
}
