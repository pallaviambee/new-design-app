package com.ambee.android.Data;

import java.util.LinkedHashMap;
import java.util.List;

public class UserStations {
    private String message;
    private String count;
    private String page;
    private List<LinkedHashMap<String, String>> stations;

    public UserStations(String message, String count, String page,
                        List<LinkedHashMap<String, String>> stations) {
        this.message = message;
        this.count = count;
        this.page = page;
        this.stations = stations;
    }

    public String getMessage() {
        return message;
    }

    public String getCount() {
        return count;
    }

    public String getPage() {
        return page;
    }

    public List<LinkedHashMap<String, String>> getStations() {
        return stations;
    }
}
