package com.ambee.android.Data;

import android.graphics.drawable.Drawable;

import java.util.LinkedHashMap;

public class ModelItem {
    private String mOption;
    private String mUnit;
    private String mParameter;
    private String mValue;
    private String mLocation;
    private String mCountry;
    private String mCity;
    private String mCityId;
    private String mTimeStamp;
    private String mType;
    private boolean mIsSaved;
    private String mHeading;
    private String mSubHeading;
    private Drawable mIcon;
    private String mTemp;
    private String mHumi;
    private String mDevId;
    private String mImg;
    private LinkedHashMap<String, String> mData;

    // For ranking list
    public ModelItem(String name, String value, String image, String option) {
        mLocation = name;
        mValue = value;
        mImg = image;
        mOption = option;
    }

    // for weather
    public ModelItem(Drawable drawable, String parameter, String value, String unit) {
        mIcon = drawable;
        mValue = value;
        mParameter = parameter;
        mUnit = unit;
    }

    public ModelItem(Drawable drawable, String heading, String subHeading) {
        mIcon = drawable;
        mHeading = heading;
        mSubHeading = subHeading;
    }

    public ModelItem(String parameter, String value) {
        this.mParameter = parameter;
        this.mValue = value;
    }

    public ModelItem(LinkedHashMap<String, String> data, String cityId, String temp, String humidity) {
        mData = data;
        mTemp = temp;
        mHumi = humidity;
        mDevId = cityId;
    }


    public ModelItem(String placeName, String city, String cityId, String country, String type) {
        mLocation = placeName;
        mCity = city;
        mCityId = cityId;
        mCountry = country;
        mType = type;
    }

    public ModelItem(String para, String value, String placeName,
                     String city, String devId, String country, String type) {
        mParameter = para;
        mValue = value;
        mLocation = placeName;
        mCity = city;
        mDevId = devId;
        mCountry = country;
        mType = type;
    }

    public ModelItem(String para, String value, String placeName,
                     String city, String devId, String country, String type, boolean isSaved) {
        mParameter = para;
        mValue = value;
        mLocation = placeName;
        mCity = city;
        mDevId = devId;
        mCountry = country;
        mType = type;
        mIsSaved = isSaved;
    }

    public ModelItem(String parameter, String value, String heading, String subhead, String dateTime, boolean b) {
        mValue = value;
        mParameter = parameter;
        mHeading = heading;
        mSubHeading = subhead;
        mTimeStamp = dateTime;
    }

    public boolean getIsSaved() {
        return mIsSaved;
    }

    public String getDevId() {
        return mDevId;
    }

    public LinkedHashMap<String, String> getData() {
        return mData;
    }

    public String getType() {
        return mType;
    }

    public String getHeading() {
        return mHeading;
    }

    public String getSubHeading() {
        return mSubHeading;
    }

    public String getParameter() {
        return mParameter;
    }

    public String getUnit() {
        return mUnit;
    }

    public Drawable getIcon() {
        return mIcon;
    }

    public String getTimeStamp() {
        return mTimeStamp;
    }

    public String getCityId() {
        return mCityId;
    }

    public String getValue() {
        return mValue;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getCountry() {
        return mCountry;
    }

    public String getCity() {
        return mCity;
    }

    public String getTemp() {
        return mTemp;
    }

    public String getHumi() {
        return mHumi;
    }

    public String getImg() {
        return mImg;
    }

    public String getOption() {
        return mOption;
    }
}
