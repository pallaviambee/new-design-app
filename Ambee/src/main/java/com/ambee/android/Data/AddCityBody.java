package com.ambee.android.Data;

public class AddCityBody {
    private String placeId;
    private String name;

    public AddCityBody(String stationId, String name) {
        this.placeId = stationId;
        this.name = name;
    }
}
