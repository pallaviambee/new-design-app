package com.ambee.android.Data;

import java.util.List;

public class AddFeedbackBody {
    private String placeId;
    private String rating;
    private List<String> tags;
    private String image;

    public AddFeedbackBody(String placeId, String rating,List<String> tags,String img) {
        this.placeId = placeId;
        this.rating = rating;
        this.tags = tags;
        this.image = img;
    }
}
