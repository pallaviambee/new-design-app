package com.ambee.android.Data;

public class RankingData {
    private String name;
    private String value;
    private String image;


    public RankingData(String name, String value, String image) {
        this.name = name;
        this.value = value;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getValue() {
        return value;
    }
}
