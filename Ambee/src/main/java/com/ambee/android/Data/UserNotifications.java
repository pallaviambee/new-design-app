package com.ambee.android.Data;

import java.util.LinkedHashMap;
import java.util.List;

public class UserNotifications {
    private String message;

    private List<LinkedHashMap<String, String>> data;

    public UserNotifications(String message, List<LinkedHashMap<String, String>> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<LinkedHashMap<String, String>> getData() {
        return data;
    }
}
