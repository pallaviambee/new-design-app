package com.ambee.android.Data;

import java.util.LinkedHashMap;
import java.util.List;

public class AnalyticsData {
    private String message;
    private String dataType;
    private List<LinkedHashMap<String, String>> data;

    public AnalyticsData(String message,String dataType, List<LinkedHashMap<String, String>> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<LinkedHashMap<String, String>> getData() {
        return data;
    }

    public String getDataType() {
        return dataType;
    }
}
