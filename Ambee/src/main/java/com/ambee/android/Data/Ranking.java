package com.ambee.android.Data;

import java.util.List;

public class Ranking {
    private String message;
    private List<RankingData> data;

    public Ranking(String message, List<RankingData> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<RankingData> getData() {
        return data;
    }
}
