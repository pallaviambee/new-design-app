package com.ambee.android.Data;

import java.util.List;

public class AddGuestBody {
    private String name;
    private String gender;
    private String city;

    public AddGuestBody(String name, String gender, String city) {
        this.name = name;
        this.gender = gender;
        this.city = city;
    }
}
