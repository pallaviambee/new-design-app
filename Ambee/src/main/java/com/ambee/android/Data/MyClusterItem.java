package com.ambee.android.Data;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyClusterItem implements ClusterItem {
    private final LatLng mPosition;
    private double mLat;
    private double mLng;
    private String mTitle;
    private String mSnippet;
    private Bitmap mBitmap;
    private String mMsg;

    public MyClusterItem(double lat, double lng, String title, String snippet, Bitmap bitmap,String msg) {
        mPosition = new LatLng(lat, lng);
        mLat = lat;
        mLng = lng;
        mTitle = title;
        mSnippet = snippet;
        mBitmap = bitmap;
        mMsg = msg;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public double getLat() {
        return mLat;
    }

    public double getLng() {
        return mLng;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getSnippet() {
        return mSnippet;
    }

    public Bitmap getBitMap() {
        return mBitmap;
    }

    public String getMsg() {
        return mMsg;
    }
}
