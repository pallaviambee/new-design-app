package com.ambee.android.Data;

import com.google.gson.annotations.SerializedName;

public class UserLogin {
    @SerializedName("message")
    private String mMessage;
    @SerializedName("token")
    private UserToken mToken;

    public UserLogin(String message, UserToken token) {
        mMessage = message;
        mToken = token;
    }

    public String getMessage() {
        return mMessage;
    }

    public UserToken getToken() {
        return mToken;
    }

}
