package com.ambee.android.Data;

import com.google.gson.annotations.SerializedName;

public class UserRefreshToken {
    @SerializedName("message")
    private String mMessage;
    @SerializedName("userId")
    private String mUserID;
    @SerializedName("token")
    private String mToken;

    public UserRefreshToken(String message, String userID, String token) {
        mMessage = message;
        mUserID = userID;
        mToken = token;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getUserID() {
        return mUserID;
    }

    public String getToken() {
        return mToken;
    }
}