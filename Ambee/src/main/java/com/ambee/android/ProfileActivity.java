package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.RecommendationAdapter;
import com.ambee.android.Data.AnalyticsData;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;

public class ProfileActivity extends BaseActivity implements OnChartValueSelectedListener {
    private Context mContext;
    private ImageView mPic, mSetting;
    private TextView mName, mEmail, mPlace, mValueTxt, mUnitTxt, mStatusTxt;
    private LinearLayout mSavedPlaceLay;
    private BarChart mBarChart;
    private TextView mHealthValue1, mHealthValue2;
    private LinearLayout mMainLay;
    private ImageView mBgIcon;
    private TextView mNoSavedPlaces;
    // below navigation
    private LinearLayout mHomeLayout, mMapLayout, mRankLayout, mProfileLayout;
    private ImageView mHomeImg, mMapImg, mRankImg, mProfileImg;
    private TextView mHomeTxt, mMapText, mRankText, mProfileText;

    // Add list for saved stations
    private List<ModelItem> mDataList = new ArrayList<>();

    // For recommendation
    private ListView mListRecommendation;
    private List<ModelItem> mRecommendationItems = new ArrayList<>();
    private RecommendationAdapter mRecommendationAdapter;
    private String mAddress = "";
    private String mPlaceName = "";
    private String mFilterDistance = "250";
    private String mFilterStr = "all";
    // for analytics duration = h(hours) , duration=d(day),duration=m(month)
    private String duration = "d";
    // Map data <Parameter(para + "_" + date),<value,date>>
    private LinkedHashMap<String, LinkedHashMap<Float, String>> mGraphDataMap =
            new LinkedHashMap<>();
    // set graph data
    private String mSelectedPara = "PM 2.5";
    private String mSelectedUnit = "ug/m3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mContext = getApplicationContext();
        initUi();
        String name = AccountUtil.getName(mContext);
        String email = AccountUtil.getEmail(mContext);
        String url = AccountUtil.getUserPic(mContext);

        if (!name.isEmpty()) {
            mName.setText(name.substring(0, 1).toUpperCase() + name.substring(1));
        }

        //   mEmail.setText(email);
        new BaseActivity.ImageLoadTask(url, mPic).execute();
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            // for nearest place
            getNearestLocation();
            // get saved places
            getSavedPlaces();
        }

        mSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, SettingActivity.class);
                startActivity(intent);
            }
        });

        setBelowNavigation();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            getNearestLocation();
            // get clear places
            getSavedPlaces();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void getSavedPlaces() {
        mDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStations("Bearer " + AccountUtil.getToken(mContext));
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userSavedPlace = response.body();
                    if (userSavedPlace != null) {
                        List<LinkedHashMap<String, String>> stations = userSavedPlace.getStations();
                        if (stations != null && stations.size() > 0) {
                            for (int i = 0; i < stations.size(); i++) {
                                String para = "";
                                String value = "";
                                String placeName = "";
                                String city = "";
                                String country = "";
                                String cityId = "";
                                LinkedHashMap<String, String> hashMap = stations.get(i);
                                if (hashMap.containsKey("placeId")) {
                                    cityId = hashMap.get("placeId");
                                }
                                if (hashMap.containsKey("name")) {
                                    placeName = hashMap.get("name");
                                }
                                if (placeName.isEmpty()) {
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }

                                if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("OZONE")) {
                                    para = "O3";
                                    value = getValue(hashMap.get("OZONE"));
                                } else if (hashMap.containsKey("CO")) {
                                    para = "CO";
                                    value = getValue(hashMap.get("CO"));
                                } else if (hashMap.containsKey("NO2")) {
                                    para = "NO2";
                                    value = getValue(hashMap.get("NO2"));
                                } else if (hashMap.containsKey("SO2")) {
                                    para = "SO2";
                                    value = getValue(hashMap.get("SO2"));
                                } else if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                }
                                mDataList.add(new ModelItem(para, value, placeName, city, cityId, country, "Place"));
                            }
                        }
                    }
                    if (mDataList != null && mDataList.size() > 0) {
                        mNoSavedPlaces.setVisibility(View.GONE);
                        addSavedPlaceView();
                    } else
                        mNoSavedPlaces.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void addSavedPlaceView() {
        mSavedPlaceLay.removeAllViews();
        for (int i = 0; i < mDataList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(ProfileActivity.this);
            final View view = inflater.inflate(R.layout.item_cleaner_details,
                    mSavedPlaceLay, false);
            TextView placeTxt = (TextView) view.findViewById(R.id.place);
            CardView main = (CardView) view.findViewById(R.id.card);
            final ImageView img = (ImageView) view.findViewById(R.id.fav);
            TextView addressTxt = (TextView) view.findViewById(R.id.address);
            LinearLayout mainLayout = (LinearLayout) view.findViewById(R.id.main);
            TextView valueTxt = (TextView) view.findViewById(R.id.value);
            TextView unitTxt = (TextView) view.findViewById(R.id.unit);
            TextView statusTxt = (TextView) view.findViewById(R.id.status);

            final String placeName = mDataList.get(i).getLocation();
            String city = mDataList.get(i).getCity();
            final String cityId = mDataList.get(i).getDevId();
            String country = mDataList.get(i).getCountry();
            String para = mDataList.get(i).getParameter();
            String value = mDataList.get(i).getValue();
            final boolean isSaved = mDataList.get(i).getIsSaved();
            img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_heart));

            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  /*  if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken(isSaved, "Action");
                    } else {
                        if (isSaved) {
                            deletePopup(cityId, placeName, "Place");
                        } else
                            addPlace();
                    }*/
                }
            });
            placeTxt.setText(placeName);
            addressTxt.setText(city + ", " + country);
            String unit = ParameterUtil.getParameterUnit(mContext, para);
            unitTxt.setText(unit);

            main.setTag(i);
            main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ProfileActivity.this,
                            HomeDetailsActivity.class);
                    intent.putExtra("IsSaved", isSaved);
                    intent.putExtra("CityId",
                            mDataList.get(Integer.parseInt(v.getTag().toString())).getDevId());
                    intent.putExtra("IsFromHome", true);
                    startActivity(intent);
                }
            });
            if (value != null && !value.isEmpty()) {
                // convert server data to user understanding data
                double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
                String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
                valueTxt.setText(String.valueOf(paraVal));
                switch (colour) {
                    case "green":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                        statusTxt.setText("Good");
                        break;
                    case "orange":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                        statusTxt.setText("Mediocre");
                        break;
                    case "red":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                        statusTxt.setText("Dangerous");
                        break;
                    case "black":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                        statusTxt.setText("Catastrophic");
                        break;
                    default:
                        break;
                }
            }

            main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //  Toast.makeText(mContext, "Show details", Toast.LENGTH_SHORT).show();
                }
            });
            mSavedPlaceLay.addView(view);
        }
    }


    // check and get token after every 5 min
    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            // get nearest location
                            getNearestLocation();
                            // get saved places
                            getSavedPlaces();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getNearestLocation() {
        if (HomeActivity.mCurrentLatitude == null && HomeActivity.mCurrentLongitude == null) {
            return;
        }
        if (HomeActivity.mCurrentLatitude.isEmpty() && HomeActivity.mCurrentLongitude.isEmpty()) {
            return;
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            getAddress();
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + AccountUtil.getToken(mContext),
                            Double.parseDouble(HomeActivity.mCurrentLatitude), Double.parseDouble(HomeActivity.mCurrentLongitude),
                            mPlaceName, mFilterDistance, "1", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    UserStations userSearchCity = response.body();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        String cityId = "";
                        String para = "";
                        String value = "";
                        String placeName = "";
                        String city = "";
                        String country = "";
                        if (stations != null && stations.size() > 0) {
                            LinkedHashMap<String, String> hashMap = stations.get(0);
                            if (hashMap != null) {
                                if (hashMap.containsKey("placeId")) {
                                    cityId = hashMap.get("placeId");
                                }
                                if (hashMap.containsKey("placeName")) {
                                    placeName = hashMap.get("placeName");
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }
                                if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("OZONE")) {
                                    para = "O3";
                                    value = getValue(hashMap.get("OZONE"));
                                } else if (hashMap.containsKey("CO")) {
                                    para = "CO";
                                    value = getValue(hashMap.get("CO"));
                                } else if (hashMap.containsKey("NO2")) {
                                    para = "NO2";
                                    value = getValue(hashMap.get("NO2"));
                                } else if (hashMap.containsKey("SO2")) {
                                    para = "SO2";
                                    value = getValue(hashMap.get("SO2"));
                                } else if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                }
                                mSelectedPara = para;
                            }
                            setStatus(para, value, placeName, city, country);
                            getLatestData(cityId);
                        } else {

                        }
                    }

                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    // id = device id or station id for getting details of data
    private void getLatestData(final String id) {
        // mGraphDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<LatestData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationLatestData("Bearer " +
                            AccountUtil.getToken(mContext), id);
            call.enqueue(new Callback<LatestData>() {
                @Override
                public void onResponse(Call<LatestData> call, Response<LatestData> response) {
                    LatestData latestData = response.body();
                    if (latestData != null) {
                        LinkedHashMap<String, String> hashMap = latestData.getData();
                        if (hashMap != null && hashMap.size() > 0) {
                            setRecommendation(hashMap, latestData.getDataType());
                            setHealthEffect(hashMap, latestData.getDataType());
                            getGraphData(id);
                        }
                    }
                }

                @Override
                public void onFailure(Call<LatestData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getGraphData(String id) {
        mGraphDataMap = new LinkedHashMap<>();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<AnalyticsData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationAnalytics("Bearer " +
                            AccountUtil.getToken(mContext), id, duration);

            call.enqueue(new Callback<AnalyticsData>() {
                @Override
                public void onResponse(Call<AnalyticsData> call, Response<AnalyticsData> response) {
                    AnalyticsData graphData = response.body();
                    if (graphData != null) {
                        List<LinkedHashMap<String, String>> graphDataList = graphData.getData();
                        if (graphDataList != null) {
                            mGraphDataMap = new LinkedHashMap<>();
                            for (int i = 0; i < graphDataList.size(); i++) {
                                LinkedHashMap<String, String> hashMap = graphDataList.get(i);
                                if (hashMap != null && hashMap.size() > 0) {
                                    for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                                        String para = "";
                                        String key = entry.getKey();
                                        String val = entry.getValue();
                                        String date = hashMap.get("dateTime");
                                        switch (key) {
                                            case "PM25":
                                                para = "PM 2.5";
                                                val = getValue(val);
                                                break;
                                            case "PM10":
                                                para = "PM 10";
                                                val = getValue(val);
                                                break;
                                            case "PM1":
                                                para = "PM 1";
                                                val = getValue(val);
                                                break;
                                            case "CO2":
                                                para = "CO2";
                                                val = getValue(val);
                                                break;
                                            case "TVOC":
                                                para = "TVOC";
                                                val = getValue(val);
                                                break;
                                            case "HCHO":
                                                para = "HCHO";
                                                val = getValue(val);
                                                break;
                                            case "OZONE":
                                                para = "O3";
                                                val = getValue(val);
                                                break;
                                            case "CO":
                                                para = "CO";
                                                val = getValue(val);
                                                break;
                                            case "NO2":
                                                para = "NO2";
                                                val = getValue(val);
                                                break;
                                            case "SO2":
                                                para = "SO2";
                                                val = getValue(val);
                                                break;
                                            case "AQI":
                                                para = "AQI";
                                                val = getValue(val);
                                                break;
                                            default:
                                                break;
                                        }
                                        if (!para.isEmpty() && para.length() > 0) {
                                            double value = ParameterUtil.getParameterValue(mContext, para, val);
                                            LinkedHashMap<Float, String> dataMap = new LinkedHashMap<>();
                                            dataMap.put((float) value, date);
                                            if (mGraphDataMap != null && mGraphDataMap.containsKey(para)) {
                                                LinkedHashMap<Float, String> tempHashMap = mGraphDataMap.get(para);
                                                mGraphDataMap.put(para + "_" + date, tempHashMap);
                                            } else {
                                                mGraphDataMap.put(para + "_" + date, dataMap);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        setGraphData();
                    }
                }

                @Override
                public void onFailure(Call<AnalyticsData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setGraphData() {
        // Get Events on bar chart(BarEntry)
        ArrayList<BarEntry> values = new ArrayList<BarEntry>();
        values.clear();

        // for entry data on bar chart
        LinkedHashMap<String, Float> eventsCount = new LinkedHashMap<>();
        eventsCount.clear();
        // to set label xAxis
        final ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.clear();

        for (Map.Entry<String, LinkedHashMap<Float, String>> data : mGraphDataMap.entrySet()) {
            String parameter = data.getKey().split("_")[0];
            if (parameter.equals(mSelectedPara)) {
                LinkedHashMap<Float, String> value = data.getValue();
                for (Map.Entry<Float, String> data1 : value.entrySet()) {
                    Float parValue = data1.getKey();
                    String time_date = data1.getValue();
                    String time = time_date.split(",")[0];
                    String date = time_date.split(",")[1];
                    String yLabel = date.split("-")[0]
                            + "-" + date.split("-")[1];
                    if (eventsCount != null && eventsCount.containsKey(yLabel)) {
                        eventsCount.put(yLabel, parValue);
                    } else {
                        eventsCount.put(yLabel, parValue);
                    }
                    if (xAxisLabel != null && !xAxisLabel.contains(yLabel)) {
                        xAxisLabel.add(yLabel);
                    }
                }
            }
        }

        for (int i = 0; i < xAxisLabel.size(); i++) {
            values.add(new BarEntry(i, eventsCount.get(xAxisLabel.get(i))));
        }

        mBarChart.setOnChartValueSelectedListener(ProfileActivity.this);
        mBarChart.setDrawBarShadow(false);
        mBarChart.setDrawValueAboveBar(true);
        mBarChart.getDescription().setEnabled(false);
        mBarChart.setDoubleTapToZoomEnabled(false);
        // scaling can now only be done on x- and y-axis separately
        mBarChart.setPinchZoom(false);
        mBarChart.setDrawGridBackground(false);
        mBarChart.setScaleEnabled(false);
        // mBarChart.setDrawYLabels(false);

        // custom xAxis
        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        //  xAxis.setTypeface(mTf);
        xAxis.setTextColor(getApplicationContext().getResources().getColor(R.color.heading_color));
        xAxis.setTextSize(12f);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(false);
        xAxis.setGranularityEnabled(true);
        xAxis.setLabelCount(values.size() + 1, false);
        xAxis.setValueFormatter(new XaxisValueFormatter(xAxisLabel, "Day"));

        // custom yAxis left
        YAxis yleft = mBarChart.getAxisLeft();
        // yleft.setTypeface(mTf);
        // setting upper and lower bound of yaxis
        yleft.setLabelCount(values.size() + 1, false);
        yleft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        yleft.setSpaceTop(15f);
        yleft.setTextColor(getApplicationContext().getResources().getColor(R.color.heading_color));
        yleft.setTextSize(12f);
        yleft.setDrawAxisLine(false);
        yleft.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        // custom yAxis right
        YAxis yRight = mBarChart.getAxisRight();
        yRight.setEnabled(false);

        Legend l = mBarChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

        mSelectedUnit = ParameterUtil.getParameterUnit(mContext, mSelectedPara);
        // set marker view to show data
        XYMarkerView mv = new XYMarkerView(getApplicationContext(), new XaxisValueFormatter(xAxisLabel, "Day"),
                "Location", mSelectedUnit);
        mv.setChartView(mBarChart);
        mBarChart.setMarker(mv);

        // set value on axis
        BarDataSet dataSet;

        dataSet = new BarDataSet(values, "");
        dataSet.setColor(ColorTemplate.rgb("#00C4FF"));
        dataSet.setDrawValues(false);
        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(dataSet);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        //data.setValueTypeface(mTf);
        data.setBarWidth(0.9f);
        mBarChart.setFitBars(true);
        mBarChart.setData(data);
        mBarChart.getData().notifyDataChanged();
        mBarChart.notifyDataSetChanged();
        mBarChart.animateY(2000);
    }

    private void setHealthEffect(LinkedHashMap<String, String> latestData, String type) {
        String paraTxt = "", valueTxt = "";
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String para = entry.getKey();
            String value = entry.getValue();
            switch (type) {
                case "device":
                    if (para.equals("PM25")) {
                        paraTxt = "PM 2.5";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("PM10")) {
                        paraTxt = "PM 10";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("PM1")) {
                        paraTxt = "PM 1";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("CO2")) {
                        paraTxt = "CO2";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("TVOC")) {
                        paraTxt = "TVOC";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("HCHO")) {
                        paraTxt = "HCHO";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("AQI")) {
                        paraTxt = "AQI";
                        valueTxt = getValue(value);
                    }
                    break;
                case "station":
                    if (para.equals("PM25")) {
                        paraTxt = "PM 2.5";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("PM10")) {
                        paraTxt = "PM 10";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("OZONE")) {
                        paraTxt = "O3";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("CO")) {
                        paraTxt = "CO";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("NO2")) {
                        paraTxt = "NO2";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("SO2")) {
                        paraTxt = "SO2";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("AQI")) {
                        paraTxt = "AQI";
                        valueTxt = getValue(value);
                    }
                    break;
            }
        }
        if (paraTxt != null && paraTxt.length() > 0) {
            mHealthValue1.setText(valueTxt);
            mHealthValue2.setText(valueTxt);
        }
    }

    private void setRecommendation(LinkedHashMap<String, String> latestData, String type) {
        mListRecommendation.setFocusable(false);
        mRecommendationItems.clear();
        String para = "", paraValue = "";
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Drawable icon = null;
            switch (type) {
                case "device":
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM1")) {
                        para = "PM 1";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO2")) {
                        para = "CO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("TVOC")) {
                        para = "TVOC";
                        paraValue = getValue(value);
                    }
                    if (key.equals("HCHO")) {
                        para = "HCHO";
                        paraValue = getValue(value);
                    }
                    if (para.equals("AQI")) {
                        para = "AQI";
                        value = getValue(value);
                    }
                    break;
                case "station":
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("OZONE")) {
                        para = "O3";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO")) {
                        para = "CO";
                        paraValue = getValue(value);
                    }
                    if (key.equals("NO2")) {
                        para = "NO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("SO2")) {
                        para = "SO2";
                        paraValue = getValue(value);
                    }
                    if (para.equals("AQI")) {
                        para = "AQI";
                        value = getValue(value);
                    }
                    break;
            }
        }
        // set background image
        if (paraValue != null && !paraValue.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, paraValue);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.good_exercise),
                            "Safe for exercising and heavy physical activities",
                            "Clean air quality in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.no_purifier),
                            "No air purifier required",
                            "Good air quality detected in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Enjoy the clean air", "No safety precautions required"));
                    break;
                case "orange":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Mediocre pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier if possible",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Exercise caution in outdoors",
                            "Reduces exposure to poor air quality"));
                    break;
                case "red":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks when outdoor",
                            "Reduce exposure to poor air quality"));
                    break;
                case "black":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Avoid all outdoor activities",
                            "Catastrophic pollution levels detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks extensively",
                            "Reduce exposure to poor air quality"));
                    break;
                default:
                    break;
            }
            if (mRecommendationItems != null && mRecommendationItems.size() > 0) {
                mRecommendationAdapter = new RecommendationAdapter(mContext, mRecommendationItems);
                mListRecommendation.setAdapter(mRecommendationAdapter);
            } else {

            }
        }
    }

    private void setStatus(String para, String value, String placeName, String city, String
            country) {
        // convert server data to user understanding data
        double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
        // get unit of parameter
        String unit = ParameterUtil.getParameterUnit(mContext, para);
        mValueTxt.setText(String.valueOf(paraVal));
        mUnitTxt.setText(unit);
        mPlace.setText(placeName + "\n" + city + ", " + country);
        // set background image
        if (value != null && !value.isEmpty()) {
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_bg_icon));
                    mStatusTxt.setText("Good");
                    break;
                case "orange":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                    mStatusTxt.setText("Mediocre");
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_bg_icon));
                    mBgIcon.setColorFilter(mContext.getResources().getColor(R.color.orange_bg_icon));
                    break;
                case "red":
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.black_bg_icon));
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                    mBgIcon.setColorFilter(mContext.getResources().getColor(R.color.red_bg_icon));
                    mStatusTxt.setText("Dangerous");
                    break;
                case "black":
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.black_bg_icon));
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                    mStatusTxt.setText("Catastrophic");
                    break;
                default:
                    break;
            }
        }
    }

    private void getAddress() {
        Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(HomeActivity.mCurrentLatitude),
                    Double.parseDouble(HomeActivity.mCurrentLongitude), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            mAddress = addresses.get(0).getAddressLine(0);
            mPlaceName = addresses.get(0).getFeatureName() + "," +
                    addresses.get(0).getSubLocality();
        } else {
            // do your stuff
        }
    }

    private void setBelowNavigation() {
        mProfileLayout.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        mProfileImg.setColorFilter(mContext.getResources().getColor(R.color.white));
        mProfileText.setVisibility(View.VISIBLE);
        mProfileText.setTextColor(mContext.getResources().getColor(R.color.white));
        mHomeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mMapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ProfileActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mProfileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void initUi() {
        mPic = (ImageView) findViewById(R.id.account_imageview);
        mSetting = (ImageView) findViewById(R.id.settings);
        mName = (TextView) findViewById(R.id.user_name);
        mPlace = (TextView) findViewById(R.id.user_place);
        mValueTxt = (TextView) findViewById(R.id.value);
        mUnitTxt = (TextView) findViewById(R.id.unit);
        mStatusTxt = (TextView) findViewById(R.id.status);
        mBarChart = (BarChart) findViewById(R.id.barChart);
        mHealthValue1 = (TextView) findViewById(R.id.value1);
        mHealthValue2 = (TextView) findViewById(R.id.value2);
        mSavedPlaceLay = (LinearLayout) findViewById(R.id.savedPlacesLay);
        mListRecommendation = (ListView) findViewById(R.id.recomList);
        mHomeLayout = (LinearLayout) findViewById(R.id.home);
        mHomeImg = (ImageView) findViewById(R.id.homeImg);
        mHomeTxt = (TextView) findViewById(R.id.homeTxt);
        mMainLay = (LinearLayout) findViewById(R.id.main);
        mBgIcon = (ImageView) findViewById(R.id.bg_icon);
        mNoSavedPlaces = (TextView) findViewById(R.id.noSavedPlaces);
        mMapLayout = (LinearLayout) findViewById(R.id.map);
        mMapImg = (ImageView) findViewById(R.id.mapImg);
        mMapText = (TextView) findViewById(R.id.mapTxt);

        mRankLayout = (LinearLayout) findViewById(R.id.rank);
        mRankImg = (ImageView) findViewById(R.id.rankImg);
        mRankText = (TextView) findViewById(R.id.rankTxt);

        mProfileLayout = (LinearLayout) findViewById(R.id.profile);
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        mProfileText = (TextView) findViewById(R.id.profileTxt);
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
