package com.ambee.android;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;

public class XaxisValueFormatter implements IAxisValueFormatter {
    private ArrayList<String> labels = new ArrayList<>();

    public XaxisValueFormatter(ArrayList<String> labels, String string) {
        this.labels = labels;
    }

    @Override
    public String getFormattedValue(float value, AxisBase axis) {

        String lable = "";
        int index = (int) value;
        if (index < labels.size() && index >= 0) {
            lable = labels.get(index);
        }
        return lable;
    }

}
