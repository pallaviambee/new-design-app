package com.ambee.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambee.android.Data.ModelItem;
import com.ambee.android.R;

import java.util.List;

public class RecommendationAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<ModelItem> data;
    private ModelItem mItemList;

    public RecommendationAdapter(Context context, List<ModelItem> modelList) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            view = inflater.inflate(R.layout.item_recommendation, null);
            holder = new ViewHolder();
            holder.mIcon = (ImageView) view.findViewById(R.id.icon);
            holder.mHeading = (TextView) view.findViewById(R.id.heading);
            holder.mSubHeading = (TextView) view.findViewById(R.id.text);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            mItemList = null;
            mItemList = (ModelItem) data.get(position);

            holder.mIcon.setBackground(mItemList.getIcon());
            holder.mHeading.setText(mItemList.getHeading());
            holder.mSubHeading.setText(mItemList.getSubHeading());
        } else {

        }
        return view;

    }

    public static class ViewHolder {
        public ImageView mIcon;
        public TextView mHeading;
        public TextView mSubHeading;
    }

}
