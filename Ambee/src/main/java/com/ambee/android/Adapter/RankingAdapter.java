package com.ambee.android.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ambee.android.BaseActivity;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.R;
import com.ambee.android.Util.ParameterUtil;

import java.util.List;

public class RankingAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<ModelItem> data;
    private ModelItem mItemList;

    public RankingAdapter(Context context, List<ModelItem> modelList) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;

        if (convertView == null) {
            view = inflater.inflate(R.layout.item_ranking, null);
            holder = new ViewHolder();
            holder.mName = (TextView) view.findViewById(R.id.place);
            holder.mImg = (ImageView) view.findViewById(R.id.img);
            holder.mValue = (TextView) view.findViewById(R.id.value);
            holder.mStatus = (TextView) view.findViewById(R.id.status);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            mItemList = null;
            mItemList = (ModelItem) data.get(position);
            String value = mItemList.getValue();
            String option = mItemList.getOption();
            String name = mItemList.getLocation();
            String imgUrl = mItemList.getImg();

            holder.mName.setText(name);
            holder.mValue.setText(value);
            String type = mItemList.getType();

            new BaseActivity.ImageLoadTask(imgUrl, holder.mImg).execute();
            String colour = ParameterUtil.getParameterColour(mContext, "PM 2.5", Double.valueOf(value));
            switch (colour) {
                case "green":
                    holder.mValue.setTextColor(mContext.getResources().getColor(R.color.green_s));
                    holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.green_s));
                    holder.mStatus.setText("Good");
                    break;
                case "orange":
                    holder.mValue.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                    holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                    holder.mStatus.setText("Mediocre");
                    break;
                case "red":
                    holder.mValue.setTextColor(mContext.getResources().getColor(R.color.red_s));
                    holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.red_s));
                    holder.mStatus.setText("Dangerous");
                    break;
                case "black":
                    holder.mValue.setTextColor(mContext.getResources().getColor(R.color.black_s));
                    holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.black_s));
                    holder.mStatus.setText("Catastrophic");
                    break;
                default:
                    break;
            }
        } else {

        }
        return view;

    }

    public static class ViewHolder {
        public TextView mName;
        public TextView mValue;
        public TextView mStatus;
        public ImageView mImg;
    }

}
