package com.ambee.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.R;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.ParameterUtil;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<ModelItem> data;
    private ModelItem mItemList;
    private Activity mActivity;


    public NotificationAdapter(Activity activity, Context context, List<ModelItem> modelList) {
        mContext = context;
        mActivity = activity;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
    }


    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public LinearLayout mMainLayout;
        public TextView mValue;
        public TextView mUnit;
        public TextView mStatus;
        public TextView mTitle;
        public TextView mSubTitle;
        public LinearLayout mMain;
        public TextView mDateTime;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_notification, null);
            holder.mMainLayout = (LinearLayout) view.findViewById(R.id.content);
            holder.mMain = (LinearLayout) view.findViewById(R.id.main);
            holder.mValue = (TextView) view.findViewById(R.id.value);
            holder.mUnit = (TextView) view.findViewById(R.id.unit);
            holder.mStatus = (TextView) view.findViewById(R.id.status);
            holder.mTitle = (TextView) view.findViewById(R.id.title);
            holder.mSubTitle = (TextView) view.findViewById(R.id.subTitle);
            holder.mDateTime = (TextView) view.findViewById(R.id.date);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            holder.mMainLayout.setVisibility(View.VISIBLE);
            mItemList = null;
            mItemList = data.get(position);
            String value = mItemList.getValue();
            String title = mItemList.getHeading();
            String subTitle = mItemList.getSubHeading();
            String para = mItemList.getParameter();
            String dateTime = mItemList.getTimeStamp();
            holder.mDateTime.setText(dateTime);

            double paraVal = ParameterUtil.getParameterValue(mContext, "PM 2.5", value);
            // get unit of parameter
            String unit = ParameterUtil.getParameterUnit(mContext, para);
            holder.mValue.setText(String.valueOf(paraVal));
            holder.mUnit.setText(unit);
            holder.mTitle.setText(title);
            holder.mSubTitle.setText(subTitle);
            if (value != null && !value.isEmpty()) {
                String colour = ParameterUtil.getParameterColour(mContext, "PM 2.5", paraVal);
                switch (colour) {
                    case "green":
                        holder.mValue.setTextColor(mContext.getResources().getColor(R.color.green_s));
                        holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.green_s));
                        holder.mStatus.setText("Good");
                        break;
                    case "orange":
                        holder.mValue.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                        holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                        holder.mStatus.setText("Mediocre");
                        break;
                    case "red":
                        holder.mValue.setTextColor(mContext.getResources().getColor(R.color.red_s));
                        holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.red_s));
                        holder.mStatus.setText("Dangerous");
                        break;
                    case "black":
                        holder.mValue.setTextColor(mContext.getResources().getColor(R.color.black_s));
                        holder.mStatus.setTextColor(mContext.getResources().getColor(R.color.black_s));
                        holder.mStatus.setText("Catastrophic");
                        break;
                    default:
                        break;
                }
            }

        } else {
            holder.mMainLayout.setVisibility(View.GONE);
            Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();
        }
        return view;
    }


    // check and get token after every 5 min
    public void checkToken(final String placeName, final String cityId, final int pos) {
        Call<UserRefreshToken> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
        // to execute the HTTP call
        call.enqueue(new Callback<UserRefreshToken>() {
            @Override
            public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                UserRefreshToken userRefreshToken = response.body();
                if (userRefreshToken != null) {
                    if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                        // set token
                        AccountUtil.setToken(mContext, userRefreshToken.getToken());
                        // set current time
                        AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                    }
                }

            }

            @Override
            public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    void isToday(String timeStamp, TextView separator) {
        Calendar c = Calendar.getInstance();
        // set the calendar to start of today
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        // and get that as a Date
        Date today = c.getTime();
        // or as a timestamp in milliseconds
        long todayInMillis = c.getTimeInMillis();
        // user-specified date which you are testing
        // let's say the components come from a form or something
        int year = 2019;
        int month = 11;
        int dayOfMonth = 5;
        // reuse the calendar to set user specified date
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        // and get that as a Date
        Date dateSpecified = c.getTime();
        // test your condition
        if (dateSpecified.equals(today)) {
            separator.setVisibility(View.VISIBLE);
        } else {
            separator.setVisibility(View.GONE);
        }
    }
}
