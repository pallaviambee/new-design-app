package com.ambee.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ambee.android.R;

import java.util.List;

public class RewardsAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<String> data;
    private Activity mActivity;


    public RewardsAdapter(Activity activity, Context context, List<String> modelList) {
        mContext = context;
        mActivity = activity;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
    }


    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public LinearLayout mMainLayout;
        public ImageView mImg;
        public TextView mValue;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_rewards, null);
            holder.mMainLayout = (LinearLayout) view.findViewById(R.id.main_layout);
            holder.mImg = (ImageView) view.findViewById(R.id.info);
            holder.mValue = (TextView) view.findViewById(R.id.value);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            holder.mMainLayout.setVisibility(View.VISIBLE);
            holder.mValue.setText(data.get(position));
        } else {
            holder.mMainLayout.setVisibility(View.GONE);
            //  Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();
        }
        return view;
    }
}
