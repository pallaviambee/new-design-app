package com.ambee.android.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.RenameCityBody;
import com.ambee.android.Data.RenameDeviceBody;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.R;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<ModelItem> data;
    private ModelItem mItemList;
    private AlertDialog mAlertDia;
    private Activity mActivity;

    public HomeAdapter(Activity activity, Context context, List<ModelItem> modelList) {
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_home, null);
            holder.mContent = (LinearLayout) view.findViewById(R.id.content);
            holder.mPlace = (TextView) view.findViewById(R.id.place);
            holder.mAddress = (TextView) view.findViewById(R.id.address);
            holder.mEdit = (ImageView) view.findViewById(R.id.edit);
            holder.mMainLay = (LinearLayout) view.findViewById(R.id.main);
            holder.mValue = (TextView) view.findViewById(R.id.value);
            holder.mUnit = (TextView) view.findViewById(R.id.unit);
            holder.mStatus = (TextView) view.findViewById(R.id.status);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            mItemList = null;
            mItemList = data.get(position);
            String para = mItemList.getParameter();
            String value = mItemList.getValue();
            final String placeName = mItemList.getLocation();
            String city = mItemList.getCity();
            final String cityId = mItemList.getDevId();
            String country = mItemList.getCountry();
            final String type = mItemList.getType();

            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            // get unit of parameter
            String unit = ParameterUtil.getParameterUnit(mContext, para);
            holder.mPlace.setText(placeName);
            holder.mAddress.setText(city + ", " + country);
            holder.mValue.setText(String.valueOf(paraVal));
            holder.mUnit.setText(unit);

            // set background image
            if (value != null && !value.isEmpty()) {
                /*get color :
                 * Good - green
                 * Moderate - yellow
                 * unhealthy - red
                 * very unhealthy - brown
                 * hazardous -  purple */
                String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
                switch (colour) {
                    case "green":
                        holder.mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                        holder.mStatus.setText("Good");
                        break;
                    case "orange":
                        holder.mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                        holder.mStatus.setText("Mediocre");
                        break;
                    case "red":
                        holder.mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                        holder.mStatus.setText("Dangerous");
                        break;
                    case "black":
                        holder.mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                        holder.mStatus.setText("Catastrophic");
                        break;
                    default:
                        break;
                }
            } else {
                holder.mContent.setVisibility(View.GONE);
            }
            holder.mEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog(cityId, placeName, position, type);
                }
            });
        } else {

        }
        return view;
    }

    private void showDialog(final String cityId, final String placeName, final int position, final String type) {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(mActivity);
        View promptsView = li.inflate(R.layout.prompts_edit, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        TextView rename = (TextView) promptsView.findViewById(R.id.rename);
        TextView delete = (TextView) promptsView.findViewById(R.id.delete);

        rename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                renamePopup(cityId, placeName, position, type);

            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                deletePopup(cityId, placeName, position, type);
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void renamePopup(final String cityId, String placeName, final int position, final String type) {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(mActivity);
        View promptsView = li.inflate(R.layout.prompts_rename, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        final EditText editText = (EditText) promptsView.findViewById(R.id.input);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                String text = editText.getText().toString();
                if (text != null && text.isEmpty()) {
                    Toast.makeText(mContext, "Please enter name", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (DateTimeUtil.checkTime(mContext))
                    // call for get token
                    checkToken("Rename", cityId, text, position, type);
                else
                    renameService(cityId, text, position, type);
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void deletePopup(final String cityId, String placeName, final int position, final String type) {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(mActivity);
        View promptsView = li.inflate(R.layout.prompts_delete, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                mActivity, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        heading.setText("Are you sure you want to remove" + "\n" + placeName
                + " from your saved places");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                //check time for token 5 min expire
                if (DateTimeUtil.checkTime(mContext)) {
                    // call for get token
                    checkToken("Rename", "Delete", cityId, position, type);
                } else {
                    deleteDeviceService(cityId, position, type);
                }
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    // check and get token after every 5 min
    public void checkToken(final String str, final String cityId, final String text, final int pos, final String type) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext),
                            AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call,
                                       Response<UserRefreshToken> response) {
                    if (response != null) {
                        UserRefreshToken userRefreshToken = response.body();
                        if (userRefreshToken != null) {
                            if (userRefreshToken.getToken() != null &&
                                    !userRefreshToken.getToken().isEmpty()) {
                                // set token
                                AccountUtil.setToken(mContext, userRefreshToken.getToken());
                                // set current time
                                AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                                if (str.equals("Delete")) {
                                    deleteDeviceService(cityId, pos, type);
                                } else if (str.equals("Rename")) {
                                    renameService(cityId, text, pos, type);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void renameService(String cityId, String name, int pos, final String type) {
        RenameCityBody renameCityBody = new RenameCityBody(name);
        RenameDeviceBody renameDeviceBody = new RenameDeviceBody(name);
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<ResponseBody> call = null;
            if (type.equals("Place")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .renameCity("Bearer " + AccountUtil.getToken(mContext),
                                renameCityBody, cityId);
            } else if (type.equals("Device")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .renameDevice("Bearer " + AccountUtil.getToken(mContext),
                                cityId, renameDeviceBody);
            }
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String msg = response.message();
                    if (response != null && response.code() == 200) {
                        Toast.makeText(mContext, "Rename successfully !! ", Toast.LENGTH_LONG).show();
                        notifyDataSetChanged();
                        mActivity.finish();
                    } else {
                        Toast.makeText(mContext, "Failed !! Please try again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void deleteDeviceService(String cityId, final int pos, final String type) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<ResponseBody> call = null;
            if (type.equals("Place")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteCity("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            } else if (type.equals("Device")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteDevice("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            }
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.code() == 200) {
                        Toast.makeText(mContext, "Deleted successfully", Toast.LENGTH_LONG).show();
                        if (data != null && data.size() > 0) {
                            data.remove(pos);
                            notifyDataSetChanged();
                            mActivity.finish();
                        }
                    } else {
                        Toast.makeText(mContext, "Not deleted, Please try again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    public static class ViewHolder {
        public LinearLayout mContent;
        public TextView mPlace;
        public TextView mAddress;
        public ImageView mEdit;
        public LinearLayout mMainLay;
        public TextView mValue;
        public TextView mUnit;
        public TextView mStatus;
    }

}
