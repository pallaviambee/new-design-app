package com.ambee.android.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ambee.android.R;
import com.ambee.android.Util.ParameterUtil;

import java.util.ArrayList;
import java.util.List;

public class TabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private Context mContext;
    private String mId;
    private int mPosition = 0;

    public TabAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        //  return mFragmentList.get(position);
        Fragment fragment = mFragmentList.get(position);
        String text = mFragmentTitleList.get(position);
        Bundle bundle = new Bundle();
        String value = text.split("_")[1];
        String para = text.split("_")[0];
        bundle.putString("SELECTED_PARA", para);
        bundle.putString("ID", mId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title, String id) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mId = id;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    public View getTabView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView valueTxt = view.findViewById(R.id.value);
        TextView paraTxt = view.findViewById(R.id.para);
        String text = mFragmentTitleList.get(position);
        String para = text.split("_")[0];
        String value = text.split("_")[1];
        switch (para) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = para;
                break;
        }
        double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
        paraTxt.setText(para);
        valueTxt.setText(String.valueOf(paraVal));
        return view;
    }

    public View getSelectedTabView(int position) {
        mPosition = position;
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView valueTxt = view.findViewById(R.id.value);
        TextView paraTxt = view.findViewById(R.id.para);
        String text = mFragmentTitleList.get(position);
        String value = text.split("_")[1];
        String para = text.split("_")[0];
        switch (para) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = para;
                break;
        }
        paraTxt.setText(para);
        paraTxt.setTextSize(18);
        valueTxt.setTextSize(24);
        paraTxt.setTypeface(paraTxt.getTypeface(), Typeface.BOLD);
        valueTxt.setTypeface(valueTxt.getTypeface(), Typeface.BOLD);
        // value.setTextColor(ContextCompat.getColor(mContext, R.color.yellow));
        if (value != null && !value.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            if (paraVal == 0.0) {
                valueTxt.setText("NA");
            } else
                valueTxt.setText(String.valueOf(paraVal));
            switch (colour) {
                case "green":
                    valueTxt.setTextColor(mContext.getResources().getColor(R.color.green_s));
                    break;
                case "orange":
                    valueTxt.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                    break;
                case "red":
                    valueTxt.setTextColor(mContext.getResources().getColor(R.color.red_s));
                    break;
                case "black":
                    valueTxt.setTextColor(mContext.getResources().getColor(R.color.black_s));
                    break;
                default:
                    break;
            }
        }
        return view;
    }
}
