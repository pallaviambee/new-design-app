package com.ambee.android.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.AddCityBody;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.R;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.ParameterUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.util.LinkedHashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchAdapter extends BaseAdapter {
    private static LayoutInflater inflater = null;
    private Context mContext;
    private List<ModelItem> data;
    private ModelItem mItemList;
    private Activity mActivity;


    public SearchAdapter(Context context, Activity activity, List<ModelItem> modelList) {
        mContext = context;
        mActivity = activity;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = modelList;
    }

    @Override
    public int getCount() {
        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static class ViewHolder {
        public LinearLayout mMainLayout;
        public ImageView mAddImg;
        public TextView mPlace;
        public TextView mAddress;
        public LinearLayout mMain;
        public TextView mValue;
        public TextView mUnit;
        public TextView mStatus;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_search, null);
            holder.mMainLayout = (LinearLayout) view.findViewById(R.id.content);
            holder.mAddImg = (ImageView) view.findViewById(R.id.fav);
            holder.mPlace = (TextView) view.findViewById(R.id.place);
            holder.mAddress = (TextView) view.findViewById(R.id.address);
            holder.mMain = (LinearLayout) view.findViewById(R.id.main);
            holder.mValue = (TextView) view.findViewById(R.id.value);
            holder.mUnit = (TextView) view.findViewById(R.id.unit);
            holder.mStatus = (TextView) view.findViewById(R.id.status);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (data.size() > 0) {
            holder.mMainLayout.setVisibility(View.VISIBLE);
            mItemList = null;
            mItemList = data.get(position);
            LinkedHashMap<String, String> hashMap = mItemList.getData();
            String para = "";
            String value = "";
            String placeName = "";
            String city = "";
            String country = "";
            String cityId = "";
            if (hashMap != null) {
                if (hashMap.containsKey("placeId")) {
                    cityId = hashMap.get("placeId");
                }
                if (hashMap.containsKey("name")) {
                    placeName = hashMap.get("name");
                }
                if (placeName.isEmpty()) {
                    if (hashMap.containsKey("placeName")) {
                        placeName = hashMap.get("placeName");
                    }
                }
                if (hashMap.containsKey("city")) {
                    city = hashMap.get("city");
                }
                if (hashMap.containsKey("countryCode")) {
                    country = hashMap.get("countryCode");
                }
                if (hashMap.containsKey("PM25")) {
                    para = "PM 2.5";
                    value = getValue(hashMap.get("PM25"));
                } else if (hashMap.containsKey("PM10")) {
                    para = "PM 10";
                    value = getValue(hashMap.get("PM10"));
                } else if (hashMap.containsKey("OZONE")) {
                    para = "O3";
                    value = getValue(hashMap.get("OZONE"));
                } else if (hashMap.containsKey("CO")) {
                    para = "CO";
                    value = getValue(hashMap.get("CO"));
                } else if (hashMap.containsKey("NO2")) {
                    para = "NO2";
                    value = getValue(hashMap.get("NO2"));
                } else if (hashMap.containsKey("SO2")) {
                    para = "SO2";
                    value = getValue(hashMap.get("SO2"));
                } else if (hashMap.containsKey("AQI")) {
                    para = "AQI";
                    value = getValue(hashMap.get("AQI"));
                }
                holder.mPlace.setText(placeName);
                holder.mAddress.setText(city + ", " + country);
                // convert server data to user understanding data
                double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
                // get unit of parameter
                String unit = ParameterUtil.getParameterUnit(mContext, para);
                holder.mValue.setText(String.valueOf(paraVal));
                holder.mUnit.setText(unit);
                final String finalCityId = cityId;
                final String finalPlaceName = placeName;

                holder.mAddImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //check time for token 5 min expire
                        if (DateTimeUtil.checkTime(mContext)) {
                            // call for get token
                            checkToken(finalPlaceName, finalCityId, position);
                        } else {
                            addLocation(finalPlaceName, finalCityId, position);
                        }
                    }
                });
                // set background image
                /*get color :
                 * Good - green
                 * Moderate - yellow
                 * unhealthy - red
                 * very unhealthy - brown
                 * hazardous -  purple */
                if (value != null && !value.isEmpty()) {
                    String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
                    switch (colour) {
                        case "green":
                            holder.mMain.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                            holder.mStatus.setText("Good");
                            break;
                        case "orange":
                            holder.mMain.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                            holder.mStatus.setText("Mediocre");
                            break;
                        case "red":
                            holder.mMain.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                            holder.mStatus.setText("Dangerous");
                            break;
                        case "black":
                            holder.mMain.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                            holder.mStatus.setText("Catastrophic");
                            break;
                        default:
                            break;
                    }
                }
            } else {
                holder.mMainLayout.setVisibility(View.GONE);
                Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();
            }
        } else {

        }
        return view;
    }

    private String getValue(String value) {
        String val = value;
        if (value.contains(".")) {
            String[] valArr = value.split("\\.");
            value = valArr[0];
        }
       /* if (value.contains(".") && val.split("\\.")[1].length() > 3) {
            String[] valArr = value.split("\\.");
            value = valArr[0] + "." + valArr[1].substring(0, 3);
        }*/
        return value;
    }


    private void addLocation(String placeName, String cityId, final int pos) {
        AddCityBody addCityBody = new AddCityBody(cityId, placeName);
        Call<ResponseBody> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .addStation("Bearer " + AccountUtil.getToken(mContext),
                        addCityBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String msg = String.valueOf(response.body());
                if (response.code() == 200) {
                    Toast.makeText(mContext, "Station added successfully !!", Toast.LENGTH_SHORT).show();
                }
                if (data != null && data.size() > 0) {
                    data.remove(pos);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    // check and get token after every 5 min
    public void checkToken(final String placeName, final String cityId, final int pos) {
        Call<UserRefreshToken> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
        // to execute the HTTP call
        call.enqueue(new Callback<UserRefreshToken>() {
            @Override
            public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                UserRefreshToken userRefreshToken = response.body();
                if (userRefreshToken != null) {
                    if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                        // set token
                        AccountUtil.setToken(mContext, userRefreshToken.getToken());
                        // set current time
                        AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                        addLocation(placeName, cityId, pos);
                    }
                }

            }

            @Override
            public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public Bitmap getScreenShot(LinearLayout mainLayout) {
        Bitmap bitmap = Bitmap.createBitmap(mainLayout.getWidth(),
                mainLayout.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        mainLayout.draw(c);
        return bitmap;
    }

    private File getMainDirectoryName() {
        //Here we will use getExternalFilesDir and inside that we will make our Demo folder
        //benefit of getExternalFilesDir is that whenever the app uninstalls the images will get deleted automatically.
        File mainDir = new File(
                mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Ambee");

        //If File is not present create directory
        if (!mainDir.exists()) {
            if (mainDir.mkdir())
                Log.e("Create Directory", "Main Directory Created : " + mainDir);
        }
        return mainDir;
    }

    private File store(Bitmap bitmap, String fileName, File saveFilePath) {
        File dir = new File(saveFilePath.getAbsolutePath());
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(saveFilePath.getAbsolutePath(), fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private void shareScreenshot(File file) {
        //Convert file path into Uri for sharing
        Uri uri;
        if (Build.VERSION.SDK_INT < 24) {
            uri = Uri.fromFile(file);
        } else {
            uri = Uri.parse(file.getPath());
        }
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ambee Details Screen");
        // intent.putExtra(android.content.Intent.EXTRA_TEXT, "Your sharing text goes here…");
        //pass uri here
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mActivity.startActivity(Intent.createChooser(intent, "Share Screenshot"));
    }

}
