package com.ambee.android.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.ambee.android.R;

import java.util.ArrayList;
import java.util.List;

public class RankingTabAdapter extends FragmentStatePagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private Context mContext;

    public RankingTabAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = mFragmentList.get(position);
        String text = mFragmentTitleList.get(position);
        String selection = text.toLowerCase().split(" ")[1];
        Bundle bundle = new Bundle();
        bundle.putString("SELECTED_PARA", selection);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return super.getPageTitle(position);
    }

    public View getTabView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView valueTxt = view.findViewById(R.id.value);
        valueTxt.setVisibility(View.GONE);
        TextView paraTxt = view.findViewById(R.id.para);
        String text = mFragmentTitleList.get(position);
        paraTxt.setText(text);
        paraTxt.setTextColor(mContext.getResources().getColor(R.color.black));
        return view;
    }

    public View getSelectedTabView(int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView valueTxt = view.findViewById(R.id.value);
        valueTxt.setVisibility(View.GONE);
        TextView paraTxt = view.findViewById(R.id.para);
        String text = mFragmentTitleList.get(position);
        paraTxt.setText(text);
        paraTxt.setTextColor(mContext.getResources().getColor(R.color.feedback));
        paraTxt.setTypeface(paraTxt.getTypeface(), Typeface.BOLD);
        return view;
    }
}
