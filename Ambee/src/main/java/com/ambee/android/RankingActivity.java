package com.ambee.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ambee.android.Adapter.RankingTabAdapter;
import com.ambee.android.Util.AccountUtil;

public class RankingActivity extends BaseActivity {
    private Context mContext;
    private RankingTabAdapter mTabAdapter;
    private LinearLayout mMapViewLay, mBestWorstLay;
    private TextView mMapViewTxt, mBestWorstTxt;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private AlertDialog mAlertDia;

    private LinearLayout mHomeLayout, mMapLayout, mRankLayout, mProfileLayout;
    private ImageView mHomeImg, mMapImg, mRankImg, mProfileImg;
    private TextView mHomeTxt, mMapText, mRankText, mProfileText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        initUi();
        mContext = getApplicationContext();
        mBestWorstLay.setBackgroundColor(mContext.getResources().getColor(R.color.appText));
        mBestWorstTxt.setTextColor(mContext.getResources().getColor(R.color.white));
        mMapViewTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(RankingActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        setAdapter();
        // For below navigation
        setBelowNavigation();
    }

    private void setBelowNavigation() {
        mMapLayout.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        mMapImg.setColorFilter(mContext.getResources().getColor(R.color.white));
        mMapText.setVisibility(View.VISIBLE);
        mMapText.setTextColor(mContext.getResources().getColor(R.color.white));
        mHomeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mMapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mMapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mMapText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(RankingActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(RankingActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(RankingActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(RankingActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(RankingActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mProfileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(RankingActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

    }
    private void showSignInPopup() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(RankingActivity.this);
        View promptsView = li.inflate(R.layout.prompts_signin, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                RankingActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountUtil.setGuest(mContext, false);
                Intent intent = new Intent(RankingActivity.this,
                        SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }
    private void setAdapter() {
        mTabAdapter = new RankingTabAdapter(getSupportFragmentManager(), mContext);
        mTabAdapter.addFragment(new RankingFragment(RankingActivity.this), "By City");
        mTabAdapter.addFragment(new RankingFragment(RankingActivity.this), "By State");
        mTabAdapter.addFragment(new RankingFragment(RankingActivity.this), "By Country");
        mTabAdapter.addFragment(new RankingFragment(RankingActivity.this), "By World");
        viewPager.setAdapter(mTabAdapter);
        tabLayout.setupWithViewPager(viewPager);

        highLightCurrentTab(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                highLightCurrentTab(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(mTabAdapter.getTabView(i));
        }

        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(mTabAdapter.getSelectedTabView(position));
    }

    private void initUi() {
        mMapViewLay = (LinearLayout) findViewById(R.id.mapViewLay);
        mMapViewTxt = (TextView) findViewById(R.id.mapViewTxt);
        mBestWorstLay = (LinearLayout) findViewById(R.id.best_worstLay);
        mBestWorstTxt = (TextView) findViewById(R.id.best_worstTxt);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        mHomeLayout = (LinearLayout) findViewById(R.id.home);
        mHomeImg = (ImageView) findViewById(R.id.homeImg);
        mHomeTxt = (TextView) findViewById(R.id.homeTxt);

        mMapLayout = (LinearLayout) findViewById(R.id.map);
        mMapImg = (ImageView) findViewById(R.id.mapImg);
        mMapText = (TextView) findViewById(R.id.mapTxt);

        mRankLayout = (LinearLayout) findViewById(R.id.rank);
        mRankImg = (ImageView) findViewById(R.id.rankImg);
        mRankText = (TextView) findViewById(R.id.rankTxt);

        mProfileLayout = (LinearLayout) findViewById(R.id.profile);
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        mProfileText = (TextView) findViewById(R.id.profileTxt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
