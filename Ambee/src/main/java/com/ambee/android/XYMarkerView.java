
package com.ambee.android;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;

/**
 * Custom implementation of the MarkerView.
 *
 * @author Philipp Jahoda
 */
public class XYMarkerView extends MarkerView {

    private TextView tvContent;
    private IAxisValueFormatter xAxisValueFormatter;
    private String graphType = "";
    private String selectedMenu = "";

    // For EventsChartActivity
    public XYMarkerView(Context context, IAxisValueFormatter xAxisValueFormatter) {
        super(context, R.layout.custom_marker_view);

        this.xAxisValueFormatter = xAxisValueFormatter;
        tvContent = (TextView) findViewById(R.id.tvContent);
    }

    public XYMarkerView(Context context, IAxisValueFormatter xAxisValueFormatter, String graphType,
                        String selectedMenu) {
        super(context, R.layout.custom_marker_view);

        this.xAxisValueFormatter = xAxisValueFormatter;
        this.graphType = graphType;
        this.selectedMenu = selectedMenu;
        tvContent = (TextView) findViewById(R.id.tvContent);

    }

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        if (graphType.equals("Location")) {
            String xLable = xAxisValueFormatter.getFormattedValue(e.getX(), null);
            Float yValue = Float.parseFloat(String.format("%.2f", (float) e.getY()));
            tvContent.setText(xLable + ", " + yValue + " " + selectedMenu);
        }
        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
