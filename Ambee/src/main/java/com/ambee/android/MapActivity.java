package com.ambee.android;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.MyClusterItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterManager;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;

public class MapActivity extends BaseActivity implements
        OnMapReadyCallback, ConnectionCallbacks, OnConnectionFailedListener, LocationListener {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 2;
    private GoogleApiClient mGoogleApiClient;
    private GoogleMap mMap;
    // private HashMap<Integer, MyClusterItem> mMarkerData;
    private SupportMapFragment mapFragment;
    private TextView mSearchTxt;
    private ImageView mFilter, mSearchImg;
    private LinearLayout mMapViewLay, mBestWorstLay;
    private TextView mMapViewTxt, mBestWorstTxt;

    private LinearLayout mHomeLayout, mMapLayout, mRankLayout, mProfileLayout;
    private ImageView mHomeImg, mMapImg, mRankImg, mProfileImg;
    private TextView mHomeTxt, mMapText, mRankText, mProfileText;

    private Context mContext;
    private AlertDialog mAlertDia;
    private boolean isUserFilter = false;
    private String mLat, mLong;

    private List<ModelItem> mDataList = new ArrayList<>();
    // make cluster of marker on map
    private ClusterManager<MyClusterItem> mClusterManager;
    // to change basic icon of map marker
    private ClusterRenderer mClusterRenderer;
    private MyClusterItem mClickedClusterItem;
    private LocationRequest mLocationRequest;
    private ProgressDialog mProgressDialog;
    private String mAddress = "";
    private LinearLayout mFilterSelection;
    private String mFilterDistance = "250";
    private String mFilterStr = "all";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        mContext = getApplicationContext();

        initUi();
        mMapViewLay.setBackgroundColor(mContext.getResources().getColor(R.color.appText));
        mMapViewTxt.setTextColor(mContext.getResources().getColor(R.color.white));
        mProgressDialog = ProgressDialog.show(MapActivity.this, "",
                "Fetching Stations!! Please wait...", true);

        try {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map1);
        mapFragment.getMapAsync(this);

        isUserFilter = false;
        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken("Readings", "", "");
        } else {
            if (HomeActivity.mCurrentLatitude != null
                    && !HomeActivity.mCurrentLatitude.isEmpty()
                    && HomeActivity.mCurrentLongitude != null
                    && !HomeActivity.mCurrentLongitude.isEmpty()) {
                getDataService();
            }
        }
        mSearchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlaceAutocompleteActivityIntent();
            }
        });
        mSearchTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlaceAutocompleteActivityIntent();
            }
        });
        mBestWorstTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MapActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        // For below navigation
        setBelowNavigation();
    }

    private void setBelowNavigation() {
        mMapLayout.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        mMapImg.setColorFilter(mContext.getResources().getColor(R.color.white));
        mMapText.setVisibility(View.VISIBLE);
        mMapText.setTextColor(mContext.getResources().getColor(R.color.white));
        mHomeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mHomeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mMapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mMapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mMapText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MapActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MapActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MapActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(MapActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(MapActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mProfileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(MapActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void showSignInPopup() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(MapActivity.this);
        View promptsView = li.inflate(R.layout.prompts_signin, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                MapActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountUtil.setGuest(mContext, false);
                Intent intent = new Intent(MapActivity.this,
                        SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void initUi() {
        mSearchImg = (ImageView) findViewById(R.id.searchImg);
        mSearchTxt = (TextView) findViewById(R.id.search);
        mFilter = (ImageView) findViewById(R.id.filter);
        mMapViewLay = (LinearLayout) findViewById(R.id.mapViewLay);
        mMapViewTxt = (TextView) findViewById(R.id.mapViewTxt);
        mBestWorstLay = (LinearLayout) findViewById(R.id.best_worstLay);
        mBestWorstTxt = (TextView) findViewById(R.id.best_worstTxt);
        mHomeLayout = (LinearLayout) findViewById(R.id.home);
        mHomeImg = (ImageView) findViewById(R.id.homeImg);
        mHomeTxt = (TextView) findViewById(R.id.homeTxt);

        mMapLayout = (LinearLayout) findViewById(R.id.map);
        mMapImg = (ImageView) findViewById(R.id.mapImg);
        mMapText = (TextView) findViewById(R.id.mapTxt);

        mRankLayout = (LinearLayout) findViewById(R.id.rank);
        mRankImg = (ImageView) findViewById(R.id.rankImg);
        mRankText = (TextView) findViewById(R.id.rankTxt);

        mProfileLayout = (LinearLayout) findViewById(R.id.profile);
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        mProfileText = (TextView) findViewById(R.id.profileTxt);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext)
                && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            if (isUserFilter) {
                checkToken("SearchedPlace", mLat, mLong);
                return;
            }
            checkToken("Readings", mLat, mLong);
        } else {
           /* if (isUserFilter) {
                getSearchCity(mLat, mLong);
                return;
            }
            getDataService();*/
        }
    }

    private void getDataService() {
        mDataList.clear();
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            getAddress(HomeActivity.mCurrentLatitude, HomeActivity.mCurrentLongitude);
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(HomeActivity.mCurrentLatitude),
                            Double.parseDouble(HomeActivity.mCurrentLongitude),
                            mAddress, mFilterDistance, "800", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userStations = response.body();
                    if (userStations != null) {
                        setMapData(userStations,
                                HomeActivity.mCurrentLatitude,
                                HomeActivity.mCurrentLongitude);
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void getAddress(String latitude, String longitude) {
        Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(latitude),
                    Double.parseDouble(longitude), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            mAddress = addresses.get(0).getFeatureName() + "," +
                    addresses.get(0).getSubLocality();
        } else {
            // do your stuff
        }
    }

    /* UserStations use for all data no filter apply
     *  UserStations use for filter based on user search*/
    private void setMapData(UserStations userStations, String lat, String lng) {
        mMap.clear();

        if (mMap != null && this != null) {
            // Initialize the manager with the context and the map.
            mClusterManager = new ClusterManager<MyClusterItem>(mContext, mMap);
            // for cluster rendering
            mClusterRenderer = new ClusterRenderer(mContext, mMap,
                    mClusterManager);
            // Point the map's listeners at the listeners
            // implemented by the cluster manager.
            mMap.setOnCameraIdleListener(mClusterManager);
            mMap.setOnMarkerClickListener(mClusterManager);
            mMap.moveCamera(CameraUpdateFactory
                    .newCameraPosition(new CameraPosition.Builder()
                            .target(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)))
                            .zoom(10.0f).build()));

            // Add cluster items (markers) to the cluster manager.
            addItems(userStations);

            mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
            mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                    new MapCustomInfoWindowAdapter());
            mClusterManager
                    .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyClusterItem>() {
                        @Override
                        public boolean onClusterItemClick(MyClusterItem item) {
                            mClickedClusterItem = item;
                            return false;
                        }
                    });
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {

                }
            });
            mClusterManager.cluster();
            mClusterManager.setRenderer(mClusterRenderer);
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.unable_to_create_maps),
                    Toast.LENGTH_SHORT).show();
        }

    }

    private void addItems(UserStations userStations) {
        if (mProgressDialog != null)
            mProgressDialog.cancel();
        if (userStations != null) {
            List<LinkedHashMap<String, String>> stations = userStations.getStations();
            if (stations == null) {
                return;
            }
            for (int i = 0; i < stations.size(); i++) {
                LinkedHashMap<String, String> hashMap = stations.get(i);
                if (hashMap != null) {
                    double lat = 0;
                    double lng = 0;
                    String para = "";
                    String value = "";
                    String placeName = "";
                    String city = "";
                    String country = "";
                    String snippet = "";
                    String lastUpdated = "";
                    String title = "";
                    if (hashMap.containsKey("PM25")) {
                        para = "PM 2.5";
                        value = getValue(hashMap.get("PM25"));
                    } else if (hashMap.containsKey("PM10")) {
                        para = "PM 10";
                        value = getValue(hashMap.get("PM10"));
                    } else if (hashMap.containsKey("OZONE")) {
                        para = "O3";
                        value = getValue(hashMap.get("OZONE"));
                    } else if (hashMap.containsKey("CO")) {
                        para = "CO";
                        value = getValue(hashMap.get("CO"));
                    } else if (hashMap.containsKey("NO2")) {
                        para = "NO2";
                        value = getValue(hashMap.get("NO2"));
                    } else if (hashMap.containsKey("SO2")) {
                        para = "SO2";
                        value = getValue(hashMap.get("SO2"));
                    } else if (hashMap.containsKey("AQI")) {
                        para = "AQI";
                        value = getValue(hashMap.get("AQI"));
                    }

                    if (hashMap.containsKey("placeName")) {
                        placeName = hashMap.get("placeName");
                    }
                    if (hashMap.containsKey("city")) {
                        city = hashMap.get("city");
                    }
                    if (hashMap.containsKey("countryCode")) {
                        country = hashMap.get("countryCode");
                    }

                    double paraVal = ParameterUtil.getParameterValue(mContext,
                            para, value);
                    String color = ParameterUtil.getParameterColour(mContext, para, paraVal);

                    int count = 0;
                    for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                        count++;
                        String key = entry.getKey();
                        String val = entry.getValue();
                        if (key.equals("lat")) {
                            lat = Double.parseDouble(val);
                        }
                        if (key.equals("lng")) {
                            lng = Double.parseDouble(val);
                        }
                        if (key.equals("lastUpdate")) {
                            lastUpdated = val;
                        }
                        if (key.equals("PM25")) {
                            para = "PM 2.5";
                            value = getValue(val);
                        }
                        if (key.equals("PM10")) {
                            para = "PM 10";
                            value = getValue(val);
                        }
                        if (key.equals("OZONE")) {
                            para = "O3";
                            value = getValue(val);
                        }
                        if (key.equals("CO")) {
                            para = "CO";
                            value = getValue(val);
                        }
                        if (key.equals("NO2")) {
                            para = "NO2";
                            value = getValue(val);
                        }
                        if (key.equals("SO2")) {
                            para = "SO2";
                            value = getValue(val);
                        }
                        if (key.equals("AQI")) {
                            para = "AQI";
                            value = getValue(val);
                        }
                        if (key.equals("PM25") || key.equals("PM 10") ||
                                key.equals("OZONE") || key.equals("CO") ||
                                key.equals("NO2") ||
                                key.equals("SO2") || key.equals("AQI")) {
                            title = placeName + "\n" + city + "," + country;
                            paraVal = ParameterUtil.getParameterValue(mContext, para, value);
                            String unit = ParameterUtil.getParameterUnit(mContext, para);
                            snippet += para + " : " + paraVal + " " + unit + "\n";
                            lastUpdated = "\n Last updated : " + lastUpdated;
                        }
                    }
                    if (count == hashMap.size()) {
                        // add to cluster
                        MyClusterItem offsetItem = new MyClusterItem(lat, lng, title, snippet + lastUpdated,
                                getMarkerIcon(color), getMsg(color));
                        mClusterManager.addItem(offsetItem);
                    }
                }
            }
        }
    }

    private String getMsg(String color) {
        String msg = "";
        switch (color) {
            case "green":
                msg = mContext.getResources().getString(R.string.pm_good_recom);
                break;
            case "yellow":
                msg = mContext.getResources().getString(R.string.pm_moderate_recom);
                break;
            case "orange":
                msg = mContext.getResources().getString(R.string.pm_unhealthy_recom);
                break;
            case "brown":
                msg = mContext.getResources().getString(R.string.pm_very_unhealthy_recom);
                break;
            case "purple":
                msg = mContext.getResources().getString(R.string.pm_hazardous_recom);
                break;
            default:
                break;
        }
        return msg;
    }

    private String getLastUpdatedTime(String lastUpdated) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy(HH:mm:ss)");
        Date date = null;
        try {
            date = inputFormat.parse(lastUpdated);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = outputFormat.format(date);
        return formattedDate;
    }

    private Bitmap getMarkerIcon(String color) {
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_green, mContext.getTheme());
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
        Bitmap bitmap1 = ((BitmapDrawable) d).getBitmap();
        switch (color) {
            case "green":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_green, mContext.getTheme());
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
                bitmap1 = ((BitmapDrawable) d).getBitmap();
                break;
            case "yellow":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_yellow, mContext.getTheme());
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
                bitmap1 = ((BitmapDrawable) d).getBitmap();
                break;
            case "orange":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_red, mContext.getTheme());
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
                bitmap1 = ((BitmapDrawable) d).getBitmap();
                break;
            case "brown":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_brown, mContext.getTheme());
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
                bitmap1 = ((BitmapDrawable) d).getBitmap();
                break;
            case "purple":
                drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_purple, mContext.getTheme());
                bitmap = ((BitmapDrawable) drawable).getBitmap();
                d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, bitmap.getWidth(), bitmap.getHeight(), true));
                bitmap1 = ((BitmapDrawable) d).getBitmap();
                break;
            default:
                break;
        }
        return bitmap1;
    }

    // check and get token after every 5 min
    public void checkToken(final String str, final String latitude, final String longitude) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            if (str.equals("Readings")) {
                                if (HomeActivity.mCurrentLatitude != null
                                        && !HomeActivity.mCurrentLatitude.isEmpty()
                                        && HomeActivity.mCurrentLongitude != null
                                        && !HomeActivity.mCurrentLongitude.isEmpty())
                                    getDataService();
                            }
                            if (str.equals("SearchedPlace")) {
                                getSearchCity(latitude, longitude);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(MapActivity.this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            //PLACE_AUTOCOMPLETE_REQUEST_CODE is integer for request code
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // Handle the error.
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                LatLng lat = place.getLatLng();
                mAddress = place.getName().toString();
                mLat = String.valueOf(lat.latitude);
                mLong = String.valueOf(lat.longitude);
                if (DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken("SearchedPlace", mLat, mLong);
                } else {
                    getSearchCity(mLat, mLong);
                }
                //  Toast.makeText(mContext, place.getAddress(), Toast.LENGTH_LONG).show();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Toast.makeText(mContext, status.getStatusMessage(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void getSearchCity(String latitude, String longitude) {
        mDataList.clear();
        if (mLat.isEmpty() && mLong.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            //getAddress(latitude, longitude);
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(latitude), Double.parseDouble(longitude), mAddress,
                            mFilterDistance, "20", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userSearchCity = response.body();
                    if (userSearchCity != null) {
                        setMapData(userSearchCity, mLat, mLong);
                    }
                    // clear for next fetch
                    mLat = "";
                    mLong = "";
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    public void onStop() {
        super.onStop();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.moveCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(new LatLng(21.0d, 78.0d))
                        .zoom(5.0f).build()));

        // set Zoom enable of map
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission
                (this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.clear();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }


    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            /*//If everything went fine lets get latitude and longitude
            mCurrentLatitude = String.valueOf(location.getLatitude());
            mCurrentLongitude = String.valueOf(location.getLongitude());
            //check time for token 5 min expire
            if (DateTimeUtil.checkTime(getApplicationContext())) {
                // call for get token
                checkToken("Readings", "", "");
            } else {
                if (!mCurrentLatitude.isEmpty() && !mCurrentLatitude.isEmpty()) {
                    getDataService();
                    mMap.moveCamera(CameraUpdateFactory
                            .newCameraPosition(new CameraPosition.Builder()
                                    .target(new LatLng(Double.parseDouble(mCurrentLatitude), Double.parseDouble(mCurrentLongitude)))
                                    .zoom(10.0f).build()));
                }
            }*/
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the
             * user with the error.
             */
            //   Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
    /*    mCurrentLatitude = String.valueOf(location.getLatitude());
        mCurrentLongitude = String.valueOf(location.getLongitude());
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken("Readings", "", "");
        } else {
            if (!mCurrentLatitude.isEmpty() && !mCurrentLongitude.isEmpty()) {
                getDataService();
                mMap.moveCamera(CameraUpdateFactory
                        .newCameraPosition(new CameraPosition.Builder()
                                .target(new LatLng(Double.parseDouble(mCurrentLatitude), Double.parseDouble(mCurrentLongitude)))
                                .zoom(10.0f).build()));
            }
        }*/
    }

    private class MapCustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            LayoutInflater li = LayoutInflater.from(mContext);
            View promptsView = li.inflate(R.layout.prompts_marker, null);
            TextView place = promptsView.findViewById(R.id.place);
            place.setText(mClickedClusterItem.getTitle());
            TextView status = promptsView.findViewById(R.id.status);
            status.setText(mClickedClusterItem.getTitle());
            status.setVisibility(View.GONE);
            TextView msg = promptsView.findViewById(R.id.msg);
            TextView value = promptsView.findViewById(R.id.value);
            value.setText(mClickedClusterItem.getSnippet());
            msg.setText(mClickedClusterItem.getMsg());
            return promptsView;
        }
    }
}
