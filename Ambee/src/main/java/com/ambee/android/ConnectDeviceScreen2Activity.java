package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.DeviceIdFromQR;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import pl.droidsonroids.gif.GifImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConnectDeviceScreen2Activity extends BaseActivity {
    private Context mContext;
    private GifImageView mGifDevice;
    private LinearLayout mBackLay, mNextLay;
    private ImageView mBackImg, mNextImg;
    private TextView mBackTxt, mNextTxt;
    //qr code scanner object
    private IntentIntegrator qrScan;
    private ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_device_screen2);
        initUi();
        mContext = getApplicationContext();

        //intializing scan object
        qrScan = new IntentIntegrator(this);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBackLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen2Activity.this,
                        ConnectDeviceScreen1Activity.class);
                startActivity(intent);
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen2Activity.this,
                        ConnectDeviceScreen1Activity.class);
                startActivity(intent);
            }
        });
        mBackTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Intent intent = new Intent(ConnectDeviceScreen2Activity.this,
                        ConnectDeviceScreen1Activity.class);
                startActivity(intent);
            }
        });
        mNextLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();
            }
        });
        mNextImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();

            }
        });
        mNextTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qrScan.initiateScan();

            }
        });
    }

    private void initUi() {
        mBack = (ImageView) findViewById(R.id.back);
        mGifDevice = (GifImageView) findViewById(R.id.gif);
        mBackLay = (LinearLayout) findViewById(R.id.backLay);
        mBackImg = (ImageView) findViewById(R.id.backArr);
        mBackTxt = (TextView) findViewById(R.id.backText);
        mNextLay = (LinearLayout) findViewById(R.id.nextLay);
        mNextImg = (ImageView) findViewById(R.id.nextArr);
        mNextTxt = (TextView) findViewById(R.id.nextText);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        Intent intent = new Intent(ConnectDeviceScreen2Activity.this,
                HomeActivity.class);
        startActivity(intent);
    }

    //Getting the scan results(url)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            String url = result.getContents();
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Please scan again", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                if (url != null && url.contains("=")) {
                    // url from scan
                    String deviceId = url.split("=")[1];
                    // send url to server and get device id
                    //check time for token 5 min expire
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken(deviceId);
                    } else {
                        getDeviceId(deviceId);
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void getDeviceId(String deviceId) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            String url = "https://j7qo4rdtm6.execute-api.ap-south-1.amazonaws.com/";
            Call<DeviceIdFromQR> call = new RetrofitClient(url)
                    .getApi()
                    .getDeviceId("Bearer " + AccountUtil.getToken(mContext), deviceId);
            call.enqueue(new Callback<DeviceIdFromQR>() {
                @Override
                public void onResponse(Call<DeviceIdFromQR> call, Response<DeviceIdFromQR> response) {
                    // device id
                    DeviceIdFromQR deviceIdFromQR = response.body();
                    if (deviceIdFromQR != null) {
                        finish();
                        Intent intent = new Intent(ConnectDeviceScreen2Activity.this,
                                ConnectDeviceScreen3Activity.class);
                        intent.putExtra("DeviceId", deviceIdFromQR.getDevice_id());
                        startActivity(intent);
                    } else {
                        Toast.makeText(mContext, "Please scan again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<DeviceIdFromQR> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void checkToken(final String deviceId) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            // get device id from QR code
                            getDeviceId(deviceId);
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

}
