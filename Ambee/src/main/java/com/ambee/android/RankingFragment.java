package com.ambee.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.RankingAdapter;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.Ranking;
import com.ambee.android.Data.RankingData;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;


public class RankingFragment extends Fragment {
    private Context mContext;
    private LinearLayout mWorstLayout;
    private TextView mWorst;
    private LinearLayout mBestLayout;
    private TextView mBest;
    private ListView mList;
    private RankingAdapter mAdapter;
    private String mSeletedOption = "best";
    private List<ModelItem> mDataList = new ArrayList<>();
    private View mView;
    private String mParameter = "";
    private Activity mActivity;

    public RankingFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public RankingFragment(Activity activity) {
        // Required empty public constructor
        mActivity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_ranking, container, false);
        mContext = getActivity().getBaseContext();
        initUi();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mParameter = bundle.getString("SELECTED_PARA", "");
        }
        if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            getDataService();
        }
        mBestLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bordershadow_blue));
        mBest.setTextColor(mContext.getResources().getColor(R.color.white));
        mBest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBestLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bordershadow_blue));
                mBest.setTextColor(mContext.getResources().getColor(R.color.white));
                mWorstLayout.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                mWorst.setTextColor(mContext.getResources().getColor(R.color.text_color));
                mSeletedOption = "best";
                mDataList.clear();
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
                //check time for token 5 min expire
                if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken();
                } else {
                    getDataService();
                }
            }
        });

        mWorst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBestLayout.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                mBest.setTextColor(mContext.getResources().getColor(R.color.text_color));
                mWorstLayout.setBackground(mContext.getResources().getDrawable(R.drawable.bordershadow_blue));
                mWorst.setTextColor(mContext.getResources().getColor(R.color.white));
                mSeletedOption = "worst";
                mDataList.clear();
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();
                //check time for token 5 min expire
                if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken();
                } else {
                    getDataService();
                }
            }
        });
        return mView;
    }

    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext),
                            AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            getDataService();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getDataService() {
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (HomeActivity.mCurrentLatitude == null && HomeActivity.mCurrentLongitude == null) {
            return;
        }
        if (HomeActivity.mCurrentLatitude.isEmpty() && HomeActivity.mCurrentLongitude.isEmpty()) {
            return;
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<Ranking> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getBestWorstPlaces("Bearer " + token,
                            mSeletedOption, mParameter,
                            Double.parseDouble(HomeActivity.mCurrentLatitude),
                            Double.parseDouble(HomeActivity.mCurrentLongitude));

            // to execute the HTTP call
            call.enqueue(new Callback<Ranking>() {
                @Override
                public void onResponse(Call<Ranking> call, Response<Ranking> response) {
                    mDataList.clear();
                    Ranking ranking = response.body();
                    if (ranking != null) {
                        for (RankingData rankingData : ranking.getData()) {
                            mDataList.add(new ModelItem(rankingData.getName(),
                                    rankingData.getValue(), rankingData.getImage(), mSeletedOption));
                        }
                        setAdapter();
                    }
                }

                @Override
                public void onFailure(Call<Ranking> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setAdapter() {
        if (mDataList != null && mDataList.size() > 0) {
            mAdapter = new RankingAdapter(getApplicationContext(), mDataList);
            mList.setAdapter(mAdapter);
        } else {

        }
    }

    private void initUi() {
        mWorstLayout = (LinearLayout) mView.findViewById(R.id.worst_layout);
        mWorst = (TextView) mView.findViewById(R.id.worst);
        mBestLayout = (LinearLayout) mView.findViewById(R.id.best_layout);
        mBest = (TextView) mView.findViewById(R.id.best);
        mList = (ListView) mView.findViewById(R.id.listView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mDataList.clear();
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext) &&
                DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            getDataService();
        }
    }
}
