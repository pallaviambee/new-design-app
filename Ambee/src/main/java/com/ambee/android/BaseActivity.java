package com.ambee.android;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Util.NetworkUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseActivity extends AppCompatActivity {
    private Handler mHandler;
    private Context mContext;
    private final static int REQUEST_LOCATION = 1000;
    public DrawerLayout mDrawerLayout;
    protected NavigationView mNavigationView;
    private GoogleApiClient googleApiClient;
    protected Toolbar mToolbar;
    protected ListView mDrawerList;
    // sPosition is use for highlight nav menu on selection
    public static int sPosition = 0;
    protected boolean mPaused;
    private AlertDialog mAlertDialog;
    private List<String> mPermissionsNeeded = new ArrayList<String>();
    private final List<String> mPermissionsList = new ArrayList<String>();
    private final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        mContext = getApplicationContext();
    }

    private void init() {
        mHandler = new Handler();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPaused = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPaused = false;
        if (!NetworkUtil.checkNetworkConnection(this) && mAlertDialog == null) {
            showAlert(getResources().getString(R.string.no_network_connection),
                    Settings.ACTION_SETTINGS);
        } else {
            mPermissionsList.clear();
            checkPermission();
        }
        onDeviceLocation();
    }

    private void onDeviceLocation() {
        this.setFinishOnTouchOutside(true);
        final LocationManager manager = (LocationManager)
                BaseActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && hasGPSDevice(BaseActivity.this)) {

        }
        if (!hasGPSDevice(BaseActivity.this)) {
            Toast.makeText(BaseActivity.this,
                    "Gps not Supported", Toast.LENGTH_SHORT).show();
        }
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && hasGPSDevice(BaseActivity.this)) {
            enableLoc();
        }
    }

    private void enableLoc() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(BaseActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {

                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(ConnectionResult connectionResult) {

                        }
                    }).build();
            googleApiClient.connect();
        }

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(BaseActivity.this,
                                    REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                }
            }
        });
    }

    private boolean hasGPSDevice(BaseActivity baseActivity) {
        final LocationManager mgr = (LocationManager) mContext
                .getSystemService(Context.LOCATION_SERVICE);
        if (mgr == null)
            return false;
        final List<String> providers = mgr.getAllProviders();
        if (providers == null)
            return false;
        return providers.contains(LocationManager.GPS_PROVIDER);
    }

    private void showAlert(String blockingMessage, final String settingAction) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.popup__error, null);
        TextView textView = (TextView) view.findViewById(R.id.alert_text);
        textView.setText(blockingMessage);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingAction.equals(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                } else if (settingAction.equals(android.provider.Settings.ACTION_DATE_SETTINGS)) {
                    startActivityForResult(
                            new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                } else if (settingAction.equals(Settings.ACTION_SETTINGS)) {
                    startActivity(new Intent(Settings.ACTION_SETTINGS));
                }
                if (mAlertDialog != null) {
                    mAlertDialog.cancel();
                }
            }
        });
        mAlertDialog = builder.show();
        mAlertDialog.setCancelable(false);
        mAlertDialog.setCanceledOnTouchOutside(false);
    }

    public boolean checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            addPermissionList();
            if (mPermissionsList.size() > 0) {
                // Need Rationale
                String message = getResources().getString(R.string.you_need_to_grant_access_to) + " ";
                if (mPermissionsNeeded.size() > 0) {
                    message += mPermissionsNeeded.get(0);
                    for (int i = 1; i < mPermissionsNeeded.size(); i++) {

                        if (i == mPermissionsNeeded.size() - 1) {
                            message = message + " and " + mPermissionsNeeded.get(i);
                        } else
                            message = message + ", " + mPermissionsNeeded.get(i);
                    }
                }
                if (mAlertDialog != null)
                    mAlertDialog.dismiss();
                LayoutInflater li = LayoutInflater.from(this);
                View promptsView = li.inflate(R.layout.prompts, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        this, R.style.MyDialogTheme);
                alertDialogBuilder.setView(promptsView);
                TextView heading = (TextView) promptsView.findViewById(R.id.heading);
                heading.setText(message);
                TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
                TextView ok = (TextView) promptsView.findViewById(R.id.ok);
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mAlertDialog != null)
                            mAlertDialog.dismiss();
                    }
                });
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mAlertDialog != null)
                            mAlertDialog.dismiss();
                        requestPermissions(
                                mPermissionsList.toArray(new String[mPermissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

                    }
                });
                mAlertDialog = alertDialogBuilder.show();
                mAlertDialog.setCanceledOnTouchOutside(false);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    public void addPermissionList() {
        mPermissionsList.clear();
        mPermissionsNeeded.clear();
        if (!addPermission(mPermissionsList, Manifest.permission.ACCESS_FINE_LOCATION)
                && !addPermission(mPermissionsList, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            mPermissionsNeeded.add(getResources().getString(R.string.access_fine_location));
        }
        if (!addPermission(mPermissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                && !addPermission(mPermissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            mPermissionsNeeded.add(getResources().getString(R.string.storage));
        }
        if (!addPermission(mPermissionsList, Manifest.permission.CAMERA)) {
            mPermissionsNeeded.add(getResources().getString(R.string.camera));
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission)
                != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            return false;
        }
        // Check for Rationale Option
        if (!shouldShowRequestPermissionRationale(permission)) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                //  perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }
                if (perms.get(Manifest.permission.ACCESS_FINE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
                    // All Permissions Granted
                } else if (!shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Intent intent = new Intent();
                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivity(intent);
                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some permission denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        //   getToolbar();
        //     setupNavBar();
    }

    public static class ImageLoadTask extends AsyncTask<Void, Void, Bitmap> {

        private String url;
        private ImageView imageView;

        public ImageLoadTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... params) {
            try {
                URL urlConnection = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) urlConnection
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            super.onPostExecute(result);
            imageView.setImageBitmap(result);
        }

    }
}
