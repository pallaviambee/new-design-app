package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.SearchAdapter;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity {
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Context mContext;
    private ImageView mBackImg;
    private TextView mSearchTxt;
    private SeekBar mSeekBar;
    private ImageView mFilter, mSearchImg;
    private TextView mHeading;
    private ListView mList;
    private SearchAdapter mAdapter;
    // Add list
    private List<ModelItem> mDataList = new ArrayList<>();
    private boolean isUserFilter = false;
    private String mLat = "", mLong = "";
    private String mAddress = "";
    private boolean mIsFilterSelected = false;
    private TextView mDisFilterTxt;
    private LinearLayout mFilterSelection, mSearchedPlace;
    private RadioGroup mRdGroup;
    private RadioButton mRdbGood, mRdbMediocre, mRdbDangerous, mRdbCatastrophic;
    private TextView mApplyFilter;
    private String mFilterDistance = "250";
    private String mFilterStr = "all";
    private boolean mIsFilterApplied = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mContext = getApplicationContext();
        initUi();

        isUserFilter = false;
        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken("Readings", "", "");
        } else {
            getDataService();
        }
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mDataList != null && mDataList.size() > 0) {
                    ModelItem modelItem = mDataList.get(position);
                    Intent intent = new Intent(SearchActivity.this,
                            HomeDetailsActivity.class);
                    intent.putExtra("CityId", mDataList.get(position).getDevId());
                    intent.putExtra("IsFromSearch", true);
                    startActivity(intent);
                }
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mIsFilterSelected)
                    finish();
                else {
                    mIsFilterSelected = false;
                    mFilterSelection.setVisibility(View.GONE);
                    mSearchedPlace.setVisibility(View.VISIBLE);
                }
            }
        });
        mSearchTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlaceAutocompleteActivityIntent();
            }
        });
        mFilterSelection.setVisibility(View.GONE);
        mSearchedPlace.setVisibility(View.VISIBLE);
        mIsFilterApplied = false;
        mFilter.setImageDrawable(mContext.getResources().getDrawable(R.drawable.filter));
        mFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsFilterSelected = true;
                mFilterSelection.setVisibility(View.VISIBLE);
                mSearchedPlace.setVisibility(View.GONE);
            }
        });
        mSearchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callPlaceAutocompleteActivityIntent();
            }
        });


        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
             /*   Toast.makeText(SearchActivity.this,
                        "Seekbar vale " + i, Toast.LENGTH_SHORT).show();*/
                mFilterDistance = String.valueOf(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
              /*  Toast.makeText(SearchActivity.this,
                        "Seekbar touch started", Toast.LENGTH_SHORT).show();*/
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
               /* Toast.makeText(SearchActivity.this,
                        mFilterDistance, Toast.LENGTH_SHORT).show();*/
                mDisFilterTxt.setText("Based on distance - " + mFilterDistance + "Km");
            }
        });
        mApplyFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsFilterApplied = true;
                mFilter.setImageDrawable(mContext.getResources().getDrawable(R.drawable.filter_applied));
                if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken("Readings", "", "");
                } else {
                    getDataService();
                }
            }
        });

    }

    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.rdbGood:
                if (checked)
                    mFilterStr = "good";
                break;
            case R.id.rdbMediocre:
                if (checked)
                    mFilterStr = "mediocre";
                break;
            case R.id.rdbDangerous:
                if (checked)
                    mFilterStr = "dangerous";
                break;
            case R.id.rdbCatastrophic:
                if (checked)
                    mFilterStr = "catastrophic";
                break;
        }
        //Toast.makeText(getApplicationContext(), mFilterStr, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            if (!AccountUtil.isGuest(mContext) && isUserFilter) {
                checkToken("SearchedPlace", mLat, mLong);
                return;
            } else {
                checkToken("Readings", mLat, mLong);
            }
        } else {
            if (isUserFilter) {
                getSearchCity(mLat, mLong);
                return;
            }
            getDataService();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void callPlaceAutocompleteActivityIntent() {
        try {
            Intent intent =
                    new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                            .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
            //PLACE_AUTOCOMPLETE_REQUEST_CODE is integer for request code
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            // Handle the error.
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                mAddress = place.getName().toString();
                LatLng lat = place.getLatLng();
                mLat = String.valueOf(lat.latitude);
                mLong = String.valueOf(lat.longitude);
                if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken("SearchedPlace", mLat, mLong);
                } else {
                    getSearchCity(mLat, mLong);
                }
                //  Toast.makeText(mContext, place.getAddress(), Toast.LENGTH_LONG).show();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                Toast.makeText(mContext, status.getStatusMessage(), Toast.LENGTH_LONG).show();
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void getSearchCity(String latitude, String longitude) {
        mDataList.clear();
        if (mLat.isEmpty() && mLong.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            // getAddress(latitude, longitude);
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(latitude), Double.parseDouble(longitude), mAddress,
                            mFilterDistance, "20", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userSearchCity = response.body();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        if (Integer.parseInt(userSearchCity.getCount()) > 0) {
                            mHeading.setText("Search Results \n" + "(" + Integer.parseInt(userSearchCity.getCount()) + " Results)");
                            for (int i = 0; i < stations.size(); i++) {
                                String cityId = "";
                                LinkedHashMap<String, String> map = stations.get(i);
                                if (map.containsKey("placeId")) {
                                    cityId = map.get("placeId");
                                }
                                mDataList.add(new ModelItem(map, cityId, "", ""));
                            }
                        } else {
                            mDataList.add(new ModelItem((LinkedHashMap<String, String>) null, "", "", ""));
                        }
                    }
                    setAdapter();
                    // clear for next fetch
                    mLat = "";
                    mLong = "";
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getAddress(String latitude, String longitude) {
        Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(latitude),
                    Double.parseDouble(longitude), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses.size() > 0) {
            mAddress = addresses.get(0).getFeatureName() + "," +
                    addresses.get(0).getSubLocality();
        } else {
            // do your stuff
        }
    }

    private void getDataService() {
        mDataList.clear();
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            getAddress(HomeActivity.mCurrentLatitude, HomeActivity.mCurrentLongitude);
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(HomeActivity.mCurrentLatitude),
                            Double.parseDouble(HomeActivity.mCurrentLongitude),
                            mAddress, mFilterDistance, "20", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    mDataList.clear();
                    UserStations userStations = response.body();
                    if (mIsFilterApplied)
                        mHeading.setText("Search Results \n" + "(" + Integer.parseInt(userStations.getCount()) + " Results)");
                    else
                        mHeading.setText("Popular Places");

                    if (userStations != null) {
                        List<LinkedHashMap<String, String>> stations = userStations.getStations();
                        if (stations != null && stations.size() > 0) {
                            for (int i = 0; i < stations.size(); i++) {
                                String cityId = "";
                                LinkedHashMap<String, String> map = stations.get(i);
                                if (map.containsKey("placeId")) {
                                    cityId = map.get("placeId");
                                }
                                mDataList.add(new ModelItem(map, cityId, "", ""));
                            }
                        }
                    }
                    setAdapter();
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setAdapter() {
        if (mDataList != null && mDataList.size() > 0) {
            mSearchedPlace.setVisibility(View.VISIBLE);
            mFilterSelection.setVisibility(View.GONE);
            mList.setVisibility(View.VISIBLE);
            mAdapter = new SearchAdapter(getApplicationContext(), SearchActivity.this, mDataList);
            mList.setAdapter(mAdapter);
        } else {
            mList.setVisibility(View.GONE);
            mSearchedPlace.setVisibility(View.VISIBLE);
            mFilterSelection.setVisibility(View.GONE);
            if (mIsFilterApplied)
                mHeading.setText("Search Results \n" + "( 0 Results)");
            else
                mHeading.setText("Popular Places");
        }
    }

    private void initUi() {
        mFilterSelection = (LinearLayout) findViewById(R.id.filterSelection);
        mSearchedPlace = (LinearLayout) findViewById(R.id.searchedPlace);
        mDisFilterTxt = (TextView) findViewById(R.id.distanceFilter);
        mRdGroup = (RadioGroup) findViewById(R.id.rdGroup);
        mRdbGood = (RadioButton) findViewById(R.id.rdbGood);
        mRdbMediocre = (RadioButton) findViewById(R.id.rdbMediocre);
        mRdbDangerous = (RadioButton) findViewById(R.id.rdbDangerous);
        mRdbCatastrophic = (RadioButton) findViewById(R.id.rdbCatastrophic);
        mApplyFilter = (TextView) findViewById(R.id.applyFilter);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar1);
        mList = (ListView) findViewById(R.id.listView);
        mBackImg = (ImageView) findViewById(R.id.back);
        mSearchTxt = (TextView) findViewById(R.id.search);
        mSearchImg = (ImageView) findViewById(R.id.searchImg);
        mFilter = (ImageView) findViewById(R.id.filter);
        mHeading = (TextView) findViewById(R.id.heading);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // check and get token after every 5 min
    public void checkToken(final String str, final String latitude, final String longitude) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            if (str.equals("Readings")) {
                                getDataService();
                            }
                            if (str.equals("SearchedPlace")) {
                                getSearchCity(latitude, longitude);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
