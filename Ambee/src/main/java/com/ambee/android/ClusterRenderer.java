package com.ambee.android;

import android.content.Context;

import com.ambee.android.Data.MyClusterItem;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;


public class ClusterRenderer extends DefaultClusterRenderer<MyClusterItem> {

    public ClusterRenderer(Context context, GoogleMap map, ClusterManager<MyClusterItem> clusterManager) {
        super(context, map, clusterManager);
        clusterManager.setRenderer(this);
    }


    @Override
    protected void onBeforeClusterItemRendered(MyClusterItem markerItem, MarkerOptions markerOptions) {
        if (markerItem.getBitMap() != null) {
            //Here you retrieve BitmapDescriptor from ClusterItem and set it as marker icon
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerItem.getBitMap()));
        }
        markerOptions.visible(true);
    }
}