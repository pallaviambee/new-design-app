package com.ambee.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.AddGuestBody;
import com.ambee.android.Data.UserLogin;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Arrays;

import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Constant.AuthConstant.SERVERCLIENT_ID;

/* Created by Pallavi on 03/01/2019 */

public class SplashActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 16;
    private Context mContext;
    private ImageView mLogo, mGoogleImg;
    private TextView mTextLogo, mOrTxt, mGuestTxt, mLoginText;
    private LinearLayout mMainLay, mLoginLayout, mGuestLay;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager mCallbackManager;
    private AlertDialog mAlertDialog;
    private String mNameStr = "", mGender = "", mCity = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        setContentView(R.layout.activity_splash);

        mContext = getApplicationContext();

        initUi();

        // if already login then splash screen time out 2 sec
        if (!AccountUtil.getToken(mContext).isEmpty() || AccountUtil.isGuest(mContext)) {
            mLoginLayout.setVisibility(View.GONE);
            mOrTxt.setVisibility(View.GONE);
            mGuestTxt.setVisibility(View.GONE);
            mTextLogo.setVisibility(View.GONE);
            mLogo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_logo));
            Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation1.setStartOffset(200);
            mLogo.setAnimation(animation1);

            Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation2.setStartOffset(200);
            mTextLogo.setAnimation(animation2);

            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }, 2000);
        } else {
            Animation animation1 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation1.setStartOffset(200);
            mLogo.setAnimation(animation1);

            mLoginLayout.setVisibility(View.VISIBLE);
            mOrTxt.setVisibility(View.VISIBLE);
            mGuestTxt.setVisibility(View.VISIBLE);

            Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation2.setStartOffset(400);
            mTextLogo.setAnimation(animation2);

            Animation animation3 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation3.setStartOffset(600);
            mLoginLayout.setAnimation(animation3);

            Animation animation4 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation3.setStartOffset(400);
            mOrTxt.setAnimation(animation4);

            Animation animation5 = AnimationUtils.loadAnimation(this, R.anim.myfadeintransaction);
            animation3.setStartOffset(400);
            mGuestTxt.setAnimation(animation5);


            new Handler().postDelayed(new Runnable() {

                /*
                 * Showing splash screen with a timer. This will be useful when you
                 * want to show case your app logo / company
                 */

                @Override
                public void run() {
                    // Check for existing Google Sign In account, if the user is already signed in
                    // the GoogleSignInAccount will be non-null.
                    //  GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(SplashActivity.this);
                    if (!AccountUtil.getToken(mContext).isEmpty()) {
                        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            }, 3000);
        }
        computePackageHash();
        configureGoogleLogin();
        mCallbackManager = CallbackManager.Factory.create();
        mGoogleImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
        mLoginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googleSignIn();
            }
        });
        mNameStr = "";
        mGender = "";
        mCity = "";
        mGuestTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGuestLay.removeAllViews();
                mGuestLay.setVisibility(View.VISIBLE);
                mMainLay.setVisibility(View.GONE);
                LayoutInflater inflater = LayoutInflater.from(SplashActivity.this);
                View view = inflater.inflate(R.layout.activity_onboarding1, mGuestLay,
                        false);
                final EditText name = (EditText) view.findViewById(R.id.name);
                TextView nextTxt = (TextView) view.findViewById(R.id.nextTxt);
                mGuestLay.addView(view);
                nextTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mNameStr = name.getText().toString();
                        if (mNameStr != null && mNameStr.length() > 0) {
                            setSecondInput();
                        } else {
                            Toast.makeText(mContext, "Please enter name", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

    }

    private void setSecondInput() {
        mGuestLay.removeAllViews();
        mGuestLay.setVisibility(View.VISIBLE);
        mMainLay.setVisibility(View.GONE);
        LayoutInflater inflater = LayoutInflater.from(SplashActivity.this);
        View view = inflater.inflate(R.layout.activity_onboarding2, mGuestLay,
                false);
        TextView nextTxt = (TextView) view.findViewById(R.id.nextTxt);
        final LinearLayout maleLay = (LinearLayout) view.findViewById(R.id.maleLay);
        final TextView maleTxt = (TextView) view.findViewById(R.id.maleTxt);
        final LinearLayout femaleLay = (LinearLayout) view.findViewById(R.id.femaleLay);
        final TextView femaleTxt = (TextView) view.findViewById(R.id.femaleTxt);
        final LinearLayout otherLay = (LinearLayout) view.findViewById(R.id.othersLay);
        final TextView othersTxt = (TextView) view.findViewById(R.id.othersTxt);
        mGuestLay.addView(view);
        maleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = "Male";
                maleLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_bordershadow));
                maleTxt.setTextColor(mContext.getResources().getColor(R.color.feedback));
                femaleLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                femaleTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
                otherLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                othersTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
            }
        });
        femaleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = "Female";
                maleLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                maleTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
                femaleLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_bordershadow));
                femaleTxt.setTextColor(mContext.getResources().getColor(R.color.feedback));
                otherLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                othersTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
            }
        });
        othersTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGender = "Others";
                maleLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                maleTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
                femaleLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bordershadow));
                femaleTxt.setTextColor(mContext.getResources().getColor(R.color.text_dark));
                otherLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_bordershadow));
                othersTxt.setTextColor(mContext.getResources().getColor(R.color.feedback));
            }
        });
        nextTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mGender != null && mGender.length() > 0) {
                    setThirdInput();
                } else {
                    Toast.makeText(mContext, "Please select gender", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setThirdInput() {
        mGuestLay.removeAllViews();
        mGuestLay.setVisibility(View.VISIBLE);
        mMainLay.setVisibility(View.GONE);
        LayoutInflater inflater = LayoutInflater.from(SplashActivity.this);
        View view = inflater.inflate(R.layout.activity_onboarding3, mGuestLay,
                false);
        final LinearLayout nextLay = (LinearLayout) view.findViewById(R.id.nextLay);
        final EditText city = (EditText) view.findViewById(R.id.city);
        final LinearLayout signUpLay = (LinearLayout) view.findViewById(R.id.signUpLay);
        final TextView signInTxt = (TextView) view.findViewById(R.id.signInTxt);
        final TextView continueTxt = (TextView) view.findViewById(R.id.continueTxt);
        mGuestLay.addView(view);
        signInTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNameStr = "";
                mGender = "";
                mCity = "";
                mGuestLay.removeAllViews();
                mGuestLay.setVisibility(View.GONE);
                mMainLay.setVisibility(View.VISIBLE);
            }
        });
        continueTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCity = city.getText().toString();
                if (mCity != null && mCity.length() > 0) {
                    signUp();
                } else {
                    Toast.makeText(mContext, "Please enter city", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void signUp() {
        AddGuestBody addGuestBody = new AddGuestBody(mNameStr, mGender, mCity);
        Call<ResponseBody> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .guestLogin("Bearer " + AccountUtil.getToken(mContext),
                        addGuestBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String msg = String.valueOf(response.body());
                if (response.code() == 200) {
                    AccountUtil.setGuest(mContext, true);
                    openHomeActivity();
                    // Toast.makeText(mContext, "Station added successfully !!", Toast.LENGTH_SHORT).show();
                } else {
                    AccountUtil.setGuest(mContext, false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void computePackageHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.ambee.android",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!NetworkUtil.checkNetworkConnection(this) && mAlertDialog == null) {
            showAlert(getResources().getString(R.string.no_network_connection),
                    Settings.ACTION_SETTINGS);
        }
    }

    private void showAlert(String blockingMessage, final String settingAction) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.popup__error, null);
        TextView textView = (TextView) view.findViewById(R.id.alert_text);
        textView.setText(blockingMessage);
        TextView ok = (TextView) view.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (settingAction.equals(Settings.ACTION_LOCATION_SOURCE_SETTINGS)) {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                } else if (settingAction.equals(android.provider.Settings.ACTION_DATE_SETTINGS)) {
                    startActivityForResult(
                            new Intent(android.provider.Settings.ACTION_DATE_SETTINGS), 0);
                } else if (settingAction.equals(Settings.ACTION_SETTINGS)) {
                    startActivity(new Intent(Settings.ACTION_SETTINGS));
                }
                if (mAlertDialog != null) {
                    mAlertDialog.cancel();
                }
            }
        });
        mAlertDialog = builder.show();
        mAlertDialog.setCancelable(false);
        mAlertDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    private void fbSignIn() {
        // set permissions
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // set facebook token
                //  AccountUtil.setToken(mContext, loginResult.getAccessToken().getToken());
                // for getting data email, profile pic , name
                handleFacebookAccessToken(loginResult);
            }

            @Override
            public void onCancel() {
                try {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }
            }
        });
    }

    private void handleFacebookAccessToken(LoginResult loginResult) {
        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        String res = response.toString();
                        // Application code
                        try {
                            String id = object.getString("id");
                            String name = object.getString("name");
                            String email = object.getString("email");
                            URL imageURL = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
                            AccountUtil.setName(mContext, name);
                            AccountUtil.setEmail(mContext, email);
                            AccountUtil.setUserPic(mContext, imageURL.toString());
                            //data post to the server
                            serverCall("Facebook");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (MalformedURLException e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email");
        request.setParameters(parameters);
        request.executeAsync();
    }

    // source = Google, facebook
    private void serverCall(String source) {
        Call<UserLogin> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .createUser(source, AccountUtil.getName(mContext),
                        AccountUtil.getEmail(mContext), AccountUtil.getUserPic(mContext));
        // to execute the HTTP call
        call.enqueue(new Callback<UserLogin>() {
            @Override
            public void onResponse(Call<UserLogin> call, Response<UserLogin> response) {
                UserLogin userLogin = response.body();
                AccountUtil.setGuest(mContext, false);
                AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                AccountUtil.setToken(mContext, userLogin.getToken().getToken());
                AccountUtil.setUserId(mContext, userLogin.getToken().getUserId());
                // Navigate to Home activity and save data in shareprefrences
                openHomeActivity();
            }

            @Override
            public void onFailure(Call<UserLogin> call, Throwable t) {
                Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                // signout from google
                mGoogleSignInClient.signOut();
                // signout from fb
                LoginManager.getInstance().logOut();
            }
        });
    }

    private void openHomeActivity() {
        Intent i = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(i);
        // close this activity
        finish();
    }

    private void googleSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void configureGoogleLogin() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(SERVERCLIENT_ID)
                .requestServerAuthCode(SERVERCLIENT_ID, false)
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initUi() {
        mLogo = (ImageView) findViewById(R.id.imgSplash);
        mTextLogo = (TextView) findViewById(R.id.textLogo);
        mGoogleImg = (ImageView) findViewById(R.id.googleLoginImg);
        mLoginLayout = (LinearLayout) findViewById(R.id.login);
        mOrTxt = (TextView) findViewById(R.id.or);
        mGuestTxt = (TextView) findViewById(R.id.guestSignIn);
        mLoginText = (TextView) findViewById(R.id.loginText);
        mMainLay = (LinearLayout) findViewById(R.id.main_layout);
        mGuestLay = (LinearLayout) findViewById(R.id.signUp);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String email = account.getEmail();
            String name = account.getDisplayName();
            String imageUrl = String.valueOf(account.getPhotoUrl());
            AccountUtil.setName(mContext, name);
            AccountUtil.setEmail(mContext, email);
            AccountUtil.setUserPic(mContext, imageUrl);
            // for sending token on server
            // AccountUtil.setToken(mContext, account.getIdToken());
            // Signed in successfully, show authenticated UI.
            serverCall("Google");
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            int error = e.getStatusCode();
        }
    }
}
