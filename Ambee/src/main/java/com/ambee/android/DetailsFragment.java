package com.ambee.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.RecommendationAdapter;
import com.ambee.android.Data.AnalyticsData;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IFillFormatter;
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;
import static com.facebook.FacebookSdk.getApplicationContext;

public class DetailsFragment extends Fragment {
    private String mParameter = "";
    private View mView;
    private Context mContext;
    private String mId = "";
    private TextView mValueTxt, mUnitTxt, mStatusTxt;
    private LinearLayout mMainLay;
    private LineChart mChart;
    private TextView mParaHead, mParaDescription, mParaCauseHead;
    private LinearLayout mCauseLay, mRecomLay;
    private TextView mHealthValue1, mHealthValue2, mHealthText1, mHealthText2;
    private ImageView mHealthImg1, mHealthImg2;
    private ListView mListRecommendation;
    // for analytics duration = h(hours) , duration=d(day),duration=m(month)
    private String duration = "d";
    // Map data <Parameter(para + "_" + date),<value,date>>
    private LinkedHashMap<String, LinkedHashMap<Float, String>> mGraphDataMap =
            new LinkedHashMap<>();
    // set graph data
    private String mSelectedPara = "PM 2.5";
    private String mSelectedUnit = "ug/m3";
    private List<ModelItem> mRecommendationItems = new ArrayList<>();
    private RecommendationAdapter mRecommendationAdapter;
    private Activity mActivity;


    public DetailsFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public DetailsFragment(Activity activity) {
        // Required empty public constructor
        mActivity = activity;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_details, container, false);
        mContext = getApplicationContext();
        initUi();
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            mParameter = bundle.getString("SELECTED_PARA", "");
            mId = bundle.getString("ID", "");
        }
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken();
        } else {
            // get details
            getPlaceDetails();
            //getGraphData();
        }

        // display chart
        //setupChart();

        return mView;
    }

    private void setupChart() {
        mChart.setViewPortOffsets(0, 0, 0, 0);
        //  mChart.setBackgroundColor(mContext.getResources().getColor(R.color.chart_g));
        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        // disable zoom
        mChart.setScaleEnabled(false);

        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(300);

        XAxis x = mChart.getXAxis();
        // x.setDrawGridLines(true);
        // x.setGridColor(mContext.getResources().getColor(R.color.separator));
        x.setEnabled(false);

        YAxis yLeft = mChart.getAxisLeft();
        // yLeft.setDrawGridLines(true);
        // yLeft.setGridColor(mContext.getResources().getColor(R.color.separator));
        yLeft.setEnabled(false);
       /* y.setTypeface(tfLight);
        y.setLabelCount(6, false);
        y.setTextColor(Color.WHITE);
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        y.setDrawGridLines(false);
        y.setAxisLineColor(Color.WHITE);*/

        mChart.getAxisRight().setEnabled(false);

        mChart.getLegend().setEnabled(false);
        mChart.animateXY(2000, 2000);
        // refresh the drawing
        mChart.invalidate();
    }

    private void setGraphData() {

        ArrayList<Entry> values = new ArrayList<>();
        values.clear();
        // for entry data on bar chart
        LinkedHashMap<String, Float> eventsCount = new LinkedHashMap<>();
        eventsCount.clear();
        // to set label xAxis
        final ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.clear();

        for (Map.Entry<String, LinkedHashMap<Float, String>> data : mGraphDataMap.entrySet()) {
            String parameter = data.getKey().split("_")[0];
            if (parameter.equals(mSelectedPara)) {
                LinkedHashMap<Float, String> value = data.getValue();
                for (Map.Entry<Float, String> data1 : value.entrySet()) {
                    Float parValue = data1.getKey();
                    String time_date = data1.getValue();
                    String time = time_date.split(",")[0];
                    String date = time_date.split(",")[1];
                    String yLabel = date;
                    if (eventsCount != null && eventsCount.containsKey(yLabel)) {
                        eventsCount.put(yLabel, parValue);
                    } else {
                        eventsCount.put(yLabel, parValue);
                    }
                    if (xAxisLabel != null && !xAxisLabel.contains(yLabel)) {
                        xAxisLabel.add(yLabel);
                    }
                }
            }
        }

        for (int i = 0; i < xAxisLabel.size(); i++) {
            values.add(new Entry(i, eventsCount.get(xAxisLabel.get(i))));
        }


        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        mSelectedUnit = ParameterUtil.getParameterUnit(mContext, mSelectedPara);
        // set marker view to show data
        XYMarkerView mv = new XYMarkerView(getApplicationContext(), new XaxisValueFormatter(xAxisLabel, "Day"),
                "Location", mSelectedUnit);
        mv.setChartView(mChart);
        mChart.setMarker(mv);

        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(true);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(5f);
            set1.setDrawHorizontalHighlightIndicator(false);
            set1.setCircleColor(mContext.getResources().getColor(R.color.appText));
            set1.setHighLightColor(Color.rgb(244, 117, 117));
            set1.setColor(mContext.getResources().getColor(R.color.appText));
            set1.setFillColor(mContext.getResources().getColor(R.color.white));
            set1.setFillAlpha(450);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mChart.getAxisLeft().getAxisMinimum();
                }
            });

            // create a data object with the data sets
            LineData data = new LineData(set1);
            // data.setValueTypeface(tfLight);
            data.setValueTextSize(9f);
            data.setDrawValues(false);

            // set data
            mChart.setData(data);
        }
    }

    private void initUi() {
        mValueTxt = (TextView) mView.findViewById(R.id.value);
        mUnitTxt = (TextView) mView.findViewById(R.id.unit);
        mStatusTxt = (TextView) mView.findViewById(R.id.status);
        mMainLay = (LinearLayout) mView.findViewById(R.id.main);
        mChart = (LineChart) mView.findViewById(R.id.lineChart);
        mParaHead = (TextView) mView.findViewById(R.id.paraHead);
        mParaDescription = (TextView) mView.findViewById(R.id.paraDescription);
        mParaCauseHead = (TextView) mView.findViewById(R.id.paraCauseHead);
        mCauseLay = (LinearLayout) mView.findViewById(R.id.causeLay);
        mRecomLay = (LinearLayout) mView.findViewById(R.id.recomLay);
        mListRecommendation = (ListView) mView.findViewById(R.id.recomList);
        mHealthValue1 = (TextView) mView.findViewById(R.id.value1);
        mHealthValue2 = (TextView) mView.findViewById(R.id.value2);
        mHealthValue1.setVisibility(View.GONE);
        mHealthValue2.setVisibility(View.GONE);
        mHealthText1 = (TextView) mView.findViewById(R.id.text1);
        mHealthText2 = (TextView) mView.findViewById(R.id.text2);
        mHealthImg1 = (ImageView) mView.findViewById(R.id.img1);
        mHealthImg2 = (ImageView) mView.findViewById(R.id.img2);
    }

    // check and get token after every 5 min
    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());

                            // get details
                            getPlaceDetails();
                            //  getGraphData();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mActivity, getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getPlaceDetails() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<LatestData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationLatestData("Bearer " +
                            AccountUtil.getToken(mContext), mId);

            call.enqueue(new Callback<LatestData>() {
                @Override
                public void onResponse(Call<LatestData> call, Response<LatestData> response) {
                    LatestData latestData = response.body();
                    if (latestData != null) {
                        LinkedHashMap<String, String> hashMap = latestData.getData();
                        if (hashMap != null && hashMap.size() > 0) {
                            setStatusData(hashMap, latestData.getDataType());
                            setCauseEffect(hashMap);
                            setHealthEffect(hashMap);
                            setRecommendation(hashMap);
                            // setTabTitle(hashMap, latestData.getDataType());
                            // setOthersDetails(hashMap, latestData.getDataType());
                            // setRecommendation();
                            // setBarGraph(hashMap, latestData.getDataType());
                        }
                    }
                }

                @Override
                public void onFailure(Call<LatestData> call, Throwable t) {
                    Toast.makeText(mActivity, t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mActivity, getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setCauseEffect(LinkedHashMap<String, String> hashMap) {
        mCauseLay.removeAllViews();
        String para = "";
        String value = "";
        Drawable icon = null;
        if (hashMap.containsKey(mParameter)) {
            value = getValue(hashMap.get(mParameter));
        }
        switch (mParameter) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = mParameter;
                break;
        }
        switch (para) {
            case "PM 2.5":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "PM 10":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "PM 1":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "CO2":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "TVOC":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "HCHO":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "O3":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "CO":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "NO2":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "SO2":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            case "AQI":
                icon = mContext.getResources().getDrawable(R.drawable.combustion_factory);
                addCauseView(icon, "Combustion from\npower plants");
                icon = mContext.getResources().getDrawable(R.drawable.garbage_burning);
                addCauseView(icon, "Smoke and soot\nfrom garbage\nburning");
                icon = mContext.getResources().getDrawable(R.drawable.vehicular_emission);
                addCauseView(icon, "Vehicular emissions");
                break;
            default:
                break;
        }
    }

    private void addCauseView(Drawable icon, String msg) {
        LayoutInflater inflater = LayoutInflater.from(mActivity);
        View view = inflater.inflate(R.layout.item_cause_details, mCauseLay,
                false);
        LinearLayout main = (LinearLayout) view.findViewById(R.id.main);
        ImageView img = (ImageView) view.findViewById(R.id.img);
        TextView statusTxt = (TextView) view.findViewById(R.id.status);
        mCauseLay.addView(view);

        if (icon != null)
            img.setImageDrawable(icon);
        statusTxt.setText(msg);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Toast.makeText(mContext, "Show details", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setRecommendation(LinkedHashMap<String, String> hashMap) {
        mRecommendationItems.clear();
        mListRecommendation.setFocusable(false);
        String para = "";
        String value = "";
        Drawable icon = null;
        if (hashMap.containsKey(mParameter)) {
            value = getValue(hashMap.get(mParameter));
        }
        switch (mParameter) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = mParameter;
                break;
        }
        // set background image
        if (value != null && !value.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.good_exercise),
                            "Safe for exercising and heavy physical activities",
                            "Clean air quality in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.no_purifier),
                            "No air purifier required",
                            "Good air quality detected in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Enjoy the clean air", "No safety precautions required"));
                    break;
                case "orange":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Mediocre pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier if possible",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Exercise caution in outdoors",
                            "Reduces exposure to poor air quality"));
                    break;
                case "red":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks when outdoor",
                            "Reduce exposure to poor air quality"));
                    break;
                case "black":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Avoid all outdoor activities",
                            "Catastrophic pollution levels detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks extensively",
                            "Reduce exposure to poor air quality"));
                    break;
                default:
                    break;
            }
            if (mRecommendationItems != null && mRecommendationItems.size() > 0) {
                mRecommendationAdapter = new RecommendationAdapter(mContext, mRecommendationItems);
                mListRecommendation.setAdapter(mRecommendationAdapter);
            } else {

            }
        }
    }

    private void setHealthEffect(LinkedHashMap<String, String> hashMap) {
        String para = "";
        String value = "";
        Drawable icon = null;
        if (hashMap.containsKey(mParameter)) {
            value = getValue(hashMap.get(mParameter));
        }
        switch (mParameter) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = mParameter;
                break;
        }
        switch (para) {
            case "PM 2.5":
                break;
            case "PM 10":
                break;
            case "PM 1":
                break;
            case "CO2":
                break;
            case "TVOC":
                break;
            case "HCHO":
                break;
            case "O3":
                break;
            case "CO":
                break;
            case "NO2":
                break;
            case "SO2":
                break;
            default:
                break;
        }
        mHealthImg1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.eye_rritation));
        mHealthText1.setText("Irritation to the eyes,\nthroat, and nose");
        mHealthImg2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.chest_tightness));
        mHealthText1.setText("Coughing, chest\ntightness\nand shortness of breath");
    }


    private void getGraphData() {
        mGraphDataMap = new LinkedHashMap<>();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<AnalyticsData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationAnalytics("Bearer " +
                            AccountUtil.getToken(mContext), mId, duration);

            call.enqueue(new Callback<AnalyticsData>() {
                @Override
                public void onResponse(Call<AnalyticsData> call, Response<AnalyticsData> response) {
                    AnalyticsData graphData = response.body();
                    if (graphData != null) {
                        List<LinkedHashMap<String, String>> graphDataList = graphData.getData();
                        if (graphDataList != null) {
                            mGraphDataMap = new LinkedHashMap<>();
                            for (int i = 0; i < graphDataList.size(); i++) {
                                LinkedHashMap<String, String> hashMap = graphDataList.get(i);
                                if (hashMap != null && hashMap.size() > 0) {
                                    for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                                        String para = "";
                                        String key = entry.getKey();
                                        String val = entry.getValue();
                                        String date = hashMap.get("dateTime");
                                        switch (key) {
                                            case "PM25":
                                                para = "PM 2.5";
                                                val = getValue(val);
                                                break;
                                            case "PM10":
                                                para = "PM 10";
                                                val = getValue(val);
                                                break;
                                            case "PM1":
                                                para = "PM 1";
                                                val = getValue(val);
                                                break;
                                            case "CO2":
                                                para = "CO2";
                                                val = getValue(val);
                                                break;
                                            case "TVOC":
                                                para = "TVOC";
                                                val = getValue(val);
                                                break;
                                            case "HCHO":
                                                para = "HCHO";
                                                val = getValue(val);
                                                break;
                                            case "OZONE":
                                                para = "O3";
                                                val = getValue(val);
                                                break;
                                            case "CO":
                                                para = "CO";
                                                val = getValue(val);
                                                break;
                                            case "NO2":
                                                para = "NO2";
                                                val = getValue(val);
                                                break;
                                            case "SO2":
                                                para = "SO2";
                                                val = getValue(val);
                                                break;
                                            case "AQI":
                                                para = "AQI";
                                                val = getValue(val);
                                                break;
                                            default:
                                                break;

                                        }
                                        if (!para.isEmpty() && para.length() > 0) {
                                            double value = ParameterUtil.getParameterValue(mContext, para, val);
                                            LinkedHashMap<Float, String> dataMap = new LinkedHashMap<>();
                                            dataMap.put((float) value, date);
                                            if (mGraphDataMap != null && mGraphDataMap.containsKey(para)) {
                                                LinkedHashMap<Float, String> tempHashMap = mGraphDataMap.get(para);
                                                mGraphDataMap.put(para + "_" + date, tempHashMap);
                                            } else {
                                                mGraphDataMap.put(para + "_" + date, dataMap);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        setGraphData();
                    }
                }

                @Override
                public void onFailure(Call<AnalyticsData> call, Throwable t) {
                    Toast.makeText(mActivity, t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mActivity, getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setStatusData(LinkedHashMap<String, String> hashMap, String type) {
        String title = "Location";
        if (hashMap.containsKey("name")) {
            title = hashMap.get("name")
                    + "\n" + hashMap.get("city") + ", " +
                    hashMap.get("countryCode");

        } else {
            title = hashMap.get("placeName")
                    + "\n" + hashMap.get("city") + ", " +
                    hashMap.get("countryCode");
        }

        // for showing first object
        String para = "";
        String value = "";
        if (hashMap.containsKey(mParameter)) {
            value = getValue(hashMap.get(mParameter));
        }
        switch (mParameter) {
            case "PM25":
                para = "PM 2.5";
                break;
            case "PM10":
                para = "PM 10";
                break;
            case "PM1":
                para = "PM 1";
                break;
            case "OZONE":
                para = "O3";
                break;
            default:
                para = mParameter;
                break;
        }
        setParaDescription(para);
        // convert server data to user understanding data
        double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
        // get unit of parameter
        String unit = ParameterUtil.getParameterUnit(mContext, para);
        mUnitTxt.setText(unit);
        if (paraVal == 0.0) {
            mValueTxt.setText("NA");
            mUnitTxt.setText("");
        } else
            mValueTxt.setText(String.valueOf(paraVal));

        //  mPlaceTxt.setText(placeName + "\n" + city + ", " + country);
        // set background image
        if (value != null && !value.isEmpty()) {
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                    mStatusTxt.setText("Good");
                    break;
                case "orange":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                    mStatusTxt.setText("Mediocre");
                    break;
                case "red":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                    mStatusTxt.setText("Dangerous");
                    break;
                case "black":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                    mStatusTxt.setText("Catastrophic");
                    break;
                default:
                    break;
            }
        }
    }

    private void setParaDescription(String para) {
        mParaHead.setText("What is " + para);
        mParaCauseHead.setText("Major causes of " + para);
        switch (para) {
            case "PM 2.5":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_pm_25));
                break;
            case "PM 10":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_pm_10));
                break;
            case "PM 1":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_pm_25));
                break;
            case "CO2":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_co));
                break;
            case "TVOC":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_pm_25));
                break;
            case "HCHO":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_hcho));
                break;
            case "O3":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_o3));
                break;
            case "CO":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_co));
                break;
            case "NO2":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_no2));
                break;
            case "SO2":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_so2));
                break;
            case "AQI":
                mParaDescription.setText(mContext.getResources().getString(R.string.info_aqi));
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
