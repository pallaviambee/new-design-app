package com.ambee.android.Retrofit;

import com.ambee.android.Data.AddCityBody;
import com.ambee.android.Data.AddDeviceBody;
import com.ambee.android.Data.AddFeedbackBody;
import com.ambee.android.Data.AddGuestBody;
import com.ambee.android.Data.AnalyticsData;
import com.ambee.android.Data.DeviceIdFromQR;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.Ranking;
import com.ambee.android.Data.RenameCityBody;
import com.ambee.android.Data.RenameDeviceBody;
import com.ambee.android.Data.UserLogin;
import com.ambee.android.Data.UserNotifications;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    /* @POST request step:
     * 1. set @FormUrlEncoded
     * 2. append base url by   @POST("user/login")
     * 3. for sending data use @Field("source") where source is key on server */

    // For post login data username, img url, email,source(Facebook ,google)
    @FormUrlEncoded
    @POST("auth/login")
    Call<UserLogin> createUser(
            @Field("source") String source,
            @Field("name") String name,
            @Field("email") String email,
            @Field("userpic") String userpic
    );

    // for refreshToken
    @FormUrlEncoded
    @POST("auth/refreshToken")
    Call<UserRefreshToken> createToken(
            @Header("Authorization") String authHeader,
            @Field("email") String email
    );

    /* @GET request step:
     * 1. append base url by   @GET("user-location/saved")
     * 2. for getting data use @Query("userId") where source is key on server
     * http://52.66.212.208:3001/user-location/saved?userId=5c765017f172515e544a3bf9*/

    // get saved stations
    @GET("stations/fav")
    Call<UserStations> getStations(
            @Header("Authorization") String authHeader
    );

    // more then 1 query
    // http://52.66.212.208:3001/stations?lat=12.912811&lng=77.60922&radius=1.5&limit=20
    // For get nearest location
    @GET("stations/nearby")
    Call<UserStations> getUserNearestStation(
            @Header("Authorization") String authHeader,
            @Query("lat") double lat,
            @Query("lng") double lng,
            @Query("name") String name,
            @Query("radius") String radius,
            @Query("limit") String limit,
            @Query("range") String range
    );

    @GET("notification")
    Call<UserNotifications> getNotification(
            @Header("Authorization") String authHeader
    );

    // Add city having @body para so no need of @FormUrlEncoded
    @POST("stations/fav")
    Call<ResponseBody> addStation(
            @Header("Authorization") String authHeader,
            @Body AddCityBody addCityBody
    );


    // get data having @query
    @DELETE("stations/fav/{placeId}")
    Call<ResponseBody> deleteCity(
            @Header("Authorization") String authHeader,
            @Path("placeId") String placeId
    );

    @PUT("stations/fav/{placeId}")
    Call<ResponseBody> renameCity(
            @Header("Authorization") String authHeader,
            @Body RenameCityBody renameCityBody,
            @Path("placeId") String placeId
    );

    @DELETE("devices/{id}")
    Call<ResponseBody> deleteDevice(
            @Header("Authorization") String authHeader,
            @Path("id") String id
    );

    @PUT("devices/{id}")
    Call<ResponseBody> renameDevice(
            @Header("Authorization") String authHeader,
            @Path("id") String id,
            @Body RenameDeviceBody renameDeviceBody
    );

    @GET("stations/best-worst/{type}/{selection}")
    Call<Ranking> getBestWorstPlaces(
            @Header("Authorization") String authHeader,
            @Path("type") String type,
            @Path("selection") String selection,
            @Query("lat") double lat,
            @Query("lng") double lng
    );

    @GET("device/devices")
    Call<DeviceIdFromQR> getDeviceId(
            @Header("Authorization") String authHeader,
            @Query("device_id") String id
    );

    @POST("devices")
    Call<ResponseBody> registerDevice(
            @Header("Authorization") String authHeader,
            @Body AddDeviceBody addDeviceBody
    );

    @POST("feedback")
    Call<ResponseBody> userFeedback(
            @Header("Authorization") String authHeader,
            @Body AddFeedbackBody addFeedbackBody
    );

    @POST("guest")
    Call<ResponseBody> guestLogin(
            @Header("Authorization") String authHeader,
            @Body AddGuestBody addGuestBody
    );

    @GET("devices")
    Call<UserStations> getDevices(
            @Header("Authorization") String authHeader
    );

    @GET("devices/{deviceId}/graph-info")
    Call<AnalyticsData> getDeviceAnalytics(
            @Header("Authorization") String authHeader,
            @Path("deviceId") String deviceId,
            @Query("duration") String duration
    );

    @GET("stations/{stationId}/history")
    Call<AnalyticsData> getStationAnalytics(
            @Header("Authorization") String authHeader,
            @Path("stationId") String stationId,
            @Query("duration") String duration
    );

    @GET("devices/{deviceId}/latest-info")
    Call<LatestData> getDeviceLatestData(
            @Header("Authorization") String authHeader,
            @Path("deviceId") String deviceId
    );

    @GET("stations/{stationId}/latest-info")
    Call<LatestData> getStationLatestData(
            @Header("Authorization") String authHeader,
            @Path("stationId") String stationId
    );

    @GET("devices/{deviceId}/weather-info")
    Call<LatestData> getDeviceWeatherData(
            @Header("Authorization") String authHeader,
            @Path("deviceId") String deviceId
    );

    @GET("stations/{stationId}/weather-info")
    Call<LatestData> getStationWeatherData(
            @Header("Authorization") String authHeader,
            @Path("stationId") String stationId
    );
}
