package com.ambee.android.Retrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


// singleton class
public class RetrofitClient {
    //private static final String BASE_URL = "https://1u2owwnrb0.execute-api.ap-south-1.amazonaws.com/dev/";
    private static final String BASE_URL = "https://bkd389qu6j.execute-api.ap-south-1.amazonaws.com/dev/";
    // To get device id from QR code
    private static final String DEVICE_ID_URL = "https://j7qo4rdtm6.execute-api.ap-south-1.amazonaws.com/";
    // client object
    private static RetrofitClient mIntance;
    // Retrofit object
    private Retrofit mRetrofit;

    /* Retrofit steps :
       https://square.github.io/retrofit/
     * 1. build retrofit object
     * 2. Set base url
     * 3. set Gson factory for parsing
     * 4. build used everywhere to call @GET @POST etc call
     */

    public RetrofitClient(String skyUrl) {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(skyUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public RetrofitClient() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    // get single intance
    public static synchronized RetrofitClient getIntance() {
        if (mIntance == null) {
            mIntance = new RetrofitClient();
        }
        return mIntance;
    }

    // get api
    public ApiInterface getApi() {
        return mRetrofit.create(ApiInterface.class);
    }
}
