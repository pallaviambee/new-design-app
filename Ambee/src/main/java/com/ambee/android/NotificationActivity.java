package com.ambee.android;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.ambee.android.Adapter.NotificationAdapter;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserNotifications;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends BaseActivity {
    private Context mContext;
    private ListView mList;
    private AlertDialog mAlertDia;
    private ImageView mBack;
    private NotificationAdapter mAdapter;
    private List<ModelItem> mDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        initUi();
        mContext = getApplicationContext();
        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken("Notify");
        } else {
            getDataService();
        }
        setAdapter();
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void checkToken(final String str) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            if (str.equals("Notify")) {
                                getDataService();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getDataService() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserNotifications> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getNotification("Bearer " + AccountUtil.getToken(mContext));
            call.enqueue(new Callback<UserNotifications>() {
                @Override
                public void onResponse(Call<UserNotifications> call, Response<UserNotifications> response) {
                    UserNotifications userNotifications = response.body();
                    if (userNotifications != null) {
                        List<LinkedHashMap<String, String>> data = userNotifications.getData();
                        if (data != null && data.size() > 0) {
                            for (int i = 0; i < data.size(); i++) {
                                LinkedHashMap<String, String> hashMap = data.get(i);
                                mDataList.add(new ModelItem(hashMap.get("name"), hashMap.get("value"),
                                        hashMap.get("mainMessage"),
                                        hashMap.get("subMessage"), hashMap.get("dateTime"), true));
                            }
                        }
                    } else {

                    }
                    setAdapter();
                }

                @Override
                public void onFailure(Call<UserNotifications> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void setAdapter() {
        if (mDataList != null && mDataList.size() > 0) {
            mAdapter = new NotificationAdapter(NotificationActivity.this, mContext, mDataList);
            mList.setAdapter(mAdapter);
        } else {

        }
    }

    private void initUi() {
        mBack = (ImageView) findViewById(R.id.back);
        mList = (ListView) findViewById(R.id.listView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
