package com.ambee.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.NetworkUtil;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import static com.ambee.android.Constant.AuthConstant.SERVERCLIENT_ID;

public class SettingActivity extends BaseActivity {
    private Context mContext;
    private ImageView mBack, mRewardImg, mReferralImg, mFavouritesImg, mSupportImg, mLogoutImg;
    private TextView mHeading, mRewardTxt, mReferralTxt, mFavouritesTxt, mSupportTxt, mLogoutTxt;
    private ScrollView mOptionLay;
    private AlertDialog mAlertDia;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        initUi();

        mContext = getApplicationContext();

        mFavouritesImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, FavouritesActivity.class);
                startActivity(intent);
            }
        });
        mFavouritesTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, FavouritesActivity.class);
                startActivity(intent);
            }
        });

        mSupportTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, SupportActivity.class);
                startActivity(intent);
            }
        });
        mSupportImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, SupportActivity.class);
                startActivity(intent);
            }
        });
        mLogoutImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureGoogleLogin();
                showLogoutDialog();
            }
        });
        mLogoutTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configureGoogleLogin();
                showLogoutDialog();
            }
        });
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mRewardImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, RewardsActivity.class);
                startActivity(intent);
            }
        });
        mRewardTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, RewardsActivity.class);
                startActivity(intent);
            }
        });
        mReferralTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referLink();
            }
        });
        mReferralImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                referLink();
            }
        });
    }

    private void referLink() {
        String link = "https://play.google.com/store/apps/details?id=com.ambee.android";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        // For gmail subject
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Referral App Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, link);
        startActivity(Intent.createChooser(sharingIntent,
                AccountUtil.getName(getApplicationContext()) +
                        " invited you to join Ambee. Monitor your air quality and keep your friends " +
                        "and family safe.\n" +
                        "Joining link: " + link));
    }

    private void initUi() {
        mHeading = (TextView) findViewById(R.id.heading);
        mBack = (ImageView) findViewById(R.id.back);
        mOptionLay = (ScrollView) findViewById(R.id.optionLay);
        mRewardImg = (ImageView) findViewById(R.id.rewardTimeLineImg);
        mRewardTxt = (TextView) findViewById(R.id.rewardTimeLine);
        mReferralImg = (ImageView) findViewById(R.id.referrelCodeImg);
        mReferralTxt = (TextView) findViewById(R.id.referrelCode);
        mFavouritesImg = (ImageView) findViewById(R.id.favouritesImg);
        mFavouritesTxt = (TextView) findViewById(R.id.favouritesTxt);
        mSupportImg = (ImageView) findViewById(R.id.supportImg);
        mSupportTxt = (TextView) findViewById(R.id.supportTxt);
        mLogoutImg = (ImageView) findViewById(R.id.logImg);
        mLogoutTxt = (TextView) findViewById(R.id.logTxt);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void configureGoogleLogin() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(SERVERCLIENT_ID)
                .requestServerAuthCode(SERVERCLIENT_ID, false)
                .build();
        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void showLogoutDialog() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.prompts, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                SettingActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        heading.setText(mContext.getResources().getString(R.string.want_to_logout));
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.checkNetworkConnection(mContext)) {
                    if (mAlertDia != null)
                        mAlertDia.dismiss();
                    AccountUtil.setToken(mContext, "");
                    AccountUtil.setName(mContext, "");
                    AccountUtil.setEmail(mContext, "");
                    AccountUtil.setUserPic(mContext, "");
                    AccountUtil.setUserId(mContext, "");
                    // signout from google
                    mGoogleSignInClient.signOut();
                    // signout from fb
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(SettingActivity.this, SplashActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
