package com.ambee.android;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SupportActivity extends BaseActivity {
    private LinearLayout mSupportLay;
    private ImageView mBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);
        mBack = (ImageView) findViewById(R.id.back);
        mSupportLay = (LinearLayout) findViewById(R.id.supportLay);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
