package com.ambee.android;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.RecommendationAdapter;
import com.ambee.android.Data.AddCityBody;
import com.ambee.android.Data.AnalyticsData;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.Util.ParameterUtil.getValue;

public class HomeActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Context mContext;
    // menu button
    private ImageView mSearchImg, mNotificationImg,
            mListImg;
    private ImageView mFavImg, mShareImg;
    private TextView mPlaceTxt;
    private ImageView mBgIcon;
    private TextView mValueTxt, mUnitTxt, mStatusTxt;
    private LinearLayout mPollutantLay, mNearbyLay;
    private LinearLayout mBuyDeviceLay, mAddDeviceLay;
    private TextView mBuyDeviceTxt, mAddDeviceTxt;
    private ImageView mEmo1, mEmo2, mEmo3, mEmo4, mEmo5;
    private TextView mHealthValue1, mHealthValue2;

    // below navigation
    private LinearLayout mHomeLayout, mMapLayout, mRankLayout, mProfileLayout;
    private ImageView mHomeImg, mMapImg, mRankImg, mProfileImg;
    private TextView mHomeTxt, mMapText, mRankText, mProfileText;

    // For recommendation
    private ExpandableHeightListView mListRecommendation;
    private List<ModelItem> mRecommendationItems = new ArrayList<>();
    private RecommendationAdapter mRecommendationAdapter;

    private List<ModelItem> mDataList = new ArrayList<>();
    private AlertDialog mAlertDia;
    // nearest place id
    private String mStationId = "";
    // nearest place name
    private String mCurrentPlace = "";
    public static String mCurrentLatitude;
    public static String mCurrentLongitude;
    private String mAddress = "";
    private String mPlaceName = "";

    private String mFilterDistance = "250";
    private String mFilterStr = "all";

    // for analytics duration = h(hours) , duration=d(day),duration=m(month)
    private String duration = "d";
    // Map data <Parameter(para + "_" + date),<value,date>>
    private LinkedHashMap<String, LinkedHashMap<Float, String>> mGraphDataMap =
            new LinkedHashMap<>();
    // set graph data
    private String mSelectedPara = "PM 2.5";
    private String mSelectedUnit = "ug/m3";
    private LinearLayout mMainLay;
    private boolean mIsSaved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();
        mContext = getApplicationContext();

        try {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds


        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken(false, "");
        } else {
            // for nearest place
            getNearestLocation();
        }

        // For below navigation
        setBelowNavigation();

        // For my devices (connect device with wifi and order a device)
        setNavigationForDevice();

        // display chart
        // setupChart();

        // share App link
        mShareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAppLink();
            }
        });

        // search stations
        mSearchImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                startActivity(intent);
            }
        });

        setFeedback();

        mListImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FavouritesActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mNotificationImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, NotificationActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mFavImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken(mIsSaved, "Action");
                    } else {
                        if (mIsSaved) {
                            deletePopup(mStationId, mCurrentPlace, "Place");
                        } else
                            addPlace();
                    }
                } else {
                    showSignInPopup();
                }
            }
        });
    }

    private void setFeedback() {
        mEmo1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("SECLECTED", 1);
                    intent.putExtra("VALUE", "Terrible");
                    intent.putExtra("PLACEID", mStationId);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mEmo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("SECLECTED", 2);
                    intent.putExtra("VALUE", "Poor");
                    intent.putExtra("PLACEID", mStationId);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mEmo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("SECLECTED", 3);
                    intent.putExtra("VALUE", "Mediocre");
                    intent.putExtra("PLACEID", mStationId);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mEmo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("SECLECTED", 4);
                    intent.putExtra("VALUE", "Good");
                    intent.putExtra("PLACEID", mStationId);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mEmo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, FeedbackActivity.class);
                    intent.putExtra("SECLECTED", 5);
                    intent.putExtra("VALUE", "Great");
                    intent.putExtra("PLACEID", mStationId);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
    }

    private void showSignInPopup() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(HomeActivity.this);
        View promptsView = li.inflate(R.layout.prompts_signin, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                HomeActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountUtil.setGuest(mContext, false);
                Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void addPlace() {
        if (mStationId != null && mStationId.length() > 0) {
            AddCityBody addCityBody = new AddCityBody(mStationId, mCurrentPlace);
            Call<ResponseBody> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .addStation("Bearer " + AccountUtil.getToken(mContext),
                            addCityBody);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String msg = String.valueOf(response.body());
                    if (response.code() == 200) {
                        refreshStatus();
                        getCleanerPlaces();
                        Toast.makeText(mContext, "Station added successfully", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }


    private void shareAppLink() {
        String link = "https://play.google.com/store/apps/details?id=com.ambee.android";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        // For gmail subject
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ambee App Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, link);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

/*    private void setupChart() {
        mChart.setViewPortOffsets(0, 0, 0, 0);
        //  mChart.setBackgroundColor(mContext.getResources().getColor(R.color.chart_g));
        // no description text
        mChart.getDescription().setEnabled(false);

        // enable touch gestures
        mChart.setTouchEnabled(true);

        // enable scaling and dragging
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);

        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(false);

        // disable zoom
        mChart.setScaleEnabled(false);

        mChart.setDrawGridBackground(false);
        mChart.setMaxHighlightDistance(300);

        XAxis x = mChart.getXAxis();
        // x.setDrawGridLines(true);
        // x.setGridColor(mContext.getResources().getColor(R.color.separator));
        x.setEnabled(false);

        YAxis yLeft = mChart.getAxisLeft();
        // yLeft.setDrawGridLines(true);
        // yLeft.setGridColor(mContext.getResources().getColor(R.color.separator));
        yLeft.setEnabled(false);
       *//* y.setTypeface(tfLight);
        y.setLabelCount(6, false);
        y.setTextColor(Color.WHITE);
        y.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        y.setDrawGridLines(false);
        y.setAxisLineColor(Color.WHITE);*//*

        mChart.getAxisRight().setEnabled(false);

        mChart.getLegend().setEnabled(false);
        mChart.animateXY(2000, 2000);
        // refresh the drawing
        mChart.invalidate();
    }*/

/*    private void setGraphData() {

        ArrayList<Entry> values = new ArrayList<>();
        values.clear();
        // for entry data on bar chart
        LinkedHashMap<String, Float> eventsCount = new LinkedHashMap<>();
        eventsCount.clear();
        // to set label xAxis
        final ArrayList<String> xAxisLabel = new ArrayList<>();
        xAxisLabel.clear();

        for (Map.Entry<String, LinkedHashMap<Float, String>> data : mGraphDataMap.entrySet()) {
            String parameter = data.getKey().split("_")[0];
            if (parameter.equals(mSelectedPara)) {
                LinkedHashMap<Float, String> value = data.getValue();
                for (Map.Entry<Float, String> data1 : value.entrySet()) {
                    Float parValue = data1.getKey();
                    String time_date = data1.getValue();
                    String time = time_date.split(",")[0];
                    String date = time_date.split(",")[1];
                    String yLabel = date;
                    if (eventsCount != null && eventsCount.containsKey(yLabel)) {
                        eventsCount.put(yLabel, parValue);
                    } else {
                        eventsCount.put(yLabel, parValue);
                    }
                    if (xAxisLabel != null && !xAxisLabel.contains(yLabel)) {
                        xAxisLabel.add(yLabel);
                    }
                }
            }
        }

        for (int i = 0; i < xAxisLabel.size(); i++) {
            values.add(new Entry(i, eventsCount.get(xAxisLabel.get(i))));
        }


        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);
        mSelectedUnit = ParameterUtil.getParameterUnit(mContext, mSelectedPara);
        // set marker view to show data
        XYMarkerView mv = new XYMarkerView(getApplicationContext(), new XaxisValueFormatter(xAxisLabel, "Day"),
                "Location", mSelectedUnit);
        mv.setChartView(mChart);
        mChart.setMarker(mv);

        LineDataSet set1;
        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setValues(values);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            // create a dataset and give it a type
            set1 = new LineDataSet(values, "DataSet 1");

            set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
            set1.setCubicIntensity(0.2f);
            set1.setDrawFilled(true);
            set1.setDrawCircles(true);
            set1.setLineWidth(1.8f);
            set1.setCircleRadius(5f);
            set1.setDrawHorizontalHighlightIndicator(false);
            set1.setCircleColor(mContext.getResources().getColor(R.color.appText));
            set1.setHighLightColor(Color.rgb(244, 117, 117));
            set1.setColor(mContext.getResources().getColor(R.color.appText));
            set1.setFillColor(mContext.getResources().getColor(R.color.white));
            set1.setFillAlpha(450);
            set1.setFillFormatter(new IFillFormatter() {
                @Override
                public float getFillLinePosition(ILineDataSet dataSet, LineDataProvider dataProvider) {
                    return mChart.getAxisLeft().getAxisMinimum();
                }
            });

            // create a data object with the data sets
            LineData data = new LineData(set1);
            // data.setValueTypeface(tfLight);
            data.setValueTextSize(9f);
            data.setDrawValues(false);

            // set data
            mChart.setData(data);
        }
    }*/

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(LocationServices.API).build();
    }


    private void getNearestLocation() {
        if (mCurrentLatitude == null && mCurrentLongitude == null) {
            return;
        }
        if (mCurrentLatitude.isEmpty() && mCurrentLongitude.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            getAddress();
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(mCurrentLatitude), Double.parseDouble(mCurrentLongitude),
                            mPlaceName, mFilterDistance, "1", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    // get clear places
                    getCleanerPlaces();
                    UserStations userSearchCity = response.body();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        String para = "";
                        String value = "";
                        String city = "";
                        String country = "";
                        boolean isSaved = false;
                        String placeName = "";
                        if (stations != null && stations.size() > 0) {
                            LinkedHashMap<String, String> hashMap = stations.get(0);
                            if (hashMap != null) {
                                if (hashMap.containsKey("fav")) {
                                    isSaved = Boolean.parseBoolean(hashMap.get("fav"));
                                }
                                if (hashMap.containsKey("placeId")) {
                                    mStationId = hashMap.get("placeId");
                                }
                                if (hashMap.containsKey("name")) {
                                    placeName = hashMap.get("name");
                                }
                                if (placeName.isEmpty()) {
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }
                                if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                } else if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("OZONE")) {
                                    para = "O3";
                                    value = getValue(hashMap.get("OZONE"));
                                } else if (hashMap.containsKey("CO")) {
                                    para = "CO";
                                    value = getValue(hashMap.get("CO"));
                                } else if (hashMap.containsKey("NO2")) {
                                    para = "NO2";
                                    value = getValue(hashMap.get("NO2"));
                                } else if (hashMap.containsKey("SO2")) {
                                    para = "SO2";
                                    value = getValue(hashMap.get("SO2"));
                                }
                                mSelectedPara = para;
                            }
                            setStatus(para, value, placeName, city, country, isSaved);
                            getLatestData();
                        } else {

                        }
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void refreshStatus() {
        if (mCurrentLatitude == null && mCurrentLongitude == null) {
            return;
        }
        if (mCurrentLatitude.isEmpty() && mCurrentLongitude.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            getAddress();
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(mCurrentLatitude), Double.parseDouble(mCurrentLongitude),
                            mPlaceName, mFilterDistance, "1", mFilterStr);
            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    UserStations userSearchCity = response.body();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        String para = "";
                        String value = "";
                        String city = "";
                        String country = "";
                        boolean isSaved = false;
                        String placeName = "";
                        if (stations != null && stations.size() > 0) {
                            LinkedHashMap<String, String> hashMap = stations.get(0);
                            if (hashMap != null) {
                                if (hashMap.containsKey("fav")) {
                                    isSaved = Boolean.parseBoolean(hashMap.get("fav"));
                                }
                                if (hashMap.containsKey("placeId")) {
                                    mStationId = hashMap.get("placeId");
                                }
                                if (hashMap.containsKey("name")) {
                                    placeName = hashMap.get("name");
                                }
                                if (placeName.isEmpty()) {
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                }
                                if (hashMap.containsKey("city")) {
                                    city = hashMap.get("city");
                                }
                                if (hashMap.containsKey("countryCode")) {
                                    country = hashMap.get("countryCode");
                                }
                                if (hashMap.containsKey("AQI")) {
                                    para = "AQI";
                                    value = getValue(hashMap.get("AQI"));
                                } else if (hashMap.containsKey("PM25")) {
                                    para = "PM 2.5";
                                    value = getValue(hashMap.get("PM25"));
                                } else if (hashMap.containsKey("PM10")) {
                                    para = "PM 10";
                                    value = getValue(hashMap.get("PM10"));
                                } else if (hashMap.containsKey("OZONE")) {
                                    para = "O3";
                                    value = getValue(hashMap.get("OZONE"));
                                } else if (hashMap.containsKey("CO")) {
                                    para = "CO";
                                    value = getValue(hashMap.get("CO"));
                                } else if (hashMap.containsKey("NO2")) {
                                    para = "NO2";
                                    value = getValue(hashMap.get("NO2"));
                                } else if (hashMap.containsKey("SO2")) {
                                    para = "SO2";
                                    value = getValue(hashMap.get("SO2"));
                                }
                                mSelectedPara = para;
                            }
                            setStatus(para, value, placeName, city, country, isSaved);
                        } else {

                        }
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }

    }

    private void setHealthEffect(LinkedHashMap<String, String> latestData, String type) {
        String paraTxt = "", valueTxt = "";
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String para = entry.getKey();
            String value = entry.getValue();
            switch (type) {
                case "device":
                    if (para.equals("AQI")) {
                        paraTxt = "AQI";
                        valueTxt = getValue(value);
                    } else if (para.equals("PM25")) {
                        paraTxt = "PM 2.5";
                        valueTxt = getValue(value);
                    }
                   /* if (para.equals("PM10")) {
                        paraTxt = "PM 10";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("PM1")) {
                        paraTxt = "PM 1";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("CO2")) {
                        paraTxt = "CO2";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("TVOC")) {
                        paraTxt = "TVOC";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("HCHO")) {
                        paraTxt = "HCHO";
                        valueTxt = getValue(value);
                    }*/
                    break;
                case "station":
                    if (para.equals("AQI")) {
                        paraTxt = "AQI";
                        valueTxt = getValue(value);
                    } else if (para.equals("PM25")) {
                        paraTxt = "PM 2.5";
                        valueTxt = getValue(value);
                    }
                   /* if (para.equals("PM10")) {
                        paraTxt = "PM 10";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("OZONE")) {
                        paraTxt = "O3";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("CO")) {
                        paraTxt = "CO";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("NO2")) {
                        paraTxt = "NO2";
                        valueTxt = getValue(value);
                    }
                    if (para.equals("SO2")) {
                        paraTxt = "SO2";
                        valueTxt = getValue(value);
                    }*/
                    break;
            }
        }
        if (paraTxt != null && paraTxt.length() > 0) {
            double v2 = (Integer.valueOf(valueTxt) - 22) / 22.0;
            v2 = Math.round(v2 * 100.0) / 100.0;
            if (Integer.valueOf(valueTxt) > 22) {
                mHealthValue2.setText(String.valueOf(v2));
            } else {
                mHealthValue2.setText("0");
            }
            mHealthValue1.setText(valueTxt);
        }
    }

    private void getCleanerPlaces() {
        mFilterStr = "good";
        if (mCurrentLatitude == null && mCurrentLongitude == null) {
            return;
        }
        if (mCurrentLatitude.isEmpty() && mCurrentLongitude.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(mCurrentLatitude),
                            Double.parseDouble(mCurrentLongitude), mAddress,
                            mFilterDistance, "4", mFilterStr);

            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    UserStations userSearchCity = response.body();
                    mDataList.clear();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        if (Integer.parseInt(userSearchCity.getCount()) > 0) {
                            String cityId = "";
                            String para = "";
                            String value = "";
                            String placeName = "";
                            String city = "";
                            String country = "";
                            boolean isSaved = false;
                            for (int i = 0; i < stations.size(); i++) {
                                LinkedHashMap<String, String> hashMap = stations.get(i);
                                if (hashMap != null) {
                                    if (hashMap.containsKey("fav")) {
                                        isSaved = Boolean.parseBoolean(hashMap.get("fav"));
                                    }
                                    if (hashMap.containsKey("placeId")) {
                                        cityId = hashMap.get("placeId");
                                    }
                                    if (hashMap.containsKey("name")) {
                                        placeName = hashMap.get("name");
                                    }
                                    if (placeName.isEmpty()) {
                                        if (hashMap.containsKey("placeName")) {
                                            placeName = hashMap.get("placeName");
                                        }
                                    }
                                    if (hashMap.containsKey("city")) {
                                        city = hashMap.get("city");
                                    }
                                    if (hashMap.containsKey("countryCode")) {
                                        country = hashMap.get("countryCode");
                                    }
                                    if (hashMap.containsKey("AQI")) {
                                        para = "AQI";
                                        value = getValue(hashMap.get("AQI"));
                                    } else if (hashMap.containsKey("PM25")) {
                                        para = "PM 2.5";
                                        value = getValue(hashMap.get("PM25"));
                                    } else if (hashMap.containsKey("PM10")) {
                                        para = "PM 10";
                                        value = getValue(hashMap.get("PM10"));
                                    } else if (hashMap.containsKey("OZONE")) {
                                        para = "O3";
                                        value = getValue(hashMap.get("OZONE"));
                                    } else if (hashMap.containsKey("CO")) {
                                        para = "CO";
                                        value = getValue(hashMap.get("CO"));
                                    } else if (hashMap.containsKey("NO2")) {
                                        para = "NO2";
                                        value = getValue(hashMap.get("NO2"));
                                    } else if (hashMap.containsKey("SO2")) {
                                        para = "SO2";
                                        value = getValue(hashMap.get("SO2"));
                                    }
                                    // addNearbyView(para, value, placeName, city, country);
                                    mDataList.add(new ModelItem(para, value, placeName, city,
                                            cityId, country, "stations", isSaved));
                                }
                            }
                        }
                    }
                    if (mDataList != null && mDataList.size() > 0) {
                        addNearbyView();
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void addNearbyView() {
        mNearbyLay.removeAllViews();
        for (int i = 0; i < mDataList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
            final View view = inflater.inflate(R.layout.item_cleaner_details, mNearbyLay, false);
            TextView placeTxt = (TextView) view.findViewById(R.id.place);
            CardView main = (CardView) view.findViewById(R.id.card);
            final ImageView img = (ImageView) view.findViewById(R.id.fav);
            TextView addressTxt = (TextView) view.findViewById(R.id.address);
            LinearLayout mainLayout = (LinearLayout) view.findViewById(R.id.main);
            TextView valueTxt = (TextView) view.findViewById(R.id.value);
            TextView unitTxt = (TextView) view.findViewById(R.id.unit);
            TextView statusTxt = (TextView) view.findViewById(R.id.status);

            final String placeName = mDataList.get(i).getLocation();
            String city = mDataList.get(i).getCity();
            final String cityId = mDataList.get(i).getDevId();
            String country = mDataList.get(i).getCountry();
            String para = mDataList.get(i).getParameter();
            String value = mDataList.get(i).getValue();
            final boolean isSaved = mDataList.get(i).getIsSaved();
            if (isSaved)
                img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_heart));
            else {
                img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fav));
                img.setColorFilter(mContext.getResources().getColor(R.color.icon_color));
            }
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCurrentPlace = placeName;
                    mStationId = cityId;
                    if (!AccountUtil.isGuest(mContext)) {
                        if (DateTimeUtil.checkTime(getApplicationContext())) {
                            // call for get token
                            checkToken(isSaved, "Action");
                        } else {
                            if (isSaved) {
                                deletePopup(cityId, placeName, "Place");
                            } else
                                addPlace();
                        }
                    } else {
                        showSignInPopup();
                    }
                }
            });
            placeTxt.setText(placeName);
            addressTxt.setText(city + ", " + country);
            String unit = ParameterUtil.getParameterUnit(mContext, para);
            unitTxt.setText(unit);

            main.setTag(i);
            main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(HomeActivity.this,
                            HomeDetailsActivity.class);
                    intent.putExtra("IsSaved", isSaved);
                    intent.putExtra("CityId",
                            mDataList.get(Integer.parseInt(v.getTag().toString())).getDevId());
                    intent.putExtra("IsFromHome", true);
                    startActivity(intent);
                }
            });
            if (value != null && !value.isEmpty()) {
                // convert server data to user understanding data
                double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
                String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
                valueTxt.setText(String.valueOf(paraVal));
                switch (colour) {
                    case "green":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                        statusTxt.setText("Good");
                        break;
                    case "orange":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                        statusTxt.setText("Mediocre");
                        break;
                    case "red":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                        statusTxt.setText("Dangerous");
                        break;
                    case "black":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                        statusTxt.setText("Catastrophic");
                        break;
                    default:
                        break;
                }
            }
            mNearbyLay.addView(view);
        }
    }

    private void deletePopup(final String cityId, String placeName,
                             final String type) {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(HomeActivity.this);
        View promptsView = li.inflate(R.layout.prompts_delete, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                HomeActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        heading.setText("Are you sure you want to remove" + "\n" + placeName
                + " from your saved places");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                deleteDeviceService(cityId, type);
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void deleteDeviceService(String cityId, final String type) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<ResponseBody> call = null;
            if (type.equals("Place")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteCity("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            } else if (type.equals("Device")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteDevice("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            }
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.code() == 200) {
                        getCleanerPlaces();
                        refreshStatus();
                        Toast.makeText(mContext, "Deleted successfully.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, "Not deleted, Please try again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setRecommendation(LinkedHashMap<String, String> latestData, String type) {
        mListRecommendation.setFocusable(false);
        mRecommendationItems.clear();
        String para = "", paraValue = "";
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Drawable icon = null;
            switch (type) {
                case "device":
                    if (key.equals("AQI")) {
                        para = "AQI";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM1")) {
                        para = "PM 1";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO2")) {
                        para = "CO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("TVOC")) {
                        para = "TVOC";
                        paraValue = getValue(value);
                    }
                    if (key.equals("HCHO")) {
                        para = "HCHO";
                        paraValue = getValue(value);
                    }
                    break;
                case "station":
                    if (key.equals("AQI")) {
                        para = "AQI";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("OZONE")) {
                        para = "O3";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO")) {
                        para = "CO";
                        paraValue = getValue(value);
                    }
                    if (key.equals("NO2")) {
                        para = "NO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("SO2")) {
                        para = "SO2";
                        paraValue = getValue(value);
                    }
                    break;
            }
        }
        // set background image
        if (paraValue != null && !paraValue.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, paraValue);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.good_exercise),
                            "Safe for exercising and heavy physical activities",
                            "Clean air quality in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.no_purifier),
                            "No air purifier required",
                            "Good air quality detected in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Enjoy the clean air", "No safety precautions required"));
                    break;
                case "orange":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Mediocre pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier if possible",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Exercise caution in outdoors",
                            "Reduces exposure to poor air quality"));
                    break;
                case "red":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks when outdoor",
                            "Reduce exposure to poor air quality"));
                    break;
                case "black":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Avoid all outdoor activities",
                            "Catastrophic pollution levels detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks extensively",
                            "Reduce exposure to poor air quality"));
                    break;
                default:
                    break;
            }
            if (mRecommendationItems != null && mRecommendationItems.size() > 0) {
                mRecommendationAdapter = new RecommendationAdapter(mContext, mRecommendationItems);
                mListRecommendation.setAdapter(mRecommendationAdapter);
            } else {

            }
        }
    }

    private void setStatus(String para, String value, String placeName, String city, String
            country, boolean isSaved) {
        mIsSaved = isSaved;
        if (isSaved)
            mFavImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_heart));
        else
            mFavImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fav));

        // convert server data to user understanding data
        double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
        // get unit of parameter
        String unit = ParameterUtil.getParameterUnit(mContext, para);
        mValueTxt.setText(String.valueOf(paraVal));
        mUnitTxt.setText(unit);
        mValueTxt.setText(String.valueOf(paraVal));
        mUnitTxt.setText(unit);
        mCurrentPlace = placeName;
        mPlaceTxt.setText(placeName + "\n" + city + ", " + country);
        // set background image
        if (value != null && !value.isEmpty()) {
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_bg_icon));
                    mStatusTxt.setText("Good");
                    mStatusTxt.setText("Good");
                    break;
                case "orange":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                    mStatusTxt.setText("Mediocre");
                    mStatusTxt.setText("Mediocre");
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.green_bg_icon));
                    mBgIcon.setColorFilter(mContext.getResources().getColor(R.color.orange_bg_icon));
                    break;
                case "red":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                    mStatusTxt.setText("Dangerous");
                    mStatusTxt.setText("Dangerous");
                    mBgIcon.setColorFilter(mContext.getResources().getColor(R.color.red_bg_icon));
                    mStatusTxt.setText("Dangerous");
                    break;
                case "black":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                    mStatusTxt.setText("Catastrophic");
                    mStatusTxt.setText("Catastrophic");
                    mBgIcon.setImageDrawable(mContext.getResources().getDrawable(R.drawable.black_bg_icon));
                    break;
                default:
                    break;
            }
        }
    }


    private void getAddress() {
        Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(Double.parseDouble(mCurrentLatitude),
                    Double.parseDouble(mCurrentLongitude), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addresses != null && addresses.size() > 0) {
            mAddress = addresses.get(0).getAddressLine(0);
            mPlaceName = addresses.get(0).getFeatureName() + "," +
                    addresses.get(0).getSubLocality();
        } else {
            // do your stuff
        }
    }

    private void setBelowNavigation() {
        mHomeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        mHomeImg.setColorFilter(mContext.getResources().getColor(R.color.white));
        mHomeTxt.setVisibility(View.VISIBLE);
        mHomeTxt.setTextColor(mContext.getResources().getColor(R.color.white));
        mHomeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken(false, "");
                } else {
                    getNearestLocation();
                }
            }
        });
        mHomeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken(false, "");
                } else {
                    getNearestLocation();
                }
            }
        });
        mHomeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DateTimeUtil.checkTime(getApplicationContext())) {
                    // call for get token
                    checkToken(false, "");
                } else {
                    getNearestLocation();
                }
            }
        });
        mMapLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });
        mMapText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, MapActivity.class);
                startActivity(intent);
            }
        });

        mRankLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mRankText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });
        mProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mProfileImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mProfileText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
    }

    private void setNavigationForDevice() {
        mAddDeviceLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(HomeActivity.this, ConnectDeviceScreen1Activity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mAddDeviceTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    finish();
                    Intent intent = new Intent(HomeActivity.this, ConnectDeviceScreen1Activity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });

        mBuyDeviceLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this,
                            ShopActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
        mBuyDeviceTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    Intent intent = new Intent(HomeActivity.this,
                            ShopActivity.class);
                    startActivity(intent);
                } else {
                    showSignInPopup();
                }
            }
        });
    }


    // id = device id or station id for getting details of data
    private void getLatestData() {
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        // mGraphDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<LatestData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationLatestData("Bearer " +
                            token, mStationId);
            call.enqueue(new Callback<LatestData>() {
                @Override
                public void onResponse(Call<LatestData> call, Response<LatestData> response) {
                    LatestData latestData = response.body();
                    if (latestData != null) {
                        LinkedHashMap<String, String> hashMap = latestData.getData();
                        if (hashMap != null && hashMap.size() > 0) {
                            setOthersDetails(hashMap, latestData.getDataType());
                            setRecommendation(hashMap, latestData.getDataType());
                            setHealthEffect(hashMap, latestData.getDataType());
                            // getGraphData();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LatestData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void getGraphData() {
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        mGraphDataMap = new LinkedHashMap<>();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<AnalyticsData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationAnalytics("Bearer " +
                            token, mStationId, duration);

            call.enqueue(new Callback<AnalyticsData>() {
                @Override
                public void onResponse(Call<AnalyticsData> call, Response<AnalyticsData> response) {
                    AnalyticsData graphData = response.body();
                    if (graphData != null) {
                        List<LinkedHashMap<String, String>> graphDataList = graphData.getData();
                        if (graphDataList != null) {
                            mGraphDataMap = new LinkedHashMap<>();
                            for (int i = 0; i < graphDataList.size(); i++) {
                                LinkedHashMap<String, String> hashMap = graphDataList.get(i);
                                if (hashMap != null && hashMap.size() > 0) {
                                    for (Map.Entry<String, String> entry : hashMap.entrySet()) {
                                        String para = "";
                                        String key = entry.getKey();
                                        String val = entry.getValue();
                                        String date = hashMap.get("dateTime");
                                        switch (key) {
                                            case "AQI":
                                                para = "AQI";
                                                val = getValue(val);
                                                break;
                                            case "PM25":
                                                para = "PM 2.5";
                                                val = getValue(val);
                                                break;
                                            case "PM10":
                                                para = "PM 10";
                                                val = getValue(val);
                                                break;
                                            case "PM1":
                                                para = "PM 1";
                                                val = getValue(val);
                                                break;
                                            case "CO2":
                                                para = "CO2";
                                                val = getValue(val);
                                                break;
                                            case "TVOC":
                                                para = "TVOC";
                                                val = getValue(val);
                                                break;
                                            case "HCHO":
                                                para = "HCHO";
                                                val = getValue(val);
                                                break;
                                            case "OZONE":
                                                para = "O3";
                                                val = getValue(val);
                                                break;
                                            case "CO":
                                                para = "CO";
                                                val = getValue(val);
                                                break;
                                            case "NO2":
                                                para = "NO2";
                                                val = getValue(val);
                                                break;
                                            case "SO2":
                                                para = "SO2";
                                                val = getValue(val);
                                                break;
                                            default:
                                                break;
                                        }
                                        if (!para.isEmpty() && para.length() > 0) {
                                            double value = ParameterUtil.getParameterValue(mContext, para, val);
                                            LinkedHashMap<Float, String> dataMap = new LinkedHashMap<>();
                                            dataMap.put((float) value, date);
                                            if (mGraphDataMap != null && mGraphDataMap.containsKey(para)) {
                                                LinkedHashMap<Float, String> tempHashMap = mGraphDataMap.get(para);
                                                mGraphDataMap.put(para + "_" + date, tempHashMap);
                                            } else {
                                                mGraphDataMap.put(para + "_" + date, dataMap);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //setGraphData();
                    }
                }

                @Override
                public void onFailure(Call<AnalyticsData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setOthersDetails(LinkedHashMap<String, String> latestData,
                                  String type) {
        mPollutantLay.removeAllViews();
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Drawable icon = null;
            String city = "";
            String country = "";
            String placeName = "";
            String para = "";
            switch (type) {
                case "device":
                    if (latestData.containsKey("name")) {
                        placeName = latestData.get("name");
                    }
                    if (placeName.isEmpty()) {
                        if (latestData.containsKey("placeName")) {
                            placeName = latestData.get("placeName");
                        }
                    }
                    if (latestData.containsKey("city")) {
                        city = latestData.get("city");
                    }
                    if (latestData.containsKey("countryCode")) {
                        country = latestData.get("countryCode");
                    }
                    if (key.equals("AQI")) {
                        para = "AQI";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM1")) {
                        para = "PM 1";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("CO2")) {
                        para = "CO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.co2);
                        addView(para, value, icon);
                    }
                    if (key.equals("TVOC")) {
                        para = "TVOC";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("HCHO")) {
                        para = "HCHO";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }

                    break;
                case "station":
                    if (latestData.containsKey("name")) {
                        placeName = latestData.get("name");
                    }
                    if (placeName.isEmpty()) {
                        if (latestData.containsKey("placeName")) {
                            placeName = latestData.get("placeName");
                        }
                    }
                    if (latestData.containsKey("city")) {
                        city = latestData.get("city");
                    }
                    if (latestData.containsKey("countryCode")) {
                        country = latestData.get("countryCode");
                    }
                    if (key.equals("AQI")) {
                        para = "AQI";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("OZONE")) {
                        para = "O3";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("CO")) {
                        para = "CO";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("NO2")) {
                        para = "NO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("SO2")) {
                        para = "SO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    break;
            }
        }
    }

    private void addView(String para, String value, Drawable icon) {
        if (value != null && !value.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            if (paraVal == 0.0)
                return;
        }
        LayoutInflater inflater = LayoutInflater.from(HomeActivity.this);
        View view = inflater.inflate(R.layout.item_other_details, mPollutantLay,
                false);
        final TextView parameter = (TextView) view.findViewById(R.id.para);
        TextView valueText = (TextView) view.findViewById(R.id.value);
        ImageView info = (ImageView) view.findViewById(R.id.info);
        CardView main = (CardView) view.findViewById(R.id.card);
        mPollutantLay.addView(view);
        if (icon != null)
            info.setImageDrawable(icon);

        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(mContext, "Show details", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(HomeActivity.this, TabActivity.class);
                // ID (cityID/deviceId)
                intent.putExtra("ID", mStationId);
                intent.putExtra("Para", parameter.getText().toString());
                startActivity(intent);
            }
        });
        if (value != null && !value.isEmpty()) {
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            parameter.setText(para);
            valueText.setText(String.valueOf(paraVal));

            switch (colour) {
                case "green":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.green_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.green_s));
                    break;
                case "orange":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.orange_s));
                    break;
                case "red":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.red_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.red_s));
                    break;
                case "black":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.black_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.black_s));
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }


    private void initUi() {
        mSearchImg = (ImageView) findViewById(R.id.search);
        mNotificationImg = (ImageView) findViewById(R.id.notification);
        mListImg = (ImageView) findViewById(R.id.list);
        mFavImg = (ImageView) findViewById(R.id.fav);
        mShareImg = (ImageView) findViewById(R.id.share);
        mBgIcon = (ImageView) findViewById(R.id.bg_icon);
        mPlaceTxt = (TextView) findViewById(R.id.place);
        mValueTxt = (TextView) findViewById(R.id.value);
        mUnitTxt = (TextView) findViewById(R.id.unit);
        mStatusTxt = (TextView) findViewById(R.id.status);
        mPollutantLay = (LinearLayout) findViewById(R.id.pollutantLay);
        mHealthValue1 = (TextView) findViewById(R.id.value1);
        mHealthValue2 = (TextView) findViewById(R.id.value2);
        mBuyDeviceLay = (LinearLayout) findViewById(R.id.buyLay);
        mBuyDeviceTxt = (TextView) findViewById(R.id.buyDevice);
        mAddDeviceLay = (LinearLayout) findViewById(R.id.addLay);
        mAddDeviceTxt = (TextView) findViewById(R.id.buyDevice);
        mListRecommendation = (ExpandableHeightListView) findViewById(R.id.recomList);
        mEmo1 = (ImageView) findViewById(R.id.emo1);
        mEmo2 = (ImageView) findViewById(R.id.emo2);
        mEmo3 = (ImageView) findViewById(R.id.emo3);
        mEmo4 = (ImageView) findViewById(R.id.emo4);
        mEmo5 = (ImageView) findViewById(R.id.emo5);
        mNearbyLay = (LinearLayout) findViewById(R.id.nearbyLay);
        mMainLay = (LinearLayout) findViewById(R.id.main);

        mHomeLayout = (LinearLayout) findViewById(R.id.home);
        mHomeImg = (ImageView) findViewById(R.id.homeImg);
        mHomeTxt = (TextView) findViewById(R.id.homeTxt);

        mMapLayout = (LinearLayout) findViewById(R.id.map);
        mMapImg = (ImageView) findViewById(R.id.mapImg);
        mMapText = (TextView) findViewById(R.id.mapTxt);

        mRankLayout = (LinearLayout) findViewById(R.id.rank);
        mRankImg = (ImageView) findViewById(R.id.rankImg);
        mRankText = (TextView) findViewById(R.id.rankTxt);

        mProfileLayout = (LinearLayout) findViewById(R.id.profile);
        mProfileImg = (ImageView) findViewById(R.id.profileImg);
        mProfileText = (TextView) findViewById(R.id.profileTxt);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken(false, "");
        } else {
            getNearestLocation();
        }
        //Now lets connect to the API
        mGoogleApiClient.connect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private Drawable getIconColour(Drawable drawable, String color) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), bitmap);
        d.setColorFilter(Color.parseColor(color), PorterDuff.Mode.SRC_ATOP);
        Bitmap bitmap1 = ((BitmapDrawable) d).getBitmap();
        return d;
    }

    private Drawable getIcon(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        Drawable d = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap,
                40, 40, true));
        d.setColorFilter(Color.parseColor("#616161"), PorterDuff.Mode.SRC_ATOP);
        Bitmap bitmap1 = ((BitmapDrawable) d).getBitmap();
        return d;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // check and get token after every 5 min
    private void checkToken(final boolean status, final String action) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            if (action.equals("Action")) {
                                if (status) {
                                    deletePopup(mStationId, mCurrentPlace, "Place");
                                } else
                                    addPlace();
                            } else  // get nearest location
                                getNearestLocation();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        android.location.Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        } else {
            //If everything went fine lets get latitude and longitude
            mCurrentLatitude = String.valueOf(location.getLatitude());
            mCurrentLongitude = String.valueOf(location.getLongitude());
            getNearestLocation();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this,
                        CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             *If no resolution is available, display a dialog to the
             *user with the error.
             */
            //   Log.e("Error", "Location services connection failed with code " + connectionResult.getErrorCode());
        }
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        mCurrentLatitude = String.valueOf(location.getLatitude());
        mCurrentLongitude = String.valueOf(location.getLongitude());
        getNearestLocation();
    }

}
