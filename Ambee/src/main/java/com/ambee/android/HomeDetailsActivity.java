package com.ambee.android;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Adapter.RecommendationAdapter;
import com.ambee.android.Data.AddCityBody;
import com.ambee.android.Data.LatestData;
import com.ambee.android.Data.ModelItem;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Data.UserStations;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;
import com.ambee.android.Util.ParameterUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ambee.android.HomeActivity.mCurrentLatitude;
import static com.ambee.android.HomeActivity.mCurrentLongitude;
import static com.ambee.android.Util.ParameterUtil.getValue;

public class HomeDetailsActivity extends BaseActivity {
    private Context mContext;
    private AlertDialog mAlertDia;
    private ImageView mBackImg, mFavImg, mShareImg;
    private TextView mPlaceTxt, mValueTxt, mUnitTxt, mStatusTxt;
    private LinearLayout mPollutantLay, mNearbyLay;
    private LinearLayout mMainLay;
    private TextView mHealthValue1, mHealthValue2;
    // For recommendation
    private ListView mListRecommendation;
    private List<ModelItem> mRecommendationItems = new ArrayList<>();
    private RecommendationAdapter mRecommendationAdapter;
    // nearest place id or from search city id
    private String mStationId = "";
    private String mDeviceId = "";
    // nearest place name
    private String mCurrentPlace = "";
    // is nearest place saved
    private boolean mIsSaved = false;
    private String mAddress = "";
    private String mPlaceName = "";
    private boolean isMore = true;
    private String mFilterDistance = "250";
    private String mFilterStr = "all";
    private boolean mIsFromSearch = false;
    private TextView mHeading;
    private List<ModelItem> mDataList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_details);
        mContext = getApplicationContext();
        initUi();
        // details page
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mIsSaved = bundle.getBoolean("IsSaved", false);
            mIsFromSearch = bundle.getBoolean("IsFromSearch", false);
            mStationId = bundle.getString("CityId", "");
            mDeviceId = bundle.getString("devId", "");
        }
        if (mIsFromSearch) {
            mHeading.setText("Popular place");
        } else {
            mHeading.setText("Cleaner place");
        }
        //check time for token 5 min expire
        if (!AccountUtil.isGuest(mContext) && DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken(false, "");
        } else {
            getCleanerPlaces();
            getLatestData();
        }
        // share App link
        mShareImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAppLink();
            }
        });
        mBackImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mFavImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AccountUtil.isGuest(mContext)) {
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken(mIsSaved, "Action");
                    } else {
                        if (mIsSaved) {
                            deletePopup(mStationId, mCurrentPlace, "Place");
                        } else
                            addPlace();
                    }
                } else {
                    showSignInPopup();
                }
            }
        });

    }

    private void showSignInPopup() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(HomeDetailsActivity.this);
        View promptsView = li.inflate(R.layout.prompts_signin, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                HomeDetailsActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountUtil.setGuest(mContext, false);
                Intent intent = new Intent(HomeDetailsActivity.this, SplashActivity.class);
                startActivity(intent);
                finish();
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void addPlace() {
        if (mStationId != null && mStationId.length() > 0) {
            AddCityBody addCityBody = new AddCityBody(mStationId, mCurrentPlace);
            Call<ResponseBody> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .addStation("Bearer " + AccountUtil.getToken(mContext),
                            addCityBody);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String msg = String.valueOf(response.body());
                    if (response.code() == 200) {
                        getCleanerPlaces();
                        getLatestData();
                        Toast.makeText(mContext, "Station added successfully !!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        }
    }


    private void shareAppLink() {
        String link = "https://play.google.com/store/apps/details?id=com.ambee.android";
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        // For gmail subject
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Ambee App Link");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, link);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    private void setHealthEffect(LinkedHashMap<String, String> latestData, String type) {
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String para = entry.getKey();
            String value = entry.getValue();
            switch (type) {
                case "device":
                    if (para.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        mHealthValue1.setText(value);
                        mHealthValue2.setText(value);
                    }
                    if (para.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                    }
                    if (para.equals("PM1")) {
                        para = "PM 1";
                        value = getValue(value);
                    }
                    if (para.equals("CO2")) {
                        para = "CO2";
                        value = getValue(value);
                    }
                    if (para.equals("TVOC")) {
                        para = "TVOC";
                        value = getValue(value);
                    }
                    if (para.equals("HCHO")) {
                        para = "HCHO";
                        value = getValue(value);
                    }

                    break;
                case "station":
                    if (para.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        mHealthValue1.setText(value);
                        mHealthValue2.setText(value);
                    }
                    if (para.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                    }
                    if (para.equals("OZONE")) {
                        para = "O3";
                        value = getValue(value);
                    }
                    if (para.equals("CO")) {
                        para = "CO";
                        value = getValue(value);
                    }
                    if (para.equals("NO2")) {
                        para = "NO2";
                        value = getValue(value);
                    }
                    if (para.equals("SO2")) {
                        para = "SO2";
                        value = getValue(value);
                    }

                    break;
            }
        }
    }

    private void getCleanerPlaces() {
        mNearbyLay.removeAllViews();
        mFilterStr = "good";
        if (mCurrentLatitude == null && mCurrentLongitude == null) {
            return;
        }
        if (mCurrentLatitude.isEmpty() && mCurrentLongitude.isEmpty()) {
            return;
        }
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserStations> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getUserNearestStation("Bearer " + token,
                            Double.parseDouble(mCurrentLatitude),
                            Double.parseDouble(mCurrentLongitude), mAddress,
                            mFilterDistance, "4", mFilterStr);

            call.enqueue(new Callback<UserStations>() {
                @Override
                public void onResponse(Call<UserStations> call, Response<UserStations> response) {
                    UserStations userSearchCity = response.body();
                    mDataList.clear();
                    if (userSearchCity != null) {
                        List<LinkedHashMap<String, String>> stations = userSearchCity.getStations();
                        if (Integer.parseInt(userSearchCity.getCount()) > 0) {
                            String cityId = "";
                            String para = "";
                            String value = "";
                            String placeName = "";
                            String city = "";
                            String country = "";
                            boolean isSaved = false;
                            for (int i = 0; i < stations.size(); i++) {
                                LinkedHashMap<String, String> hashMap = stations.get(i);
                                if (hashMap != null) {
                                    if (hashMap.containsKey("fav")) {
                                        isSaved = Boolean.parseBoolean(hashMap.get("fav"));
                                    }
                                    if (hashMap.containsKey("placeId")) {
                                        cityId = hashMap.get("placeId");
                                    }
                                    if (hashMap.containsKey("placeName")) {
                                        placeName = hashMap.get("placeName");
                                    }
                                    if (hashMap.containsKey("city")) {
                                        city = hashMap.get("city");
                                    }
                                    if (hashMap.containsKey("countryCode")) {
                                        country = hashMap.get("countryCode");
                                    }
                                    if (hashMap.containsKey("PM25")) {
                                        para = "PM 2.5";
                                        value = getValue(hashMap.get("PM25"));
                                    } else if (hashMap.containsKey("PM10")) {
                                        para = "PM 10";
                                        value = getValue(hashMap.get("PM10"));
                                    } else if (hashMap.containsKey("OZONE")) {
                                        para = "O3";
                                        value = getValue(hashMap.get("OZONE"));
                                    } else if (hashMap.containsKey("CO")) {
                                        para = "CO";
                                        value = getValue(hashMap.get("CO"));
                                    } else if (hashMap.containsKey("NO2")) {
                                        para = "NO2";
                                        value = getValue(hashMap.get("NO2"));
                                    } else if (hashMap.containsKey("SO2")) {
                                        para = "SO2";
                                        value = getValue(hashMap.get("SO2"));
                                    } else if (hashMap.containsKey("AQI")) {
                                        para = "AQI";
                                        value = getValue(hashMap.get("AQI"));
                                    }
                                    mDataList.add(new ModelItem(para, value, placeName, city,
                                            cityId, country, "stations", isSaved));

                                    //  addNearbyView(para, value, placeName, city, country);
                                }
                            }
                        }
                    }
                    if (mDataList != null && mDataList.size() > 0) {
                        addNearbyView();
                    }
                }

                @Override
                public void onFailure(Call<UserStations> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void addNearbyView() {
        mNearbyLay.removeAllViews();
        for (int i = 0; i < mDataList.size(); i++) {
            LayoutInflater inflater = LayoutInflater.from(HomeDetailsActivity.this);
            final View view = inflater.inflate(R.layout.item_cleaner_details, mNearbyLay, false);
            TextView placeTxt = (TextView) view.findViewById(R.id.place);
            CardView main = (CardView) view.findViewById(R.id.card);
            final ImageView img = (ImageView) view.findViewById(R.id.fav);
            TextView addressTxt = (TextView) view.findViewById(R.id.address);
            LinearLayout mainLayout = (LinearLayout) view.findViewById(R.id.main);
            TextView valueTxt = (TextView) view.findViewById(R.id.value);
            TextView unitTxt = (TextView) view.findViewById(R.id.unit);
            TextView statusTxt = (TextView) view.findViewById(R.id.status);

            final String placeName = mDataList.get(i).getLocation();
            String city = mDataList.get(i).getCity();
            final String cityId = mDataList.get(i).getDevId();
            String country = mDataList.get(i).getCountry();
            String para = mDataList.get(i).getParameter();
            String value = mDataList.get(i).getValue();
            final boolean isSaved = mDataList.get(i).getIsSaved();
            if (isSaved)
                img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_heart));
            else {
                img.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fav));
                img.setColorFilter(mContext.getResources().getColor(R.color.icon_color));
            }
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCurrentPlace = placeName;
                    mStationId = cityId;
                    mIsSaved = isSaved;
                    if (!AccountUtil.isGuest(mContext)) {
                        if (DateTimeUtil.checkTime(getApplicationContext())) {
                            // call for get token
                            checkToken(isSaved, "Action");
                        } else {
                            if (isSaved) {
                                deletePopup(cityId, placeName, "Place");
                            } else
                                addPlace();
                        }
                    } else {
                        showSignInPopup();
                    }
                }
            });
            placeTxt.setText(placeName);
            addressTxt.setText(city + ", " + country);
            String unit = ParameterUtil.getParameterUnit(mContext, para);
            unitTxt.setText(unit);

            main.setTag(i);
            main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mIsSaved = isSaved;
                    mStationId = mDataList.get(Integer.parseInt(v.getTag().toString())).getDevId();
                    getCleanerPlaces();
                    getLatestData();
                }
            });
            if (value != null && !value.isEmpty()) {
                // convert server data to user understanding data
                double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
                String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
                valueTxt.setText(String.valueOf(paraVal));
                switch (colour) {
                    case "green":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                        statusTxt.setText("Good");
                        break;
                    case "orange":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                        statusTxt.setText("Mediocre");
                        break;
                    case "red":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                        statusTxt.setText("Dangerous");
                        break;
                    case "black":
                        mainLayout.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                        statusTxt.setText("Catastrophic");
                        break;
                    default:
                        break;
                }
            }
            mNearbyLay.addView(view);
        }
    }

    private void setRecommendation(LinkedHashMap<String, String> latestData, String type) {
        mListRecommendation.setFocusable(false);
        mRecommendationItems.clear();
        String para = "", paraValue = "";
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Drawable icon = null;
            switch (type) {
                case "device":
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM1")) {
                        para = "PM 1";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO2")) {
                        para = "CO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("TVOC")) {
                        para = "TVOC";
                        paraValue = getValue(value);
                    }
                    if (key.equals("HCHO")) {
                        para = "HCHO";
                        paraValue = getValue(value);
                    }
                    break;
                case "station":
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        paraValue = getValue(value);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        paraValue = getValue(value);
                    }
                    if (key.equals("OZONE")) {
                        para = "O3";
                        paraValue = getValue(value);
                    }
                    if (key.equals("CO")) {
                        para = "CO";
                        paraValue = getValue(value);
                    }
                    if (key.equals("NO2")) {
                        para = "NO2";
                        paraValue = getValue(value);
                    }
                    if (key.equals("SO2")) {
                        para = "SO2";
                        paraValue = getValue(value);
                    }
                    break;
            }
        }
        // set background image
        if (paraValue != null && !paraValue.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, paraValue);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.good_exercise),
                            "Safe for exercising and heavy physical activities",
                            "Clean air quality in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.no_purifier),
                            "No air purifier required",
                            "Good air quality detected in your area"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Enjoy the clean air", "No safety precautions required"));
                    break;
                case "orange":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Mediocre pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier if possible",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.details_outdoor),
                            "Exercise caution in outdoors",
                            "Reduces exposure to poor air quality"));
                    break;
                case "red":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Restrict exercising and heavy physical activities",
                            "Pollution detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks when outdoor",
                            "Reduce exposure to poor air quality"));
                    break;
                case "black":
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.outdoor_icon),
                            "Avoid all outdoor activities",
                            "Catastrophic pollution levels detected"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.airpurifier_icon),
                            "Use an air purifier extensively",
                            "Air purifier helps reduce the PM 2.5 levels and PM 10"));
                    mRecommendationItems.add(new ModelItem(mContext.getResources().getDrawable(R.drawable.mask_icon),
                            "Wear masks extensively",
                            "Reduce exposure to poor air quality"));
                    break;
                default:
                    break;
            }
            if (mRecommendationItems != null && mRecommendationItems.size() > 0) {
                mRecommendationAdapter = new RecommendationAdapter(mContext, mRecommendationItems);
                mListRecommendation.setAdapter(mRecommendationAdapter);
            } else {

            }
        }
    }

    private void setStatus(String para, String value, String placeName, String city, String
            country) {
        // convert server data to user understanding data
        double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
        // get unit of parameter
        String unit = ParameterUtil.getParameterUnit(mContext, para);
        if (mIsSaved)
            mFavImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_heart));
        else
            mFavImg.setImageDrawable(mContext.getResources().getDrawable(R.drawable.fav));

        mValueTxt.setText(String.valueOf(paraVal));
        mUnitTxt.setText(unit);
        mPlaceTxt.setText(placeName + "\n" + city + ", " + country);
        // set background image
        if (value != null && !value.isEmpty()) {
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            switch (colour) {
                case "green":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.green_bg));
                    mStatusTxt.setText("Good");
                    break;
                case "orange":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.orange_bg));
                    mStatusTxt.setText("Mediocre");
                    break;
                case "red":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.red_bg));
                    mStatusTxt.setText("Dangerous");
                    break;
                case "black":
                    mMainLay.setBackground(mContext.getResources().getDrawable(R.drawable.black_bg));
                    mStatusTxt.setText("Catastrophic");
                    break;
                default:
                    break;
            }
        }
    }


    // id = device id or station id for getting details of data
    private void getLatestData() {
        String token = AccountUtil.getGuestToken(mContext);
        if (!AccountUtil.isGuest(mContext)) {
            token = AccountUtil.getToken(mContext);
        }
        // mGraphDataList.clear();
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<LatestData> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .getStationLatestData("Bearer " +
                            token, mStationId);
            call.enqueue(new Callback<LatestData>() {
                @Override
                public void onResponse(Call<LatestData> call, Response<LatestData> response) {
                    LatestData latestData = response.body();
                    if (latestData != null) {
                        LinkedHashMap<String, String> hashMap = latestData.getData();
                        if (hashMap != null && hashMap.size() > 0) {
                            setOthersDetails(hashMap, latestData.getDataType());
                            setRecommendation(hashMap, latestData.getDataType());
                            setHealthEffect(hashMap, latestData.getDataType());
                            // getGraphData();
                        }
                    }
                }

                @Override
                public void onFailure(Call<LatestData> call, Throwable t) {
                    Toast.makeText(getBaseContext(), t.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void setOthersDetails(LinkedHashMap<String, String> latestData,
                                  String type) {
        mPollutantLay.removeAllViews();
        for (Map.Entry<String, String> entry : latestData.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Drawable icon = null;
            String city = "";
            String country = "";
            String para = "";
            String placeName = "";
            switch (type) {
                case "device":
                    if (latestData.containsKey("name")) {
                        placeName = latestData.get("name");
                    }
                    if (placeName.isEmpty()) {
                        if (latestData.containsKey("placeName")) {
                            placeName = latestData.get("placeName");
                        }
                    }
                    if (latestData.containsKey("city")) {
                        city = latestData.get("city");
                    }
                    if (latestData.containsKey("fav")) {
                        mIsSaved = Boolean.parseBoolean(latestData.get("fav"));
                    }
                    if (latestData.containsKey("countryCode")) {
                        country = latestData.get("countryCode");
                    }

                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM1")) {
                        para = "PM 1";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("CO2")) {
                        para = "CO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.co2);
                        addView(para, value, icon);
                    }
                    if (key.equals("TVOC")) {
                        para = "TVOC";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("HCHO")) {
                        para = "HCHO";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (para.length() > 0)
                        setStatus(para, value, placeName, city, country);
                    break;
                case "station":
                    if (latestData.containsKey("name")) {
                        placeName = latestData.get("name");
                    }
                    if (placeName.isEmpty()) {
                        if (latestData.containsKey("placeName")) {
                            placeName = latestData.get("placeName");
                        }
                    }
                    if (latestData.containsKey("city")) {
                        city = latestData.get("city");
                    }
                    if (latestData.containsKey("countryCode")) {
                        country = latestData.get("countryCode");
                    }
                    if (key.equals("PM25")) {
                        para = "PM 2.5";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("PM10")) {
                        para = "PM 10";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.pm);
                        addView(para, value, icon);
                    }
                    if (key.equals("OZONE")) {
                        para = "O3";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("CO")) {
                        para = "CO";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("NO2")) {
                        para = "NO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (key.equals("SO2")) {
                        para = "SO2";
                        value = getValue(value);
                        icon = mContext.getResources().getDrawable(R.drawable.hcho);
                        addView(para, value, icon);
                    }
                    if (para.length() > 0)
                        setStatus(para, value, placeName, city, country);
                    break;
            }
        }
    }

    private void addView(String para, String value, Drawable icon) {
        LayoutInflater inflater = LayoutInflater.from(HomeDetailsActivity.this);
        View view = inflater.inflate(R.layout.item_other_details, mPollutantLay,
                false);
        final TextView parameter = (TextView) view.findViewById(R.id.para);
        TextView valueText = (TextView) view.findViewById(R.id.value);
        ImageView info = (ImageView) view.findViewById(R.id.info);
        CardView main = (CardView) view.findViewById(R.id.card);
        if (icon != null)
            info.setImageDrawable(icon);
        mPollutantLay.addView(view);
        main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                // Toast.makeText(mContext, "Show details", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(HomeDetailsActivity.this, TabActivity.class);
                // ID (cityID/deviceId)
                intent.putExtra("ID", mStationId);
                intent.putExtra("Para", parameter.getText().toString());
                startActivity(intent);
            }
        });
        if (value != null && !value.isEmpty()) {
            // convert server data to user understanding data
            double paraVal = ParameterUtil.getParameterValue(mContext, para, value);
            String colour = ParameterUtil.getParameterColour(mContext, para, paraVal);
            parameter.setText(para);
            if (paraVal == 0.0) {
                valueText.setText("NA");
            } else
                valueText.setText(String.valueOf(paraVal));
            switch (colour) {
                case "green":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.green_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.green_s));
                    break;
                case "orange":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.orange_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.orange_s));
                    break;
                case "red":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.red_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.red_s));
                    break;
                case "black":
                    valueText.setTextColor(mContext.getResources().getColor(R.color.black_s));
                    info.setColorFilter(mContext.getResources().getColor(R.color.black_s));
                    break;
                default:
                    break;
            }

        }
    }

    private void initUi() {
        mHeading = (TextView) findViewById(R.id.heading);
        mBackImg = (ImageView) findViewById(R.id.back);
        mFavImg = (ImageView) findViewById(R.id.fav);
        mShareImg = (ImageView) findViewById(R.id.share);
        mPlaceTxt = (TextView) findViewById(R.id.place);
        mValueTxt = (TextView) findViewById(R.id.value);
        mUnitTxt = (TextView) findViewById(R.id.unit);
        mStatusTxt = (TextView) findViewById(R.id.status);
        mPollutantLay = (LinearLayout) findViewById(R.id.pollutantLay);
        mHealthValue1 = (TextView) findViewById(R.id.value1);
        mHealthValue2 = (TextView) findViewById(R.id.value2);
        mListRecommendation = (ListView) findViewById(R.id.recomList);
        mNearbyLay = (LinearLayout) findViewById(R.id.nearbyLay);
        mMainLay = (LinearLayout) findViewById(R.id.main);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //check time for token 5 min expire
        if (DateTimeUtil.checkTime(getApplicationContext())) {
            // call for get token
            checkToken(false, "");
        } else {
            getCleanerPlaces();
            getLatestData();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Take appropriate action for each action item click
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    // check and get token after every 5 min
    private void checkToken(final boolean status, final String action) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());

                            if (action.equals("Action")) {
                                if (status) {
                                    deletePopup(mStationId, mCurrentPlace, "Place");
                                } else
                                    addPlace();
                            } else {
                                getLatestData();
                                getCleanerPlaces();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void deletePopup(final String cityId, String placeName,
                             final String type) {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(HomeDetailsActivity.this);
        View promptsView = li.inflate(R.layout.prompts_delete, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                HomeDetailsActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        TextView heading = (TextView) promptsView.findViewById(R.id.heading);
        TextView cancel = (TextView) promptsView.findViewById(R.id.cancel);
        TextView ok = (TextView) promptsView.findViewById(R.id.ok);
        heading.setText("Are you sure you want to remove" + "\n" + placeName
                + " from your saved places");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                deleteDeviceService(cityId, type);
            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(true);
    }

    private void deleteDeviceService(String cityId, final String type) {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<ResponseBody> call = null;
            if (type.equals("Place")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteCity("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            } else if (type.equals("Device")) {
                call = new RetrofitClient()
                        .getIntance()
                        .getApi()
                        .deleteDevice("Bearer " + AccountUtil.getToken(mContext),
                                cityId);
            }
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response != null && response.code() == 200) {
                        getCleanerPlaces();
                        getLatestData();
                        Toast.makeText(mContext, "Deleted successfully.", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(mContext, "Not deleted, Please try again", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(mContext, mContext.getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

}
