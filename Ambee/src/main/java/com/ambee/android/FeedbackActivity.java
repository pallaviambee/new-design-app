package com.ambee.android;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ambee.android.Data.AddFeedbackBody;
import com.ambee.android.Data.UserRefreshToken;
import com.ambee.android.Retrofit.RetrofitClient;
import com.ambee.android.Util.AccountUtil;
import com.ambee.android.Util.DateTimeUtil;
import com.ambee.android.Util.NetworkUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackActivity extends BaseActivity {
    private Context mContext;
    private static final int PICK_IMAGE_CAMERA = 0;
    private static final int PICK_IMAGE_GALLERY = 2;
    private ImageView mEmo, mBack, mTakeImg;
    private LinearLayout mTrafficLay, mBreathingLay, mBurningLay, mLandfillsLay, mIssueLay,
            mNegligenceLay, mPollutionLay, mSubmitLay, mTakeImgLay;
    private TextView mStatusTxt, mTrafficTxt, mBreathingTxt, mBurningTxt, mLandfillsTxt,
            mIssueTxt, mNegligenceTxt, mPollutionTxt, mSubmitTxt;
    private int mSelectedIcon = 3;
    private String mValue = "";
    // is selected description
    private boolean mIsSelected = false;
    private ArrayList<String> mSelectedDesc = new ArrayList<>();
    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;
    private AlertDialog mAlertDia;
    private boolean mIsSuccess = false;
    private String encodedImage = "";
    private String mRating;
    private String mPlaceId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        mContext = getApplicationContext();
        initUi();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mSelectedIcon = bundle.getInt("SECLECTED", 3);
            mValue = bundle.getString("VALUE", "");
            mPlaceId = bundle.getString("PLACEID", "");
        }
        mStatusTxt.setText(mValue);
        switch (mSelectedIcon) {
            case 1:
                mRating = "1";
                mEmo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.emo1));
                break;
            case 2:
                mRating = "2";
                mEmo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.emo2));
                break;
            case 3:
                mRating = "3";
                mEmo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.emo3));
                break;
            case 4:
                mRating = "4";
                mEmo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.emo4));
                break;
            case 5:
                mRating = "5";
                mEmo.setImageDrawable(mContext.getResources().getDrawable(R.drawable.emo5));
                break;
        }
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mSubmitLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedDesc != null && mSelectedDesc.size() > 0) {
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken();
                    } else {
                        addFeedback();
                    }
                } else {
                    Toast.makeText(mContext, "Please select description", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mSubmitTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSelectedDesc != null && mSelectedDesc.size() > 0) {
                    if (DateTimeUtil.checkTime(getApplicationContext())) {
                        // call for get token
                        checkToken();
                    } else {
                        addFeedback();
                    }
                } else {
                    Toast.makeText(mContext, "Please select description", Toast.LENGTH_SHORT).show();
                }
            }
        });
        mTrafficLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Heavy traffic")) {
                    mSelectedDesc.add("Heavy traffic");
                    mTrafficTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mTrafficLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mTrafficTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Heavy traffic")) {
                    mSelectedDesc.add("Heavy traffic");
                    mTrafficTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mTrafficLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mBreathingLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Difficulty in breathing")) {
                    mSelectedDesc.add("Difficulty in breathing");
                    mBreathingTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mBreathingLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mBreathingTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Difficulty in breathing")) {
                    mSelectedDesc.add("Difficulty in breathing");
                    mBreathingTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mBreathingLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mBurningLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Garbage burning")) {
                    mSelectedDesc.add("Garbage burning");
                    mBurningTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mBurningLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mBurningTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Garbage burning")) {
                    mSelectedDesc.add("Garbage burning");
                    mBurningTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mBurningLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mLandfillsLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Open landfills")) {
                    mSelectedDesc.add("Open landfills");
                    mLandfillsTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mLandfillsLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mLandfillsTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Open landfills")) {
                    mSelectedDesc.add("Open landfills");
                    mLandfillsTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mLandfillsLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mIssueLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("other issues")) {
                    mSelectedDesc.add("other issues");
                    mIssueTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mIssueLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mIssueTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("other issues")) {
                    mSelectedDesc.add("other issues");
                    mIssueTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mIssueLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mNegligenceLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Negligence")) {
                    mSelectedDesc.add("Negligence");
                    mNegligenceTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mNegligenceLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mNegligenceTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Negligence")) {
                    mSelectedDesc.add("Negligence");
                    mNegligenceTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mNegligenceLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mPollutionLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Pollution code violation")) {
                    mSelectedDesc.add("Pollution code violation");
                    mPollutionTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mPollutionLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mPollutionTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mSelectedDesc.contains("Pollution code violation")) {
                    mSelectedDesc.add("Pollution code violation");
                    mPollutionTxt.setTextColor(mContext.getResources().getColor(R.color.white));
                    mPollutionLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
                }
            }
        });
        mTakeImgLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
        mTakeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void checkToken() {
        if (NetworkUtil.checkNetworkConnection(mContext)) {
            Call<UserRefreshToken> call = new RetrofitClient()
                    .getIntance()
                    .getApi()
                    .createToken("Bearer " + AccountUtil.getToken(mContext), AccountUtil.getEmail(mContext));
            // to execute the HTTP call
            call.enqueue(new Callback<UserRefreshToken>() {
                @Override
                public void onResponse(Call<UserRefreshToken> call, Response<UserRefreshToken> response) {
                    UserRefreshToken userRefreshToken = response.body();
                    if (userRefreshToken != null) {
                        if (userRefreshToken.getToken() != null && !userRefreshToken.getToken().isEmpty()) {
                            // set token
                            AccountUtil.setToken(mContext, userRefreshToken.getToken());
                            // set current time
                            AccountUtil.setTime(mContext, DateTimeUtil.getCurrentTimeDate());
                            addFeedback();
                        }
                    }
                }

                @Override
                public void onFailure(Call<UserRefreshToken> call, Throwable t) {
                    Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getBaseContext(), getResources().getString(R.string.no_net_conn),
                    Toast.LENGTH_LONG).show();
        }
    }

    private void addFeedback() {
        AddFeedbackBody addFeedbackBody = new AddFeedbackBody(mPlaceId, mRating, mSelectedDesc, encodedImage);
        Call<ResponseBody> call = new RetrofitClient()
                .getIntance()
                .getApi()
                .userFeedback("Bearer " + AccountUtil.getToken(mContext),
                        addFeedbackBody);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String msg = String.valueOf(response.body());
                if (response.code() == 200) {
                    mIsSuccess = true;
                } else {
                    mIsSuccess = false;
                }
                showDialog();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void showDialog() {
        if (mAlertDia != null)
            mAlertDia.dismiss();
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.promts_feedback_status, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                FeedbackActivity.this, R.style.MyDialogTheme);
        alertDialogBuilder.setView(promptsView);
        ImageView image = (ImageView) promptsView.findViewById(R.id.img);
        TextView text1 = (TextView) promptsView.findViewById(R.id.text1);
        TextView text2 = (TextView) promptsView.findViewById(R.id.text2);
        LinearLayout backLay = (LinearLayout) promptsView.findViewById(R.id.back);
        TextView backText = (TextView) promptsView.findViewById(R.id.back_text);
        LinearLayout retryLay = (LinearLayout) promptsView.findViewById(R.id.retry);
        TextView retryText = (TextView) promptsView.findViewById(R.id.retry_txt);
        if (mIsSuccess) {
            text1.setText("Thank you for your feedback");
            text2.setText("You’ve earned 10 reward points for\nyour feedback");
            image.setImageDrawable(mContext.getResources().getDrawable(R.drawable.success));
            retryLay.setVisibility(View.GONE);
            backText.setTextColor(mContext.getResources().getColor(R.color.white));
            backLay.setBackground(mContext.getResources().getDrawable(R.drawable.blue_rounded_border));
        } else {
        }
        backText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAlertDia != null)
                    mAlertDia.dismiss();
                finish();
            }
        });
        retryText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        mAlertDia = alertDialogBuilder.show();
        mAlertDia.setCanceledOnTouchOutside(false);
    }

    private void initUi() {
        mBack = (ImageView) findViewById(R.id.back);
        mEmo = (ImageView) findViewById(R.id.emo);
        mStatusTxt = (TextView) findViewById(R.id.status);
        mTrafficLay = (LinearLayout) findViewById(R.id.heavy_traffic_lay);
        mTrafficTxt = (TextView) findViewById(R.id.heavy_traffic_text);
        mBreathingLay = (LinearLayout) findViewById(R.id.breathing_lay);
        mBreathingTxt = (TextView) findViewById(R.id.breathing_txt);
        mBurningLay = (LinearLayout) findViewById(R.id.garbage_burning_lay);
        mBurningTxt = (TextView) findViewById(R.id.garbage_burning_text);
        mLandfillsLay = (LinearLayout) findViewById(R.id.open_landfills_lay);
        mLandfillsTxt = (TextView) findViewById(R.id.open_landfills_txt);
        mIssueLay = (LinearLayout) findViewById(R.id.issue_lay);
        mIssueTxt = (TextView) findViewById(R.id.issue_txt);
        mNegligenceLay = (LinearLayout) findViewById(R.id.negligence_lay);
        mNegligenceTxt = (TextView) findViewById(R.id.negligence_text);
        mPollutionLay = (LinearLayout) findViewById(R.id.pollution_lay);
        mPollutionTxt = (TextView) findViewById(R.id.pollution_txt);
        mTakeImgLay = (LinearLayout) findViewById(R.id.takeImgLay);
        mTakeImg = (ImageView) findViewById(R.id.takeImg);
        mSubmitLay = (LinearLayout) findViewById(R.id.submitLay);
        mSubmitTxt = (TextView) findViewById(R.id.submitTxt);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        inputStreamImg = null;
        if (requestCode == PICK_IMAGE_CAMERA) {
            try {
                Uri selectedImage = data.getData();
                bitmap = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                encodedImage = Base64.encodeToString(bytes.toByteArray(),
                        Base64.DEFAULT);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
                destination = new File(Environment.getExternalStorageDirectory() + "/" +
                        getString(R.string.app_name), "IMG_" + timeStamp + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                imgPath = destination.getAbsolutePath();
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                mTakeImg.setVisibility(View.GONE);
                mTakeImgLay.setBackground(d);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == PICK_IMAGE_GALLERY && data != null) {
            Uri selectedImage = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                encodedImage = Base64.encodeToString(bytes.toByteArray(),
                        Base64.DEFAULT);
                imgPath = getRealPathFromURI(selectedImage);
                destination = new File(imgPath.toString());
                Drawable d = new BitmapDrawable(getResources(), bitmap);
                mTakeImg.setVisibility(View.GONE);
                mTakeImgLay.setBackground(d);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void selectImage() {
        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
                AlertDialog.Builder builder = new AlertDialog.Builder(FeedbackActivity.this);
                builder.setTitle("Please select Option");
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Take Photo")) {
                            dialog.dismiss();
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, PICK_IMAGE_CAMERA);
                        } else if (options[item].equals("Choose From Gallery")) {
                            dialog.dismiss();
                            Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
                        } else if (options[item].equals("Cancel")) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, "Camera Permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

}
